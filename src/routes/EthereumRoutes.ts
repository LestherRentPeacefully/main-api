import * as express from 'express';
import {EthereumService} from '../services/Wallet/EthereumService';
import {SharedService} from '../services/Shared/SharedService';



const sharedService = new SharedService();


const EthereumRoutes = express.Router();

const ethereumService = new EthereumService();




EthereumRoutes.post('/sendAndUploadWalletFileVerificationCode',sharedService.verifyIdToken,((req,res)=>{

	ethereumService.sendAndUploadWalletFileVerificationCode(res.locals.uid)
		.then(()=>res.send({success:true,data:null,error:null}))
		.catch((err) => res.send({success: false, data: null, error: {message: err}}));
}));

EthereumRoutes.post('/getWalletFileAndVerifyCode',sharedService.verifyIdToken, ((req,res)=>{
	
	if(req.body.code) {
		ethereumService.getWalletFileAndVerifyCode(res.locals.uid, req.body.code)
			.then((walletFile)=> res.send({success:true,data:walletFile,error:null}))
			.catch((err)=> res.send(err));

	}else{
		res.send({success:false,data:null,error:{message:'Missing parameters'}});
	}
}));

EthereumRoutes.post('/getGasPrice',sharedService.verifyIdToken,((req,res)=>{
	res.send({ success:true, data:ethereumService.getGasPrice(), error:null });
}));



EthereumRoutes.post('/getBalances',sharedService.verifyIdToken,((req,res)=>{
	ethereumService.getBalances(res.locals.uid)
		.then((data)=>res.send(data))
		.catch((err)=>res.send(err));
}));

EthereumRoutes.post('/estimateGasAndGasPrice',sharedService.verifyIdToken,((req,res)=>{
	if (req.body.from && req.body.to && req.body.amount && req.body.amount>0) {
		ethereumService.estimateGasAndGasPrice(req.body.from, req.body.to, req.body.amount)
			.then((response)=>{
				res.send({success:true,data:response,error:null});
			}).catch((err)=>{
				res.send({success:false,data:null,error:{message:err}});
			});
	} else {
		res.send({success:false,data:null,error:{message:'Missing parameters'}});
	}
}));


EthereumRoutes.post('/sendTx',sharedService.verifyIdToken,((req,res)=>{

	if (req.body.to && req.body.amount && req.body.amount>0 && req.body.gasPrice && req.body.gasLimit) { 
		ethereumService.send(res.locals.uid, req.body.to, req.body.amount, req.body.gasPrice, req.body.gasLimit)
			.then((hash)=>{
				res.send({success:true,data:hash,error:null});
			}).catch((err)=>{
				res.send({success:false,data:null,error:{message:err}});
			});
	} else {
		res.send({success:false,data:null,error:{message:'Missing parameters'}});
	}

}));

EthereumRoutes.post('/getTransaction',sharedService.verifyIdToken,((req,res)=>{
	if (req.body.txHash) {
		ethereumService.getTransaction(req.body.txHash)
			.then((response)=>{
				res.send({success:true,data:response,error:null});
			}).catch((err)=>{
				res.send({success:false,data:null,error:{message:err}});
			});
	} else {
		res.send({success:false,data:null,error:{message:'Missing parameters'}});
	}
}));



export { EthereumRoutes };