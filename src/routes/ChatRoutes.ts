const express = require("express");
import {SharedService} from '../services/Shared/SharedService';
import {ChatService} from '../services/Chat/ChatService';

const ChatRoutes = express.Router();

const chatService = new ChatService();

const sharedService = new SharedService();



ChatRoutes.post('/getChats', sharedService.verifyIdToken, ((req, res)=>{

  chatService.getChats(res.locals.uid)
    .then((chats)=>res.send({success:true, data: chats, error: null}))
    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

}));


ChatRoutes.post('/sendMessage', sharedService.verifyIdToken, ((req, res)=>{

  if (req.body.chatID && req.body.content) {
    chatService.sendMessage(res.locals.uid, req.body.chatID, req.body.content)
      .then((id)=>res.send({success:true, data: id, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
    
  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));


export {ChatRoutes};