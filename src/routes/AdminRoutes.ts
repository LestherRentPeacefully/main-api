const express = require("express");
import {RequestService} from '../services/Admin/RequestService';
import {SharedService} from '../services/Shared/SharedService';




const AdminRoutes = express.Router();

const requestService = new RequestService();

const sharedService = new SharedService();




AdminRoutes.post('/rejectIDRequest', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.rejectionReason && req.body.requestID && req.body.role && req.body.uid) { 
		requestService.rejectIDRequest(res.locals.uid, req.body.rejectionReason, req.body.requestID, req.body.role, req.body.uid)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error. Try again later'}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AdminRoutes.post('/verifyIDRequest', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.uid && req.body.fields && req.body.id && req.body.role) { 
		requestService.verifyIDRequest(req.body.uid, req.body.fields, req.body.id, req.body.role)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AdminRoutes.post('/rejectListing', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.listingID && req.body.rejectionReason) { 
		requestService.rejectListing(res.locals.uid, req.body.listingID, req.body.rejectionReason)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AdminRoutes.post('/verifyListing', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.uid && req.body.listingID && req.body.fields && req.body.fields.name && 
		req.body.fields.name.length<=50 && req.body.fields.address && req.body.fields.location && 
		req.body.fields.type && req.body.fields.numOfBeds && req.body.fields.numOfBaths && req.body.fields.sqft && 
		req.body.fields.rentAmount>0 && req.body.fields.securityDepositAmount>=0 && req.body.fields.details && 
		req.body.fields.details.length<=1000 && req.body.fields.availableFrom) {
		requestService.verifyListing(req.body.uid, req.body.listingID, req.body.fields)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}

}));


AdminRoutes.post('/rejectWithdrawalRequest', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.withdrawalID && req.body.rejectionReason) { 
		requestService.rejectWithdrawalRequest(res.locals.uid, req.body.withdrawalID, req.body.rejectionReason)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AdminRoutes.post('/processWithdrawalRequest', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.withdrawalID && req.body.txHash) { 
		requestService.processWithdrawalRequest(res.locals.uid, req.body.withdrawalID, req.body.txHash)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AdminRoutes.post('/getPlaformBalances', sharedService.verifyIdToken, ((req, res)=>{
	sharedService.forwardRequest('altcoin/getPlaformBalances', {})
		.then(info => res.send(info))
		.catch(err => res.send({success:false, data: null, error: {message: err}}));

}));


AdminRoutes.post('/estimatePlaformWithdrawalFee', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency && req.body.to && req.body.amount) { 
		requestService.estimatePlaformWithdrawalFee(req.body.currency, req.body.to, req.body.amount)
			.then(info => res.send({success:true, data: info, error: null}))
    	.catch(err => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



export {AdminRoutes};