const express = require("express");
import {SettingsService} from '../services/User/SettingsService';

/*
* Init
* */
const SettingsRoutes = express.Router();


// Settings Service
const settings = new SettingsService();


SettingsRoutes.post('/resetPassword',((req,res)=>{
    // Check for required parameters
    if(req.body.currentPassword && req.body.newPassword && req.body.email){
      settings.resetPassword(req.body.currentPassword, req.body.newPassword,req.body.email)
        .then(()=> res.send({success:true, data: null, error: null}))
        .catch(err=> res.send({success:false, data: null, error: { message: err}}));

    } else {
      res.send({success:false, data: null, error: { message: 'Missing Parameters'}});
    }

}));



export {SettingsRoutes};