const express = require("express");
import {AccountService} from '../services/User/AccountService';


const AccountRoutes = express.Router();


// Account Service
const account = new AccountService();


AccountRoutes.route('/create').post((req,res)=>{
    // Check for required parameters
  if(req.body.role && req.body.firstName && req.body.lastName && req.body.email && req.body.password){
      // Create User
    account.createAccount(req.body.role,req.body.firstName,req.body.lastName,req.body.email,req.body.password,req.body.brokerage,req.body.licenceNo)
      .then((uid) => res.send({success:true, data: uid, error: null}))
			.catch((err) => res.send({success: false, data: null, error: {message: err}}));
			
  } else {
      res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
  }

});


AccountRoutes.route('/verifyEmail').post((req,res)=>{
    // Check for required parameters
    if(req.body.code){
        // Verify code
			account.verifyEmail(req.body.code)
				.then(() => res.send({success:true, data: null, error: null}))
				.catch((err) => res.send({success: false, data: null, error: {message: err}}));
    } else {
        res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
    }

});

AccountRoutes.route('/resendEmailVerificationCode').post((req,res)=>{
    // Check for required parameters
    if(req.body.email){
        account.resendAndUploadEmailVerificationCode(req.body.email)
					.then(()=> res.send({success:true, data: null, error: null}))
					.catch((err) => res.send({success: false, data: null, error: {message: err}}));      
    } else {
        res.send({success:false, data: null, error: { message: 'Missing Parameters'}})
    }

});

AccountRoutes.route('/sendAndUploadResetPasswordCode').post((req,res)=>{
    // Check for required parameters
    if(req.body.email){
			account.sendAndUploadResetPasswordCode(req.body.email)
				.then(()=> res.send({success:true, data: null, error: null}))
				.catch(err=> res.send({success:false, data: null, error: { message: err}}));
    } else {
        res.send({success:false, data: null, error: { message: 'Missing Parameters'}})
    }

});

AccountRoutes.route('/resetPassword').post((req,res)=>{
    // Check for required parameters
    if(req.body.code && req.body.password){
			account.resetPasswordWithCode(req.body.code, req.body.password)
				.then(()=> res.send({success:true, data: null, error: null}))
				.catch(err=>res.send({success:false, data: null, error: { message: err}}));

    } else {
        res.send({success:false, data: null, error: { message: 'Missing Parameters'}})
    }

});

export {AccountRoutes};