const express = require("express");
import {AccountService} from '../services/User/AccountService';
import {PropertiesRentedService} from '../services/PropertiesRented/PropertiesRentedService';
const multer  = require('multer')
import * as admin from "firebase-admin";

/*
* Init
* */
const PropertiesRentedRoutes = express.Router();


//Properties Service
const propertiesRentedService = new PropertiesRentedService();


/*Account service*/
const accountService = new AccountService();

/*Middleware*/
const verifyIdToken = (req,res,next)=>{

	const token = req.get('authorization');

	if (token) {

		accountService.verifyIdToken(token).then((uid)=>{
			res.locals.uid = uid; 
			next();
		
		}).catch(err=>res.send({success:false,data:null,error:{message:err}}));

	} else {
		res.send({success:false,data:null,error:{message:'No token provided'}});
	}
}



const upload = multer({
	storage: multer.diskStorage({
		destination: './rental-agreements',
		filename: (req, file, cb) => 
			cb(null, `${admin.firestore().collection('uniqueID').doc().id}.${file.originalname.substr(file.originalname.lastIndexOf('.') + 1)}`)
	})
});



PropertiesRentedRoutes.post('/endLease',verifyIdToken,((req, res)=>{
	if (req.body.propertyRentedID && req.body.userInPropertyID) { 
		propertiesRentedService.endLease(res.locals.uid, req.body.userInPropertyID, req.body.propertyRentedID)
			.then(()=>{
				res.send({success:true, data: null, error: null});
			}).catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/cancelCulmination',verifyIdToken,((req, res)=>{
	if (req.body.propertyRentedID) { 
		propertiesRentedService.cancelCulmination(res.locals.uid, req.body.propertyRentedID)
			.then(()=>{
				res.send({success:true, data: null, error: null});
			}).catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/provideFeedBack', verifyIdToken, ((req,res)=>{

	if (req.body.userInPropertyID && req.body.propertyRentedID && req.body.feedback) {

		propertiesRentedService.provideFeedBack(res.locals.uid, req.body.userInPropertyID, req.body.propertyRentedID, req.body.feedback)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/depositWithCrypto', verifyIdToken, ((req,res)=>{

	if (req.body.propertyRentedID && req.body.currency && req.body.amount) {

		propertiesRentedService.depositWithCrypto(res.locals.uid, req.body.propertyRentedID, req.body.currency, req.body.amount)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/ticket', verifyIdToken, ((req,res)=>{

	if (req.body.propertyRentedID) {

		propertiesRentedService.ticket(res.locals.uid, req.body.propertyRentedID, req.body.details)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/modifyRent', verifyIdToken, ((req,res)=>{

	if (req.body.propertyRentedID && req.body.newRent) {

		propertiesRentedService.modifyRent(res.locals.uid, req.body.propertyRentedID, req.body.newRent)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/getCurrencyPriceForEsign', verifyIdToken, ((req,res)=>{

	if (req.body.currency) {

		propertiesRentedService.getCurrencyPriceForEsign(res.locals.uid, req.body.currency)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/payForEsignWithCrypto', verifyIdToken, ((req,res)=>{

	if (req.body.currency && req.body.propertyRentedID) {

		propertiesRentedService.payForEsignWithCrypto(res.locals.uid, req.body.currency, req.body.propertyRentedID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/getSignatures', verifyIdToken, ((req,res)=>{

	if (req.body.signatureRequestID) {

		propertiesRentedService.getSignatures(req.body.signatureRequestID)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/uploadDocument', verifyIdToken, upload.single('doc'), ((req,res)=>{

	if (req.body.propertyRentedID) {

		propertiesRentedService.uploadDocument(res.locals.uid, req.body.propertyRentedID, `${req.file.destination}/${req.file.filename}`)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/getSignUrl', verifyIdToken, ((req,res)=>{

	if (req.body.signatureID) {

		propertiesRentedService.getSignUrl(req.body.signatureID)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


PropertiesRentedRoutes.post('/getDocUrl', verifyIdToken, ((req,res)=>{

	if (req.body.signatureRequestID) {

		propertiesRentedService.getDocUrl(req.body.signatureRequestID)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


export {PropertiesRentedRoutes};