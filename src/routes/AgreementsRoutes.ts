import * as express from 'express';
import {SharedService} from '../services/Shared/SharedService';
import {Agreements} from '../services/Agreements/Agreements';
const multer = require('multer')

const sharedService = new SharedService();

const agreements = new Agreements();



const AgreementsRoutes = express.Router();



const upload = multer({
	storage: multer.diskStorage({
		destination: './rental-agreements',
		filename: (req, file, cb) => 
			cb(null, `${sharedService.generateId()}.${file.originalname.substr(file.originalname.lastIndexOf('.') + 1)}`)
	})
});



AgreementsRoutes.post('/getArrayOfSignatures', sharedService.verifyIdToken, ((req, res)=>{
	
	if (req.body.signaturesRequestID && req.body.signaturesRequestID.length>0) {
		agreements.getArrayOfSignatures(req.body.signaturesRequestID)
			.then((arrayOfSignatures)=>res.send({success:true, data: arrayOfSignatures, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}
}));


AgreementsRoutes.post('/getCurrencyPriceForEsign', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency) {

		agreements.getCurrencyPriceForEsign(res.locals.uid, req.body.currency)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



AgreementsRoutes.post('/payForEsignWithCrypto', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency && req.body.agreementId) {

		agreements.payForEsignWithCrypto(res.locals.uid, req.body.currency, req.body.agreementId)
			.then((id)=>res.send({success:true, data: id, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/getSignUrl', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.signatureID) {

		agreements.getSignUrl(req.body.signatureID)
			.then((signUrl)=>res.send({success:true, data: signUrl, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/uploadDocument', sharedService.verifyIdToken, upload.single('doc'), ((req,res)=>{

	if (req.body.agreementId && req.body.signatureRequestSelectedID) {

		agreements.uploadDocument(res.locals.uid, req.body.agreementId, req.body.signatureRequestSelectedID, `${(<any>req).file.destination}/${(<any>req).file.filename}`)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



AgreementsRoutes.post('/estimateGasAndGasPriceDocSaving', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.agreementId) {
		agreements.estimateGasAndGasPriceDocSaving(res.locals.uid, req.body.agreementId)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/getDocUrl', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.signatureRequestID) {

		agreements.getDocUrl(req.body.signatureRequestID)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/uploadToIPFS', sharedService.verifyIdToken, ((req,res)=>{
	// Check for required parameters
	if (req.body.url) {
		agreements.uploadToIPFS(req.body.url)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/saveDocOnBlockchain', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.ipfsHash && req.body.agreementId && req.body.landlordId && req.body.signatureRequestSelectedID && req.body.gasPrice && req.body.gasLimit) {
		agreements.saveDocOnBlockchain(
			res.locals.uid, 
			req.body.ipfsHash, 
			req.body.agreementId, 
			req.body.landlordId, 
			req.body.signatureRequestSelectedID, 
			req.body.gasPrice, 
			req.body.gasLimit
			)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



AgreementsRoutes.post('/estimateGasAndGasPriceContractCreation', sharedService.verifyIdToken, ((req, res)=>{

	agreements.estimateGasAndGasPriceContractCreation(res.locals.uid)
		.then((info)=>res.send({success:true, data: info, error: null}))
		.catch((err)=>res.send({success:false, data: null, error: {message: err}}));


}));


AgreementsRoutes.post('/createSmartContract', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.agreementId && req.body.gasPrice && req.body.gasLimit) {

		agreements.createSmartContract(res.locals.uid, req.body.agreementId, req.body.gasPrice, req.body.gasLimit)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/endLease', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.agreementId) {

		agreements.endLease(res.locals.uid, req.body.agreementId)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/cancelCulmination', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.agreementId) {

		agreements.cancelCulmination(res.locals.uid, req.body.agreementId)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


AgreementsRoutes.post('/startChat', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.content && req.body.agreementId) {

		agreements.startChat(res.locals.uid, req.body.content, req.body.agreementId)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));




export {AgreementsRoutes};