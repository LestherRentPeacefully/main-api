const express = require("express");
import {PricingService} from '../services/pricing/PricingService';
import {AccountService} from '../services/User/AccountService';


/*
* Init
* */
const PricingRoutes = express.Router();
/*Account service*/
const accountService = new AccountService();

const pricingService = new PricingService();

/*Middleware*/
const verifyIdToken = (req,res,next)=>{

	const token = req.get('authorization');

	if (token) {

		accountService.verifyIdToken(token).then((uid)=>{
			res.locals.uid = uid; 
			next();
		
		}).catch(err=>res.send({success:false,data:null,error:{message:err}}));

	} else {
		res.send({success:false,data:null,error:{message:'No token provided'}});
	}
}


PricingRoutes.post('/getCurrencyPrice', verifyIdToken, ((req,res)=>{

  if (req.body.currency) {
    pricingService.getCurrencyPrice(res.locals.uid, req.body.currency)
      .then((response)=>res.send({success:true, data: response, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected getting price'}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
  }

}));


PricingRoutes.post('/buyPricingPlan', verifyIdToken, ((req,res)=>{

  if (req.body.currency && req.body.plan) {
    pricingService.buyPricingPlan(res.locals.uid, req.body.currency, req.body.plan)
      .then(()=>res.send({success:true, data: null, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
  }

}));


PricingRoutes.post('/cancelPricingPlan', verifyIdToken, ((req,res)=>{

	pricingService.cancelPricingPlan(res.locals.uid)
		.then(()=>res.send({success:true, data: null, error: null}))
		.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error. Try again later'}}));

}));


PricingRoutes.post('/selectPaymentMethod', verifyIdToken, ((req,res)=>{

  if (req.body.currency) {
    pricingService.selectPaymentMethod(res.locals.uid, req.body.currency)
      .then(()=>res.send({success:true, data: null, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
    
  } else {
    res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
  }

}));

export {PricingRoutes};