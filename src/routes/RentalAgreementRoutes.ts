const express = require("express");
import {RentalAgreementService} from '../services/rentalAgreement/RentalAgreementService';
import {ClauseValidatorService} from '../services/rentalAgreement/ClauseValidatorService';
import {smartContract} from '../services/rentalAgreement/smartContract';
import {AccountService} from '../services/User/AccountService';

/*
* Init
* */
const RentalAgreementRoutes = express.Router();

/* Rental agreement service */
const rentalAgreementService = new RentalAgreementService();

/*Validator for Rental agreement clauses*/
const clauseValidatorService = new ClauseValidatorService();

/*Account service*/
const accountService = new AccountService();


/*Middleware*/
const verifyIdToken = (req,res,next)=>{

	const token = req.get('authorization');

	if (token) {

		accountService.verifyIdToken(token).then((uid)=>{
			res.locals.uid = uid; 
			next();
		
		}).catch(err=>res.send({success:false,data:null,error:{message:err}}));

	} else {
		res.send({success:false,data:null,error:{message:'No token provided'}});
	}
}


RentalAgreementRoutes.post('/smartContractCodePreview', verifyIdToken, ((req, res)=>{
	if (req.body.general && req.body.property && req.body.parties && req.body.terms && req.body.finalDetails && req.body.propertyRentedID) { 
		let clauses = req.body;
		clauses.uid = res.locals.uid;

		clauseValidatorService.validateClauses(clauses)
			.then(()=> res.send({success:true, data: smartContract(clauses), error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

RentalAgreementRoutes.post('/estimateGasAndGasPriceContractCreation', verifyIdToken, (async (req, res)=>{
	if (req.body.general && req.body.property && req.body.parties && req.body.terms && req.body.finalDetails && req.body.propertyRentedID) {

		let clauses = req.body;
		clauses.uid = res.locals.uid;
		try {
			await clauseValidatorService.validateClauses(clauses);
			let info = await rentalAgreementService.estimateGasAndGasPriceContractCreation(clauses);
			res.send({success:true, data: info, error: null})
		} catch (err) {
			res.send({success:false, data: null, error: {message: err}});
		}

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

RentalAgreementRoutes.post('/new',verifyIdToken, (async (req,res)=>{
	if (req.body.general && req.body.property && req.body.parties && req.body.terms && req.body.finalDetails &&
			req.body.propertyRentedID && req.body.gasPrice && req.body.gasLimit) { 
		
		let clauses = req.body;
		clauses.uid = res.locals.uid;
		try {
			await clauseValidatorService.validateClauses(clauses);
			let hash = await rentalAgreementService.create(clauses);
			res.send({success:true, data: hash, error: null});
		} catch (err) {
			res.send({success:false, data: null, error: {message: err}});
		}

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


RentalAgreementRoutes.post('/estimateGasAndGasPriceRentalAgreementAction',verifyIdToken,((req,res)=>{
	if (req.body.from && req.body.action && 
			(req.body.action=='approveAgreement' || 
			 req.body.action=='withdraw' || 
			 req.body.action=='startDispute' || 
			 req.body.action=='endDispute' || 
			 req.body.action=='openTicket' || 
			 req.body.action=='closeTicket' || 
			((req.body.action=='modifyRent' || req.body.action=='deposit') && req.body.rent)) && 
		  req.body.contractAddress) { 
		rentalAgreementService.estimateGasAndGasPriceRentalAgreementAction(req.body.from,req.body.action,req.body.contractAddress,req.body.rent)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

RentalAgreementRoutes.post('/rentalAgreementAction',verifyIdToken,((req,res)=>{
	if (req.body.gasPrice && req.body.gasLimit && req.body.action && 
		 (req.body.action=='approveAgreement' || 
		 	req.body.action=='withdraw' || 
		 	req.body.action=='startDispute' || 
		 	req.body.action=='endDispute' || 
		 	req.body.action=='openTicket' || 
		 	req.body.action=='closeTicket' || 
		 ((req.body.action=='modifyRent' || req.body.action=='deposit') && req.body.rent)) && 
		 	req.body.contractAddress) { 
		rentalAgreementService.rentalAgreementAction(res.locals.uid,req.body.gasPrice,req.body.gasLimit,req.body.action,req.body.contractAddress,req.body.rent)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


RentalAgreementRoutes.post('/getRentalAgreementActions',verifyIdToken,((req,res)=>{
	if (req.body.contractAddress) { 
		rentalAgreementService.getRentalAgreementActions(req.body.contractAddress)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

RentalAgreementRoutes.post('/viewClauses',verifyIdToken,((req,res)=>{
	if (req.body.contractAddress) { 
		rentalAgreementService.viewClauses(req.body.contractAddress)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


RentalAgreementRoutes.post('/viewContractState',verifyIdToken,((req,res)=>{
	if (req.body.contractAddress) { 
		rentalAgreementService.viewContractState(req.body.contractAddress,res.locals.uid)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


RentalAgreementRoutes.post('/checkApproval',verifyIdToken,((req,res)=>{
	if (req.body.contractAddress) { 
		rentalAgreementService.checkApproval(res.locals.uid, req.body.contractAddress)
			.then(info=>{
				res.send({success:true, data: info, error: null});
			}).catch((err) => { res.send({success:false, data: null, error: {message: err}}); });

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));




export {RentalAgreementRoutes};