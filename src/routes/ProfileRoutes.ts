// const express = require("express");
import * as express from 'express'
import {ProfileService} from '../services/User/ProfileService';
import {AccountService} from '../services/User/AccountService';



/*
* Init
* */
const ProfileRoutes = express.Router();



/*Profile Service*/
const profileService = new ProfileService();


/*Account service*/
const accountService = new AccountService();


/*Middleware*/
const verifyIdToken = (req,res,next)=>{

	const token = req.get('authorization');

	if (token) {

		accountService.verifyIdToken(token).then((uid)=>{
			res.locals.uid = uid; 
			next();
		
		}).catch(err=>res.send({success:false,data:null,error:{message:err}}));

	} else {
		res.send({success:false,data:null,error:{message:'No token provided'}});
	}
}


ProfileRoutes.post('/uploadProfilePicture', verifyIdToken, ((req,res)=>{

	if (req.body.photo) {

		profileService.uploadProfilePicture(res.locals.uid, req.body.photo, req.body.oldFiles)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: 'Unexpected error uploading profile picture'}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

ProfileRoutes.post('/addAreaServing', verifyIdToken, ((req,res)=>{

	if (req.body.role && req.body.name && req.body.lat && req.body.lng && req.body.geohash) {

		profileService.addAreaServing(res.locals.uid, req.body.role, req.body.name, req.body.lat, req.body.lng, req.body.geohash)
			.then((id)=>res.send({success:true, data: id, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

ProfileRoutes.post('/deleteAreaServing', verifyIdToken, ((req,res)=>{

	if (req.body.id) {

		profileService.deleteAreaServing(res.locals.uid, req.body.id)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

ProfileRoutes.post('/editSkills', verifyIdToken, ((req,res)=>{

	if (req.body.role && req.body.skills) {

		profileService.editSkills(res.locals.uid, req.body.role, req.body.skills)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


ProfileRoutes.post('/hireMe', verifyIdToken, ((req,res)=>{

	if ((req.body.role=='tenant' || req.body.role=='landlord') && req.body.serviceProviderID && req.body.name && 
			req.body.name.length<=80 && req.body.details && req.body.details.length<=1000 && 
		  req.body.price && req.body.price>=5 && req.body.skillsRequired && Object.keys(req.body.skillsRequired).length>0) {

		profileService.hireMe(res.locals.uid, req.body.role, req.body.serviceProviderID, req.body.name, req.body.details, req.body.price, req.body.skillsRequired)
			.then((id)=>res.send({success:true, data: id, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid parameters'}});
	}

}));


ProfileRoutes.post('/contactMe', ((req, res)=>{

	if (req.body.agentId && req.body.message && req.body.email && req.body.name) {

		profileService.contactMe(req.body.agentId, req.body.message, req.body.email, req.body.name)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid parameters'}});
	}

}));



export {ProfileRoutes};