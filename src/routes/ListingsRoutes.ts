const express = require("express");
import {SharedService} from '../services/Shared/SharedService';
import {ListingsService} from '../services/ListingsService/ListingsService';



const sharedService = new SharedService();

const listingsService = new ListingsService();


const ListingsRoutes = express.Router();



ListingsRoutes.post('/new', sharedService.verifyIdToken, ((req, res)=>{
    
  if (req.body.name && req.body.name.length<=50 && req.body.type && req.body.numOfBeds && req.body.numOfBaths && 
      req.body.sqft && req.body.rentAmount>0 && req.body.securityDepositAmount>=0 && req.body.details && 
      req.body.details.length<=1000 && req.body.availableFrom && req.body.photos && req.body.photos.length>=3) {
        listingsService.new(req.body, res.locals.uid)
      .then((listingID)=>res.send({success:true, data: listingID, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));


ListingsRoutes.post('/isTenant', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.myPropertyID) {
		listingsService.isTenant(res.locals.uid, req.body.myPropertyID)
			.then((isTenant)=>res.send({success:true, data: isTenant, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/submitApplication', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID && req.body.rentAmount && req.body.securityDepositAmount && req.body.message) {
		listingsService.submitApplication(res.locals.uid, req.body.listingID, req.body.rentAmount, req.body.securityDepositAmount, req.body.message)
			.then(()=>res.send({success:true, data: null, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/deleteApplication', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID) {
		listingsService.deleteApplication(res.locals.uid, req.body.listingID)
			.then(()=>res.send({success:true, data: null, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));

ListingsRoutes.post('/acceptApplication', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID && req.body.applicantID) {
		listingsService.acceptApplication(res.locals.uid, req.body.listingID, req.body.applicantID)
			.then(()=>res.send({success:true, data: null, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));

ListingsRoutes.post('/retractApplication', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID && req.body.applicantID) {
		listingsService.retractApplication(res.locals.uid, req.body.listingID, req.body.applicantID)
			.then(()=>res.send({success:true, data: null, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));

ListingsRoutes.post('/deleteListing', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID) {
		listingsService.deleteListing(res.locals.uid, req.body.listingID, req.body.oldFiles)
			.then(()=>res.send({success:true, data: null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));

ListingsRoutes.post('/moveIn', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.listingID && req.body.listingOwnerID) {
		listingsService.moveIn(res.locals.uid, req.body.listingID, req.body.listingOwnerID)
			.then((myPropertyID)=>res.send({success:true, data: myPropertyID, error: null}))
	    .catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));

ListingsRoutes.post('/getTotalListingsOnApp', ((req, res)=>{
	listingsService.getTotalListingsOnApp()
		.then((total)=>res.send({success:true, data: total, error: null}))
		.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading listings'}}));


}));


ListingsRoutes.post('/getTotalListings', ((req, res)=>{
if (req.body.filter && req.body.filter.lat && req.body.filter.lng && req.body.filter.radius) {
	listingsService.getTotalListings(req.body.filter)
		.then((total)=>res.send({success:true, data: total, error: null}))
		.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading listings'}}));

} else {
	res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
}
}));


ListingsRoutes.post('/getListings', ((req, res)=>{
if (req.body.filter && req.body.filter.lat && req.body.filter.lng && req.body.filter.radius && req.body.offset>=0) {
	listingsService.getListings(req.body.filter, req.body.offset)
		.then((listings)=>res.send({success:true, data: listings, error: null}))
		.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading listings'}}));

} else {
	res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
}
}));


ListingsRoutes.post('/getTotalAndListings', ((req, res)=>{
if (req.body.filter && req.body.filter.lat && req.body.filter.lng && req.body.filter.radius && req.body.offset>=0) {
	listingsService.getTotalAndListings(req.body.filter, req.body.offset)
		.then((listings)=>res.send({success:true, data: listings, error: null}))
		.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading listings'}}));

} else {
	res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
}
}));


ListingsRoutes.post('/editListing', sharedService.verifyIdToken, ((req, res)=>{
	if (req.body.name && req.body.name.length<=50 && req.body.type && req.body.listingID && req.body.numOfBeds && 
		req.body.numOfBaths && req.body.sqft && req.body.rentAmount>0 && 
		req.body.securityDepositAmount>=0 && req.body.details && req.body.details.length<=1000 &&
		req.body.availableFrom && req.body.photos && req.body.photos.length>=3) {

			listingsService.editListing(res.locals.uid, req.body, req.body.listingID, req.body.oldFiles)
				.then((listings)=>res.send({success:true, data: listings, error: null}))
				.catch((err) => res.send({success:false, data: null, error: {message: err}}));
			
	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}
}));


ListingsRoutes.post('/markListingAsRented', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.listingID) {
		listingsService.markListingAsRented(res.locals.uid, req.body.listingID)
			.then(()=>res.send({success:true, data: null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/getCurrencyPriceForFeesAndCharges', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency) {
		listingsService.getCurrencyPriceForFeesAndCharges(res.locals.uid, req.body.currency)
			.then((data)=>res.send({success:true, data, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/highlightListing', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency && req.body.listingID) {
		listingsService.highlightListing(res.locals.uid, req.body.currency, req.body.listingID)
			.then(()=>res.send({success:true, data:null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/highlightApplication', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency && req.body.listingID) {
		listingsService.highlightApplication(res.locals.uid, req.body.currency, req.body.listingID)
			.then(()=>res.send({success:true, data:null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/contactAdvertiser', ((req, res)=>{

	if (req.body.listingID && req.body.message && req.body.email && req.body.name) {
		listingsService.contactAdvertiser(req.body.listingID, req.body.message, req.body.email, req.body.name)
			.then(()=>res.send({success:true, data:null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));


ListingsRoutes.post('/startChat', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.content && req.body.listingID && req.body.applicantID) {
		listingsService.startChat(res.locals.uid, req.body.content, req.body.listingID, req.body.applicantID)
			.then((info)=>res.send({success:true, data:info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));



export {ListingsRoutes};