const express = require("express");
import {myPropertiesService} from '../services/myProperties/myPropertiesService';
import {AccountService} from '../services/User/AccountService';
import {SharedService} from '../services/Shared/SharedService';
const multer  = require('multer')

/*
* Init
* */
const myPropertiesRoutes = express.Router();


// My Properties Service
const myProperties = new myPropertiesService();


const sharedService = new SharedService();



const upload = multer({
	storage: multer.diskStorage({
		destination: './rental-agreements',
		filename: (req, file, cb) => 
			cb(null, `${sharedService.generateId()}.${file.originalname.substr(file.originalname.lastIndexOf('.') + 1)}`)
	})
});



myPropertiesRoutes.post('/new', sharedService.verifyIdToken, ((req, res)=>{
    
	if (req.body.name && req.body.name.length<=50 && req.body.address && req.body.location && 
			req.body.type && req.body.photos) {
		myProperties.new(req.body, res.locals.uid)
			.then((myPropertyID)=>res.send({success:true, data: myPropertyID, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}

}));


myPropertiesRoutes.post('/edit', sharedService.verifyIdToken, ((req, res)=>{
    
	if (req.body.name && req.body.name.length<=50 && req.body.address && req.body.location && 
			req.body.type && req.body.photos) {
		myProperties.edit(res.locals.uid, req.body, req.body.myPropertyID, req.body.oldFiles)
			.then(()=>res.send({success:true, data: null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}

}));


myPropertiesRoutes.post('/delete', sharedService.verifyIdToken, ((req, res)=>{
    
	if (req.body.myPropertyID) {
		myProperties.delete(res.locals.uid, req.body.myPropertyID)
			.then(()=>res.send({success:true, data: null, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}

}));



myPropertiesRoutes.post('/getTenants', sharedService.verifyIdToken, ((req, res)=>{
	
	if (req.body.tenantsID && req.body.tenantsID.length>=0) {
		myProperties.getTenants(req.body.tenantsID)
			.then((tenantsInfo)=>res.send({success:true, data: tenantsInfo, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}
}));


myPropertiesRoutes.post('/getArrayOfSignatures', sharedService.verifyIdToken, ((req, res)=>{
	
	if (req.body.signaturesRequestID && req.body.signaturesRequestID.length>0) {
		myProperties.getArrayOfSignatures(req.body.signaturesRequestID)
			.then((arrayOfSignatures)=>res.send({success:true, data: arrayOfSignatures, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}
}));



myPropertiesRoutes.post('/getCurrencyPriceForEsign', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency) {

		myProperties.getCurrencyPriceForEsign(res.locals.uid, req.body.currency)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



myPropertiesRoutes.post('/payForEsignWithCrypto', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.currency && req.body.myPropertyID && req.body.tenantID) {

		myProperties.payForEsignWithCrypto(res.locals.uid, req.body.currency, req.body.myPropertyID, req.body.tenantID)
			.then((id)=>res.send({success:true, data: id, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/uploadDocument', sharedService.verifyIdToken, upload.single('doc'), ((req,res)=>{

	if (req.body.myPropertyID && req.body.signatureRequestSelectedID && req.body.tenantID) {

		myProperties.uploadDocument(res.locals.uid, req.body.myPropertyID, req.body.signatureRequestSelectedID, req.body.tenantID, `${req.file.destination}/${req.file.filename}`)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/getSignUrl', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.signatureID) {

		myProperties.getSignUrl(req.body.signatureID)
			.then((signUrl)=>res.send({success:true, data: signUrl, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



myPropertiesRoutes.post('/getDocUrl', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.signatureRequestID) {

		myProperties.getDocUrl(req.body.signatureRequestID)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



myPropertiesRoutes.post('/depositWithCrypto', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID && req.body.currency && req.body.amount) {

		myProperties.depositWithCrypto(res.locals.uid, req.body.myPropertyID, req.body.currency, req.body.amount)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/ticket', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID) {

		myProperties.ticket(res.locals.uid, req.body.myPropertyID, req.body.details)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));



myPropertiesRoutes.post('/modifyRent', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID && req.body.tenantID && req.body.newRent) {

		myProperties.modifyRent(res.locals.uid, req.body.myPropertyID, req.body.tenantID, req.body.newRent)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/endLease', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID && req.body.tenantID) {

		myProperties.endLease(res.locals.uid, req.body.myPropertyID, req.body.tenantID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/cancelCulmination', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID && req.body.tenantID) {

		myProperties.cancelCulmination(res.locals.uid, req.body.myPropertyID, req.body.tenantID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));

myPropertiesRoutes.post('/estimateGasAndGasPriceContractCreation', sharedService.verifyIdToken, ((req, res)=>{

	myProperties.estimateGasAndGasPriceContractCreation(res.locals.uid)
		.then((info)=>res.send({success:true, data: info, error: null}))
		.catch((err)=>res.send({success:false, data: null, error: {message: err}}));


}));


myPropertiesRoutes.post('/createSmartContract', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.myPropertyID && req.body.gasPrice && req.body.gasLimit) {

		myProperties.createSmartContract(res.locals.uid, req.body.myPropertyID, req.body.gasPrice, req.body.gasLimit)
			.then((info)=>res.send({success:true, data: info, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/uploadToIPFS', sharedService.verifyIdToken, ((req,res)=>{
	// Check for required parameters
	if (req.body.url) {
		myProperties.uploadToIPFS(req.body.url)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));




myPropertiesRoutes.post('/estimateGasAndGasPriceDocSaving', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.myPropertyID) {
		myProperties.estimateGasAndGasPriceDocSaving(res.locals.uid, req.body.myPropertyID)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/saveDocOnBlockchain', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.ipfsHash && req.body.myPropertyID && req.body.tenantID && req.body.signatureRequestSelectedID && req.body.gasPrice && req.body.gasLimit) {
		myProperties.saveDocOnBlockchain(
			res.locals.uid, 
			req.body.ipfsHash, 
			req.body.myPropertyID, 
			req.body.tenantID, 
			req.body.signatureRequestSelectedID, 
			req.body.gasPrice, 
			req.body.gasLimit
			)
			.then((info)=>res.send({success:true, data: info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myPropertiesRoutes.post('/startChat', sharedService.verifyIdToken, ((req, res)=>{

	if (req.body.content && req.body.myPropertyID && req.body.tenantID) {
		myProperties.startChat(res.locals.uid, req.body.content, req.body.myPropertyID, req.body.tenantID)
			.then((info)=>res.send({success:true, data:info, error: null}))
			.catch((err) => res.send({success:false, data: null, error: {message: err}}));
	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}
}));







export {myPropertiesRoutes};