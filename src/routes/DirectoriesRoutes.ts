const express = require("express");
import {AccountService} from '../services/User/AccountService';
import {DirectoriesService} from '../services/Directories/DirectoriesService'



/*
* Init
* */
const DirectoriesRoutes = express.Router();

const directoriesService = new DirectoriesService();



DirectoriesRoutes.post('/getTotalAndServiceProviders', ((req, res)=>{

	if (req.body.filter && req.body.filter.lat && req.body.filter.lng && req.body.filter.radius && req.body.offset>=0) { 
		directoriesService.getTotalAndServiceProviders(req.body.filter, req.body.offset)
			.then((response)=>res.send({success:true, data: response, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading Service Providers'}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


DirectoriesRoutes.post('/getTotalAndRealEstateAgents', ((req, res)=>{

	if (req.body.filter && req.body.filter.lat && req.body.filter.lng && req.body.filter.radius && req.body.offset>=0) { 
		directoriesService.getTotalAndRealEstateAgents(req.body.filter, req.body.offset)
			.then((response)=>res.send({success:true, data: response, error: null}))
    	.catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected error loading Service Providers'}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


export {DirectoriesRoutes};