const express = require("express");

import {NotificationsService} from '../services/Notifications/NotificationsService';
import {SharedService} from '../services/Shared/SharedService';


const notificationsService = new NotificationsService();

const sharedService = new SharedService();

const NotificationsRoutes = express.Router();


NotificationsRoutes.post('/provideFeedback', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.notificationID && req.body.feedback) {

		notificationsService.provideFeedback(res.locals.uid, req.body.notificationID, req.body.feedback)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


NotificationsRoutes.post('/confirmPropertyListing', sharedService.verifyIdToken, ((req,res)=>{

	if (req.body.notificationID) {

		notificationsService.confirmPropertyListing(res.locals.uid, req.body.notificationID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


export {NotificationsRoutes};