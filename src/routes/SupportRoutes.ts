const express = require("express");
import {SupportService} from '../services/Support/SupportService';

/*
* Init
* */
const SupportRoutes = express.Router();

//Email Service
const support = new SupportService();


SupportRoutes.post('/contact',((req,res)=>{

	if (req.body.name && req.body.email && req.body.subject && req.body.message) { 
		support.sendMailToSupport(req.body.name,req.body.email,req.body.subject,req.body.message)
			.then(()=> res.send({success:true, data: null, error: null}))
      .catch(err=> res.send({success:false, data: null, error: { message: err}}));
	} else {
		res.send({success:false, data: null, error: { message: 'Missing Parameters'}});
	}

}));


export {SupportRoutes};