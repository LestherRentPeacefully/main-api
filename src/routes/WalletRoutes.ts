const express = require("express");
import {WalletService} from '../services/Wallet/WalletService';
import {AltcoinService} from '../services/Wallet/AltcoinService';
import {SharedService} from '../services/Shared/SharedService';

/*
* Init
* */
const WalletRoutes = express.Router();


//Wallet Service
const walletService = new WalletService();

const altcoinService = new AltcoinService();

/*Shared service*/
const sharedService = new SharedService();




WalletRoutes.post('/create', sharedService.verifyIdToken, ((req, res)=>{

  if (req.body.currency) {
    walletService.create(req.body.currency, res.locals.uid)
      .then((address)=>res.send({success:true, data: address, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));


WalletRoutes.post('/createPersonalWallet', ((req, res)=>{

  if (req.body.currency) {
    walletService.createPersonalWallet(req.body.currency)
      .then((address)=>res.send({success:true, data: address, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));



WalletRoutes.post('/isValidAddress', ((req, res)=>{

  if (sharedService.currencies.includes(req.body.currency)) {
    walletService.isValidAddress(req.body.currency, req.body.address)
      .then((valid)=>res.send({success:true, data: valid, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));


WalletRoutes.post('/getWithdrawalFee', ((req, res)=>{

  if (sharedService.currencies.includes(req.body.currency)) {
    walletService.getWithdrawalFee(req.body.currency)
      .then((fee)=>res.send({success:true, data: fee, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));


WalletRoutes.post('/checkWithdrawalAddress', sharedService.verifyIdToken, ((req, res)=>{

  if (sharedService.currencies.includes(req.body.currency) && req.body.address) {
    walletService.checkWithdrawalAddress(res.locals.uid, req.body.currency, req.body.address)
      .then((info)=>res.send({success:true, data: info, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));


WalletRoutes.post('/sendWithdrawalCode', sharedService.verifyIdToken, ((req, res)=>{

  walletService.sendWithdrawalCode(res.locals.uid)
    .then(()=>res.send({success:true, data: null, error: null}))
    .catch((err) => res.send({success:false, data: null, error: {message: err}}));
}));



WalletRoutes.post('/withdraw', sharedService.verifyIdToken, ((req, res)=>{

  if (sharedService.currencies.includes(req.body.currency) && req.body.code && req.body.to && req.body.amount>0) {
    walletService.withdraw(res.locals.uid, req.body.currency, req.body.code, req.body.to, req.body.amount)
      .then(()=>res.send({success:true, data: null, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Invalid parameters'}})
  }

}));


WalletRoutes.post('/toLegacyAddress', ((req, res)=>{

  if (req.body.address) {
    altcoinService.toLegacyAddress(req.body.address)
      .then((address)=>res.send({success:true, data: address, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));

WalletRoutes.post('/toCashAddress', ((req, res)=>{

  if (req.body.address) {
    altcoinService.toCashAddress(req.body.address)
      .then((address)=>res.send({success:true, data: address, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));

WalletRoutes.post('/transfer', ((req, res)=>{

  if (req.body.currency && req.body.from && req.body.privateKey && req.body.to && req.body.amount) {
    altcoinService.transfer(req.body.currency, req.body.from, req.body.privateKey, req.body.to, req.body.amount)
      .then((address)=>res.send({success:true, data: address, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));
  } else {
    res.send({success:false, data: null, error: {message: 'Missing parameters'}})
  }

}));



export {WalletRoutes};