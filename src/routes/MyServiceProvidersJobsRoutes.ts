const express = require("express");
import {MyServiceProvidersJobsService} from '../services/MyServiceProvidersJobs/MyServiceProvidersJobsService';
import {AccountService} from '../services/User/AccountService';


/*
* Init
* */
const myServiceProvidersJobsRoutes = express.Router();

const myServiceProvidersJobsService = new MyServiceProvidersJobsService();

/*Account service*/
const accountService = new AccountService();



/*Middleware*/
const verifyIdToken = (req,res,next)=>{

	const token = req.get('authorization');

	if (token) {

		accountService.verifyIdToken(token).then((uid)=>{
			res.locals.uid = uid; 
			next();
		
		}).catch(err=>res.send({success:false,data:null,error:{message:err}}));

	} else {
		res.send({success:false,data:null,error:{message:'No token provided'}});
	}
}



myServiceProvidersJobsRoutes.post('/acceptJob', verifyIdToken, ((req,res)=>{

	if (req.body.serviceProviderJobID) {

		myServiceProvidersJobsService.acceptJob(res.locals.uid, req.body.serviceProviderJobID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/cancelJob', verifyIdToken, ((req,res)=>{

	if (req.body.serviceProviderJobID) {

		myServiceProvidersJobsService.cancelJob(res.locals.uid, req.body.serviceProviderJobID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/proposeMilestone', verifyIdToken, ((req,res)=>{

	if (req.body.serviceProviderJobID && req.body.amount>=5 && req.body.details && req.body.details.length<=50) {

		myServiceProvidersJobsService.proposeMilestone(res.locals.uid, req.body.serviceProviderJobID, req.body.amount, req.body.details)
			.then((id)=>res.send({success:true, data: id, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/deleteProposedMilestone', verifyIdToken, ((req, res)=>{

	if (req.body.milestoneID) {

		myServiceProvidersJobsService.deleteProposedMilestone(res.locals.uid, req.body.milestoneID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/getCurrencyPrice', verifyIdToken, ((req,res)=>{

  if (req.body.currency) {
    myServiceProvidersJobsService.getCurrencyPrice(res.locals.uid, req.body.currency)
      .then((response)=>res.send({success:true, data: response, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: 'Unexpected getting price'}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
  }

}));


myServiceProvidersJobsRoutes.post('/createMilestone', verifyIdToken, ((req,res)=>{

  if (req.body.currency && (req.body.milestoneID || (req.body.serviceProviderJobID && req.body.amount>=5 && req.body.details.length<=50))) {
    myServiceProvidersJobsService.createMilestone(res.locals.uid, req.body.currency, req.body.milestoneID, req.body.serviceProviderJobID, req.body.amount, req.body.details)
      .then((id)=>res.send({success:true, data: id, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));


myServiceProvidersJobsRoutes.post('/cancelCreatedMilestone', verifyIdToken, ((req, res)=>{

	if (req.body.milestoneID) {

		myServiceProvidersJobsService.cancelCreatedMilestone(res.locals.uid, req.body.milestoneID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/releaseMilestone', verifyIdToken, ((req, res)=>{

	if (req.body.milestoneID && req.body.serviceProviderJobID) {

		myServiceProvidersJobsService.releaseMilestone(res.locals.uid, req.body.milestoneID, req.body.serviceProviderJobID)
			.then(()=>res.send({success:true, data: null, error: null}))
    	.catch((err)=>res.send({success:false, data: null, error: {message: err}}));

	} else {
		res.send({success:false, data: null, error: {message: 'Missing Parameters'}});
	}

}));


myServiceProvidersJobsRoutes.post('/sendPayment', verifyIdToken, ((req,res)=>{

  if (req.body.currency && req.body.serviceProviderJobID && req.body.amount>=5 && req.body.details.length<=50) {
    myServiceProvidersJobsService.sendPayment(res.locals.uid, req.body.currency, req.body.serviceProviderJobID, req.body.amount, req.body.details)
      .then((id)=>res.send({success:true, data: id, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));


myServiceProvidersJobsRoutes.post('/provideFeedBack', verifyIdToken, ((req,res)=>{

  if (req.body.serviceProviderJobID && req.body.feedback) {
    myServiceProvidersJobsService.provideFeedBack(res.locals.uid, req.body.serviceProviderJobID, req.body.feedback)
      .then(()=>res.send({success:true, data: null, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));


myServiceProvidersJobsRoutes.post('/startChat', verifyIdToken, ((req,res)=>{

  if (req.body.serviceProviderJobID && req.body.content) {
    myServiceProvidersJobsService.startChat(res.locals.uid, req.body.serviceProviderJobID, req.body.content)
      .then((ids)=>res.send({success:true, data: ids, error: null}))
      .catch((err) => res.send({success:false, data: null, error: {message: err}}));

  } else {
    res.send({success:false, data: null, error: {message: 'Invalid Parameters'}});
  }

}));

export {myServiceProvidersJobsRoutes};