import * as admin from "firebase-admin";
const sql = require('mssql');
import {Database} from '../Database';
import {BigNumber} from 'bignumber.js';
import {SharedService} from '../Shared/SharedService';
import {EthereumService} from '../Wallet/EthereumService';
import {AltcoinService} from '../Wallet/AltcoinService';
import {environment} from '../../environments/environment';
// import {Nodemailer} from '../Email/Nodemailer';

const ethereumService = new EthereumService();

const altcoinService = new AltcoinService();

const shared = new SharedService();



export class RequestService{

	rejectIDRequest(aid:string, rejectionReason:string, requestID:string, role:string, uid:string):Promise<any>{
    const db = admin.firestore();
    const batch = db.batch();

    batch.delete(db.doc(`admin/requests/verifications/${requestID}`));

    if (role=='real estate agent') { 
      batch.update(db.doc(`users/${uid}/requests/realEstateAgentIDVerification`),{
        status:'rejected',
        rejectionReason
      });
    } else {
      batch.update(db.doc(`users/${uid}/requests/IDVerification`),{
        status:'rejected',
        rejectionReason
      });
		}

		const notifId = shared.generateId();
		
		batch.update(db.doc(`users/${uid}/settings/preferences`), {
			'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
		});

		batch.set(db.doc(`users/${uid}/notifications/${notifId}`), {
			title: `ID verification rejected`,
			content: `Reason: ${rejectionReason}`,
			id: notifId,
			creationTS: Date.now(),
		});

    return batch.commit();
  }



	public verifyIDRequest(uid:string, fields:any, id:string, role:string):Promise<any>{
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				batch.delete(db.doc(`admin/requests/verifications/${id}`));
		
				if (role=='real estate agent') {
					// batch.update(db.doc(`public/${uid}`),{
					// 	'status.realEstateAgentIDVerified':true,
					// 	firstName: fields.firstName.toLowerCase(),
					// 	lastName: fields.lastName.toLowerCase(),
					// 	realEstateAgent:{
					// 		brokerage:fields.brokerage,
					// 		licenceNo:fields.licenceNo
					// 	}
					// });
		
					// batch.update(db.doc(`private/${uid}`),{
					// 	'status.realEstateAgentIDVerified':true,
					// 	firstName: fields.firstName.toLowerCase(),
					// 	lastName: fields.lastName.toLowerCase()
					// });
		
					batch.update(db.doc(`users/${uid}`),{
						'status.realEstateAgentIDVerified':true,
						firstName: fields.firstName.toLowerCase(),
						lastName: fields.lastName.toLowerCase(),
						'realEstateAgent.brokerage': fields.brokerage,
						'realEstateAgent.licenceNo': fields.licenceNo
					});
		
					batch.update(db.doc(`users/${uid}/requests/realEstateAgentIDVerification`),{
						status:'verified',
						firstName: fields.firstName.toLowerCase(),
						lastName: fields.lastName.toLowerCase(),
						brokerage:fields.brokerage,
						licenceNo:fields.licenceNo,
						dateOfBirth:fields.dateOfBirth,
						country:fields.country,
						IDType:fields.IDType,
						IDExpirationDate:fields.IDExpirationDate,
					});

						const request = Database.pool.request();

						let SQLcommand = 
							 `IF (SELECT COUNT(*) FROM realEstateAgents WHERE uid = '${uid}') = 1
									BEGIN
										DELETE FROM realEstateAgents WHERE uid = '${uid}';
									END
								
								INSERT INTO realEstateAgents ( [uid], [status], [score], [reviews], [totalRented], [totalTenants], [totalLandlords] ) 
									VALUES ( '${uid}', 'verified', 0, 0, 0, 0, 0 )`;
						
						await request.query(SQLcommand);
		
		
				} else {

					// batch.update(db.doc(`public/${uid}`),{
					// 	'status.IDVerified':true,
					// 	firstName: fields.firstName.toLowerCase(),
					// 	lastName: fields.lastName.toLowerCase(),
					// });
		
					// batch.update(db.doc(`private/${uid}`),{
					// 	'status.IDVerified':true,
					// 	firstName: fields.firstName.toLowerCase(),
					// 	lastName: fields.lastName.toLowerCase(),
					// });
		
					batch.update(db.doc(`users/${uid}`),{
						'status.IDVerified':true,
						firstName: fields.firstName.toLowerCase(),
						lastName: fields.lastName.toLowerCase(),
					});
		
					batch.update(db.doc(`users/${uid}/requests/IDVerification`),{
						status:'verified',
						firstName: fields.firstName.toLowerCase(),
						lastName: fields.lastName.toLowerCase(),
						dateOfBirth: fields.dateOfBirth,
						country: fields.country,
						IDType: fields.IDType,
						IDExpirationDate: fields.IDExpirationDate,
					});
					
					if(role=='service provider'){
						let insertString = '[uid], [status], [score], [reviews], [jobsCompleted]';
						let valuesString = '@uid, @status, @score, @reviews, @jobsCompleted';
						let request = Database.pool.request()
							.input('uid', sql.NVarChar(50), uid)
							.input('status', sql.NVarChar(50), 'verified')
							.input('score', sql.Float, 0)
							.input('reviews', sql.Int, 0)
							.input('jobsCompleted', sql.Int, 0)

						let SQLcommand = 
							 `IF (SELECT COUNT(*) FROM serviceProviders WHERE uid = @uid) = 1
									BEGIN
										DELETE FROM serviceProviders WHERE uid = @uid;
									END
								
								INSERT INTO serviceProviders ( ${insertString} ) VALUES ( ${valuesString} )`;
						
						await request.query(SQLcommand);
					}

				}

				const notifId = shared.generateId();

				batch.update(db.doc(`users/${uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});

				batch.set(db.doc(`users/${uid}/notifications/${notifId}`), {
					title: `ID verified`,
					content: `Your ID has been verified successfully`,
					id: notifId,
					creationTS: Date.now(),
				});


				await batch.commit();
				resolve();
				
			} catch (err) {
				reject('Unexpected error verying request');
			}

		});


	}



	public rejectListing(aid:string, listingID:string, rejectionReason:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				
				const listing = (await db.doc(`listings/${listingID}`).get()).data();

				batch.update(db.doc(`listings/${listingID}`), {status:'rejected', rejectionReason});
				
				const notifId = shared.generateId();
		
				batch.update(db.doc(`users/${listing.user.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});
		
				batch.set(db.doc(`users/${listing.user.uid}/notifications/${notifId}`), {
					title: `Listing rejected`,
					content: `Reason: ${rejectionReason}`,
					id: notifId,
					creationTS: Date.now(),
				});

				await batch.commit();
				resolve();
				
			} catch (err) {
				console.log(err.message || err);
				reject('Unexpected error verying listing');
			}
		});
  }



	public verifyListing(uid:string, listingID:string, fields:any):Promise<any>{
		return new Promise(async (resolve, reject)=>{
			try{
				const db = admin.firestore();
				const batch = db.batch();
				let [propertyDoc, userDoc] = 	await db.getAll(db.doc(`listings/${listingID}`),	db.doc(`users/${uid}`));
				let userInfo = userDoc.data();
				let listing = propertyDoc.data();

				if (listing.user.uid !== uid) {
					reject('User is not the owner of this listing'); return;
				}


				let insertString = '[id], [lat], [lng], [type], [numOfBeds], [numOfBaths], [sqft], [rentAmount], [securityDepositAmount], [availableFrom], [creationTS], [uid], [role], [status], [recommendedUntil]';
				let valuesString = '@id, @lat, @lng, @type, @numOfBeds, @numOfBaths, @sqft, @rentAmount, @securityDepositAmount, @availableFrom, @creationTS, @uid, @role, @status, @recommendedUntil';

				fields.location.geopoint = new admin.firestore.GeoPoint(fields.location.lat, fields.location.lng);

				let request = Database.pool.request()
												.input('id', sql.NVarChar(50), listing.id)
												.input('lat', sql.Float, fields.location.lat)
												.input('lng', sql.Float, fields.location.lng)
												.input('type', sql.NVarChar(50), fields.type)
												.input('numOfBeds', sql.NVarChar(50), fields.numOfBeds)
												.input('numOfBaths', sql.NVarChar(50), fields.numOfBaths)
												.input('sqft', sql.Float, fields.sqft)
												.input('rentAmount', sql.Float, fields.rentAmount)
												.input('securityDepositAmount', sql.Float, fields.securityDepositAmount)
												.input('availableFrom', sql.BigInt, fields.availableFrom)
												.input('creationTS', sql.BigInt, listing.creationTS)
												.input('uid', sql.NVarChar(50), listing.user.uid)
												.input('role', sql.NVarChar(50), listing.user.role)
												.input('status', sql.NVarChar(50), 'listed')
												.input('recommendedUntil', sql.BigInt, 0)
				
				for(let amenity of Object.keys(fields.amenities)){
					if(fields.amenities[amenity]){
						request = request.input(amenity=='view'? '_view' : amenity, sql.Bit, 1);
						insertString +=`, [${amenity=='view'? '_view' : amenity}]`;
						valuesString +=`, @${amenity=='view'? '_view' : amenity}`;
					}
				}

				let SQLcommand = 
				 `IF (SELECT COUNT(*) FROM listings WHERE id = @id) = 1
						BEGIN
							DELETE FROM listings WHERE id = @id;
						END
					
					INSERT INTO listings ( ${insertString} ) VALUES ( ${valuesString} )`;
				
				delete fields.location.lat, delete fields.location.lng;

				batch.update(db.doc(`listings/${listingID}`), {
					name:fields.name,
					address:fields.address,
					location:fields.location,
					unit:fields.unit || '',
					type:fields.type,
					numOfBeds:fields.numOfBeds,
					numOfBaths:fields.numOfBaths,
					sqft:fields.sqft,
					rentAmount:fields.rentAmount,
					securityDepositAmount:fields.securityDepositAmount,
					details:fields.details,
					availableFrom:fields.availableFrom,
					amenities:fields.amenities,
					status:'listed'
				});

				const notifId = shared.generateId();
		
				batch.update(db.doc(`users/${listing.user.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});
		
				batch.set(db.doc(`users/${listing.user.uid}/notifications/${notifId}`), {
					title: `Listing verified`,
					content: `Listing verified successfully. Now is publicly visible`,
					id: notifId,
					creationTS: Date.now(),
				});

				await request.query(SQLcommand);
				await batch.commit();
				resolve();
				
			}catch(err){
				console.log(err.message || err);
				reject('Unexpected error verying listing');
			}
		});
	}


	public rejectWithdrawalRequest(aid:string, withdrawalID:string, rejectionReason:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = shared.generateId();
				
				let withdrawal = (await db.doc(`transactions/${withdrawalID}`).get()).data();
				
				let [balanceSnapshot] = await Promise.all([
					db.collection(`balances`).where('uid','==',withdrawal.uid).where('currency','==',withdrawal.currency).get()
				]);

				let balance = balanceSnapshot.docs[0].data();

				if(withdrawal.status!='pending'){
					reject('Transaction is not pending'); return;
				}
				if(withdrawal.type!='withdrawal'){
					reject('Transaction is not a withdrawal'); return;
				}

				batch.delete(db.doc(`transactions/${withdrawalID}`));
				
				batch.update(db.doc(`users/${withdrawal.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});

				const amountToPay = new BigNumber(withdrawal.amount).plus(withdrawal.fee).toString(10);

				batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).plus(amountToPay).toString(10)
        });

				batch.set(db.doc(`users/${withdrawal.uid}/notifications/${id}`), {
					title: `Withdrawal request rejected`,
					content: `Your withdrawal request has been reject. Reason: ${rejectionReason}`,
					id,
					creationTS: Date.now(),
				});
				
				await batch.commit();
				resolve();
				
			} catch (errr) {
				reject('Unexpected error. Try again later');
			}
		});
	}


	public processWithdrawalRequest(aid:string, withdrawalID:string, txHash:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = shared.generateId();
	
				let withdrawal = (await db.doc(`transactions/${withdrawalID}`).get()).data();

				const totalUsersBalance = (await db.doc(`totalUsersBalances/${withdrawal.currency}`).get()).data();
	
				if(withdrawal.status!='pending'){
					reject('Transaction is not pending'); return;
				}
				if(withdrawal.type!='withdrawal'){
					reject('Transaction is not a withdrawal'); return;
				}

				batch.update(db.doc(`transactions/${withdrawalID}`), {
					status:'confirmed',
					txHash
				});

				const amountToPay = new BigNumber(withdrawal.amount).plus(withdrawal.fee).toString(10);
				
				batch.update(db.doc(`totalUsersBalances/${withdrawal.currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
				});

				batch.update(db.doc(`users/${withdrawal.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});

				batch.set(db.doc(`users/${withdrawal.uid}/notifications/${id}`), {
					title: `Withdrawal request processed`,
					content: `Your withdrawal request for ${withdrawal.amount} ${withdrawal.currency} has been processed`,
					id,
					creationTS: Date.now(),
				});

				await batch.commit();
				resolve();
				
			} catch (err) {
				reject('Unexpected error. Try again later');
			}

		});
	}




	public estimatePlaformWithdrawalFee(currency: string, to: string, amount: string){
    return new Promise(async(resolve, reject)=>{
      try {

				let isValidAddress: boolean;

				if (currency=='ETH') isValidAddress = ethereumService.isValidAddress(to);
				else isValidAddress = await altcoinService.isValidAddress(currency, to);
				
				if(!isValidAddress) {
					reject('Invalid address'); return;
				}

        let response;

        if(currency === 'ETH'){
          response = await ethereumService.estimateGasAndGasPrice(environment.blockchain.ETH.securePlatformWallet.address, to, ethereumService.toWei(amount))
        } else {
          response = await altcoinService.estimatePlaformWithdrawalFee(currency, to, amount);
        }
        
        resolve(response);
        
      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }





}