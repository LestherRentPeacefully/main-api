const sgMail = require('@sendgrid/mail');
import {environment} from '../../environments/environment';

sgMail.setApiKey(environment.sengridPrivateKey);

/*const msg = {
  to: 'lesther993@gmail.com',
  from: `${environment.noreplyEmail}`,
  subject: 'Sending with SendGrid is Fun',
  text: 'and easy to do anywhere, even with Node.js',
  html: '<strong>and easy to do anywhere, even with Node.js!!</strong>',
};*/

export class SendGrid{

	private message:any;

	constructor(msg:{from:string,to:string,subject:string,html?:string,text?}){
		this.message = msg;
	}

	public sendMail():Promise<any>{
		return new Promise((resolve,reject)=>{

			sgMail.send(this.message).then(()=>{
				resolve();
			}).catch(err=>{
				// console.log(String(err)); 
				//console.log(err.message); 
				//console.log(err.code);
				reject(err.message);
			});
		});
		
	}


}