const nodemailer = require('nodemailer');
import {environment} from '../../environments/environment';


export class Nodemailer{
	private message:any;

	constructor(msg:{from:string,to:string,subject:string,html?:string,text?}){
		this.message = msg;
		// this.message.bcc = 'pramod.blockchain@gmail.com';
	}

	public sendMail():Promise<any>{
		let transporter = nodemailer.createTransport(environment.smtpConfig);
		return new Promise((resolve,reject)=>{
			transporter.sendMail(this.message,(err, info)=>{
				if(err) {
					reject(err.message); return;
				}
				resolve(true);
    	});
		});
	}


}