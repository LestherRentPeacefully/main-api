const https = require("https");
// import * as ethereumjsWallet from 'ethereumjs-wallet';
const ethereumjsWallet = require('ethereumjs-wallet');
import * as admin from "firebase-admin";


export class SettingsService{

	public resetPassword(currentPassword:string, newPassword:string, email:string){
		const db = admin.firestore();
		return new Promise(async (resolve,reject)=>{
			try{
				let response = await this.handleRequest({
		      host: 'www.googleapis.com',
		      path: '/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyCPaIsy18OhydzE1FDI7mEKd28qaNSbULI', 
		      method: 'POST',
		      headers: {
		      'Content-Type': 'application/json'
		      }
			    },{
			    	email,
			    	password:currentPassword,
			    	returnSecureToken:false
			    });

		    if (!response.error) {
					let uid = response.localId;
					let identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get()).docs[0].data();
					let privateKey = identityWallet.privateKey;
					let generatedWallet = ethereumjsWallet.fromPrivateKey(Buffer.from(privateKey, 'hex'));
					if (!generatedWallet) {
						reject('Unexpected error ocurred re-encrypting wallet. Try again later'); return;
					}
					let V3 = generatedWallet.toV3(newPassword);
					await admin.auth().updateUser(uid, {password:newPassword});
					await db.doc(`wallets/identity/private/${identityWallet.id}`).update({walletFile:V3});
					resolve();

		    } else {
		    	switch (response.error.message) {
		    		case "EMAIL_NOT_FOUND":
		    			reject('Email not found');
	    			break;
	    			case "INVALID_PASSWORD":
						reject('The current password is invalid');
					break;
		    		case "INVALID_EMAIL":
		    			reject('Invalid email');
	    			break;
		    		default:
		    			reject('Unexpected error occurred. Try again later');
		    		break;
		    	}
		    }

			}catch(err){
				reject('Unexpected error occured. Try again later');
			}
  	});
	}


	private handleRequest(options: any, info?:any): Promise<any> {
    return new Promise((resolve,reject)=>{
    	try{
        const req = https.request(options, (response)=>{
            let data = '';
            response.on('data',  (chunk)=> {
                data += chunk;
            });
            response.on('end',  ()=> {
                if (options.method =='DELETE') {
                    resolve(true);
                }else{
                    try{
                        resolve(JSON.parse(data));
                    }catch(err){
                        reject(err);
                    }
                }
            });
            response.on('error',(e) =>{
                reject(e);
            })
        });
        if(info!=undefined)
            req.write(JSON.stringify(info));
        req.end();

    	}catch(err){
    		reject(err);
    	}
    });
   }


}