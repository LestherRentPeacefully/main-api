import * as admin from "firebase-admin";
const ethereumjsWallet = require('ethereumjs-wallet');
import {EthereumService} from '../Wallet/EthereumService';
import {SharedService} from '../Shared/SharedService';
import {environment} from '../../environments/environment';
import {Nodemailer} from "../Email/Nodemailer";



const sharedService = new SharedService();

const ethereumService = new EthereumService();


export class AccountService {
	
	public createAccount(role:string, firstName:string, lastName:string, email: string, password: string, brokerage?: string, licenceNo?: string):Promise<any>{
    return new Promise(async (resolve, reject)=>{
    
      const db = admin.firestore();
      const wallet = await ethereumService.create(password);

      const emailCode = sharedService.generateId();
      const id1 = sharedService.generateId();
      const id2 = sharedService.generateId();

      admin.auth().createUser({	email, password	}).then(async firebaseUser=>{

        const batch = db.batch();

        const roleObj = {
          tenant:{
            score:0,
            reviews:0,
            currentlyRented:0,
            totalRented:0,
            ontimePayments:0,
            employer:{
              reviews:0,
              activeJobs:0,
              serviceProvidersHired:0,
              paymentPromptness:0
            }
          },
          landlord:{
            score:0,
            reviews:0,
            currentlyListed:0,
            currentTenants:0,
            totalTenants:0,
            totalRented:0,
            avgTimeOfTicketResolution:0,
            employer:{
              reviews:0,
              activeJobs:0,
              serviceProvidersHired:0,
              paymentPromptness:0
            }
          },
          serviceProvider:{
            score:0,
            reviews:0,
            jobsCompleted:0,
            activeJobs:0,
            qualityOfWork:0,
            finishedOnTime:0
          },
          realEstateAgent:{
            score:0,
            reviews:0,
            currentlyListed:0,
            totalTenants:0,
            totalLandlords:0,
            totalRented:0,
            brokerage:brokerage || null,
            licenceNo:licenceNo || null,
          }
        }

        const userObj = {
          firstName: firstName.toLowerCase(),
          lastName: lastName.toLowerCase(),
          email:email.toLowerCase(),
          creationTS: Date.now(),
          uid:firebaseUser.uid,
          status:{
            banned: false,
            emailVerified: false,
            IDVerified: false,
            realEstateAgentIDVerified:false
          },
        }

        const membership = {
          pricingPlan:'free',
          // nextPayment: sharedService.addMonths(new Date(), 1).getTime()
        }
        
        batch.set(db.doc(`users/${firebaseUser.uid}`), {
          ...userObj,
          ...roleObj,
          membership
        });
        
        // batch.set(db.doc(`public/${firebaseUser.uid}`),{
        //   ...userObj,
        //   ...roleObj,
        //   membership
        // });

        // batch.set(db.doc(`private/${firebaseUser.uid}`),{
        //   ...userObj,
        // });

        batch.set(db.doc(`users/${firebaseUser.uid}/notifications/${id2}`), {
          title: `Welcome, ${sharedService.capitalize(firstName.toLowerCase())}`,
          content: `On RentPeafully, you can list and rent properties, room, etc. You can also hire services providers for that property 🏠. Cryptocurrencies is used for all payments`,
          id: id2,
          creationTS: Date.now(),
        });

        batch.set(db.doc(`users/${firebaseUser.uid}/settings/preferences`), {
          role:role.toLowerCase(),
          counters:{
            unreads:{
              notifications:1,
              messages:0
            }
          }
        });

        batch.set(db.doc(`users/${firebaseUser.uid}/profile/editable`), {
          shortDescription: `Nice to meet you! I'm ${firstName} ${lastName}`,
          aboutMe: `I'm a great RentPeacefully user`,
          social:{
            website:{
              url:null,
              enabled:false
            },
            facebook:{
              url:null,
              enabled:false,
            },
            twitter:{
              url:null,
              enabled:false
            },
            instagram:{
              url:null,
              enabled:false
            },
            linkedin:{
              url:null,
              enabled:false
            },
          }      
        });

        batch.set(db.doc(`wallets/identity/public/${id1}`),{
          currency:'ETH',
          uid:firebaseUser.uid,
          id:id1,
          searchable:wallet.address.toLowerCase(),
          address:wallet.address
        });

        batch.set(db.doc(`wallets/identity/private/${id1}`),{
          currency:'ETH',
          uid:firebaseUser.uid,
          id:id1,
          searchable:wallet.address.toLowerCase(),
          address:wallet.address,
          walletFile:wallet.V3,
          privateKey:wallet.privateKey
        });
        
        batch.set(db.doc(`emailVerificationCodes/${emailCode}`),{
          uid: firebaseUser.uid,
          expirationDate: new Date(new Date().setHours(new Date().getHours()+24)).getTime()
        });

        await batch.commit();
        
        this.sendEmailVerificationCode(email.toLowerCase(), emailCode);
        resolve(firebaseUser.uid);

      }).catch((err)=> {
        if(err.code === 'auth/invalid-email'){ reject('Invalid email'); return; }
        if(err.code === 'auth/invalid-password'){ reject('Invalid password'); return; }
        if(err.code === 'auth/email-already-exists'){ reject('Email already exists'); return; }
        reject('An error has ocurred. Try again later');
      });

		});
  }
  

  public resendAndUploadEmailVerificationCode(email: string):Promise<any>{
    return new Promise(async(resolve, reject)=>{
      const db = admin.firestore();
      const batch = db.batch();
      try {
        const userSnapshot = (await db.collection('users').where('email','==',email.toLowerCase()).limit(1).get());
        
        if(userSnapshot.empty) {
          reject(`User's email not found`); return;
        }
  
        const userInfo = userSnapshot.docs[0].data();
  
        const emailCode = sharedService.generateId();
  
        const querySnapshot = await db.collection('emailVerificationCodes').where('uid','==',userInfo.uid).limit(1).get();

        if(!querySnapshot.empty){
          batch.delete(db.doc(`emailVerificationCodes/${querySnapshot.docs[0].id}`));
        }
        batch.set(db.doc(`emailVerificationCodes/${emailCode}`),{
          uid: userInfo.uid,
          expirationDate: new Date(new Date().setHours(new Date().getHours()+24)).getTime()
        });
        
        await batch.commit();

        this.sendEmailVerificationCode(email,emailCode);
        resolve();
        
      } catch (err) {
        reject('An error has ocurred. Try again later');
      }

		});	
  }
  

  
  private sendEmailVerificationCode(to:string, code:string){
		const msg = {
      from:`${environment.appName} <${environment.noreplyEmail}>`,
      to,
      subject:`Welcome to ${environment.appName}! Confirm your Email`,
      html:`Access the url to confirm your email: ${environment.mainAppURL}/account/verify-email?code=${code}`
    };
		const nodemailer = new Nodemailer(msg);
		return nodemailer.sendMail();
  }
 

  public sendAndUploadResetPasswordCode(email: string): Promise<boolean>{
    return new Promise(async (resolve, reject)=>{
      try{
        const db = admin.firestore();
        const batch = db.batch();
        
        const userSnapshot = (await db.collection('users').where('email','==',email.toLowerCase()).limit(1).get());

        if(userSnapshot.empty) {
          reject(`User's email not found`); return;
        }
        const userInfo = userSnapshot.docs[0].data();
        
        const querySnapshot = await db.collection('resetPasswordCodes').where('uid','==',userInfo.uid).limit(1).get();
        
        const passwordCode = sharedService.generateId();
        
        const msg = {
          from:`${environment.appName} <${environment.noreplyEmail}>`,
          to:email,
          subject:'Account restoration',
          html:`<p> Access the link to reset your password ${environment.mainAppURL}/account/reset-password?code=${passwordCode}<p>.
                <p>Your Wallet File will be re-encrypted with your new user password.<p>`
        };

        const nodemailer = new Nodemailer(msg);

				if(!querySnapshot.empty){
     			batch.delete(db.doc(`resetPasswordCodes/${querySnapshot.docs[0].id}`));
				}
       	batch.set(db.doc(`resetPasswordCodes/${passwordCode}`),{
					uid: userInfo.uid,
					expirationDate: new Date(new Date().setHours(new Date().getHours()+24)).getTime() 
				});

       	await batch.commit();
				await nodemailer.sendMail();

        resolve();
        
			}catch(err){
				reject('Unexpected error generating reset password link');
			}
		});	
	}
  

  public verifyEmail(code:string):Promise<any>{
    return new Promise(async(resolve, reject) => {
      try {
        const db = admin.firestore();
        const doc = (await db.doc(`emailVerificationCodes/${code}`).get());
        if (!doc.exists) {
          reject(`User's code not found`);return;
        }
  
        if (doc.data().expirationDate < Date.now()) {
          reject(`Code has expired. Request a new one`); return;
        }

        const uid = doc.data().uid;
        const batch = db.batch();
  
        batch.update(db.doc(`users/${uid}`),{
        'status.emailVerified':true
        });
        // batch.update(db.doc(`public/${uid}`),{
        //   'status.emailVerified':true
        // });
        // batch.update(db.doc(`private/${uid}`),{
        //   'status.emailVerified':true
        // });

        const querySnapshot = await db.collection('emailVerificationCodes').where('uid','==',uid).limit(1).get();

        if(!querySnapshot.empty){
          batch.delete(db.doc(`emailVerificationCodes/${querySnapshot.docs[0].id}`));
        }

        await batch.commit()
        resolve();
       
        
      } catch (err) {
        reject('An error has ocurred. Try again later');
      }
  	});
  }

  public resetPasswordWithCode(code:string, password:string): Promise<any>{
    const db = admin.firestore();
    return new Promise(async (resolve, reject) => {
      try{
        const batch = db.batch();
        const doc = await db.doc(`resetPasswordCodes/${code}`).get();

        if (!doc.exists) {
          reject(`User's code not found`); return;
        }
        if (doc.data().expirationDate>=Date.now()) {
          reject(`Code has expired. Request a new one`); return;
        }

        const uid = doc.data().uid;

        const [identityWalletSnapshot, querySnapshot] = await Promise.all([
          db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get(),
          db.collection('resetPasswordCodes').where('uid','==',uid).limit(1).get()
        ]);

        const identityWallet = identityWalletSnapshot.docs[0].data();
        const privateKey = identityWallet.privateKey;

        const generatedWallet = ethereumjsWallet.fromPrivateKey(Buffer.from(privateKey, 'hex'));

        if (!generatedWallet) {
          reject('Unexpected error ocurred re-encrypting wallet. Try again later'); return;
        }

        const V3 = generatedWallet.toV3(password);

        if(!querySnapshot.empty){
          batch.delete(db.doc(`resetPasswordCodes/${querySnapshot.docs[0].id}`));
        }

        batch.update(db.doc(`wallets/identity/private/${identityWallet.id}`), {walletFile:V3});
        const firebaseUser = await admin.auth().updateUser(uid, {password});
        
        await batch.commit();
        resolve(firebaseUser.email);


      }catch(err){
        reject('Unexpected error verifying link. Try again later');
      }
    });
  }  


  public verifyIdToken(token:string):Promise<string>{
    return new Promise((resolve,reject)=>{
      admin.auth().verifyIdToken(token)
        .then(decodedToken=>resolve(decodedToken.uid))
        .catch(err=>reject('Error verifying ID, try again later'));
    });
  }



}

