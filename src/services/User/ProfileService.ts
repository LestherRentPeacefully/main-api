import * as admin from "firebase-admin";
import * as sql from 'mssql'
import {Database} from '../Database';
import {serviceProviderSkills} from './Skills';
import { SharedService } from "../Shared/SharedService";
import { environment } from '../../environments/environment';
import {Nodemailer} from "../Email/Nodemailer";


const sharedService = new SharedService();

export class ProfileService {

	public uploadProfilePicture(uid:string, photo:string, oldFiles:string[]){
		const db = admin.firestore();
		const batch = db.batch();

		batch.update(db.doc(`users/${uid}`), {photo});
		// batch.update(db.doc(`public/${uid}`),{
		// 	photo:photo
		// });
		
		if(oldFiles && oldFiles.length>0) this.deleteFiles(oldFiles).catch(e=>'');
		return batch.commit();
	}


	public addAreaServing(uid:string, role:string, name:string, lat:number, lng:number, geohash:string){
		return new Promise(async (resolve,reject)=>{
			try {
				const db = admin.firestore();
				const id = sharedService.generateId();

				const [userSnapshot, recordSnapshot] = await Promise.all([
					db.doc(`users/${uid}`).get(),
					db.collection(`users/${uid}/areasServing`)
						.where('role','==',role)
						.where('name','==',name)
						.limit(1).get(),
				]);

				const recordExists = !recordSnapshot.empty;
				const userInfo = userSnapshot.data();

				if(role === 'service provider' && !userInfo.status.IDVerified){
					reject('Please, verify your ID'); return;
				}
				if(role === 'real estate agent' && !userInfo.status.realEstateAgentIDVerified){
					reject('Please, verify your ID'); return;
				}

				if(recordExists) {
					reject('Area already exists'); return;
				}
	
				let obj = {
					name,
					role,
					geohash,
					geopoint: new admin.firestore.GeoPoint(lat, lng),
					id,
					creationTS: Date.now()
				};

				let insertString = '[uid], [name], [role], [lat], [lng], [id], [creationTS]';
				let valuesString = '@uid, @name, @role, @lat, @lng, @id, @creationTS';

				let request = Database.pool.request()
					.input('uid', sql.NVarChar(50), uid)
					.input('name', sql.NVarChar(50), obj.name)
					.input('role', sql.NVarChar(50), obj.role)
					.input('lat', sql.Float, lat)
					.input('lng', sql.Float, lng)
					.input('id', sql.NVarChar(50), obj.id)
					.input('creationTS', sql.BigInt, obj.creationTS)

				let SQLcommand = 
					`IF (SELECT COUNT(*) FROM areasServing WHERE uid=@uid AND role=@role AND name = @name) = 1
						BEGIN
							DELETE FROM areasServing WHERE uid=@uid AND role=@role AND name = @name;
						END
					
					INSERT INTO areasServing ( ${insertString} ) VALUES ( ${valuesString} )`;

				await request.query(SQLcommand);
				await db.doc(`users/${uid}/areasServing/${id}`).set(obj);
				resolve(id);


				
			} catch (err) {
				reject('Unexpect error adding area');
			}

		});
	}


	public deleteAreaServing(uid:string, id:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				
				let request = Database.pool.request()
					.input('uid', sql.NVarChar(50), uid)
					.input('id', sql.NVarChar(50), id);

				let SQLcommand = 
					`IF (SELECT COUNT(*) FROM areasServing WHERE uid=@uid AND id=@id) = 1
						BEGIN
							DELETE FROM areasServing WHERE uid=@uid AND id=@id;
						END`;

				await request.query(SQLcommand);
				await db.doc(`users/${uid}/areasServing/${id}`).delete();
				resolve();

			}catch(err){
				reject('Unexpect error deleting area');
			}
		});
	}

	public editSkills(uid:string, role:string, skills:any){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const userInfo = (await db.doc(`users/${uid}`).get()).data();

				if (role=='service provider') {
					
					if(!userInfo.status.IDVerified){
						reject('Please, verify your ID'); return;
					}

					let snapshot = await db.doc(`users/${uid}/profile/serviceProvider`).get();
					let _serviceProviderSkills = serviceProviderSkills;
					let request = Database.pool.request().input('uid', sql.NVarChar(50), uid);
					let updateString = '';
					
					for(let skill of _serviceProviderSkills){
						request = request.input(skill.id, sql.Bit, skills[skill.id]? 1 : 0);
						updateString += `, [${skill.id}] = @${skill.id}`;
					}
					updateString = updateString.substring(2);
					
					let SQLcommand = `UPDATE serviceProviders SET ${updateString} WHERE uid = @uid`;

					await request.query(SQLcommand);

					if (snapshot.exists) {
						await db.doc(`users/${uid}/profile/serviceProvider`).update({skills});
					} else {
						await db.doc(`users/${uid}/profile/serviceProvider`).set({skills});
					}
					resolve();
					
				} else {
					resolve();
				}
			}catch(err){
				reject('Unexpect error adding skills');
			}
		});
	}


	public hireMe(employerID:string, role:string, serviceProviderID:string, name:string, details:string, price:number, skillsRequired){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const [employerInfo, serviceProviderInfo] = (await db.getAll(
					db.doc(`users/${employerID}`),
					db.doc(`users/${serviceProviderID}`)
				)).map(doc => doc.data());
				
					
				if(employerID === serviceProviderID) {
					reject("You can't hire yourself"); return;
				}

				if(!serviceProviderInfo.status.IDVerified) {
					reject('Request the Service Provider to verify his/her Identity'); return;
				}

				if(!employerInfo.status.IDVerified) {
					reject('Plese, verify your Identity'); return;
				}

				const id = sharedService.generateId();

				batch.set(db.doc(`myServiceProvidersJobs/${id}`), {
					name,
					details,
					price:price.toString(),
					skillsRequired,
					status:'wating for acceptance',
					creationTS: Date.now(),
					amountPaid:'0',
					id,
					employer:{
						firstName:employerInfo.firstName,
						lastName:employerInfo.lastName,
						email:employerInfo.email,
						uid:employerInfo.uid,
						role:role
					},
					serviceProvider:{
						firstName:serviceProviderInfo.firstName,
						lastName:serviceProviderInfo.lastName,
						email:serviceProviderInfo.email,
						uid:serviceProviderInfo.uid
					}
				});

				await batch.commit();
				resolve(id);



			} catch (err) {
				reject('Unexpected error hiring Service Provider');
			}
		});
	}



	contactMe(agentId: string, message:string, email:string, name:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();

				const userInfo = (await db.doc(`users/${agentId}`).get()).data();


				batch.set(db.doc(`users/${agentId}/notifications/${id}`), {
					title: `You have a message about your Real Estate Agent profile`,
					content: `${name} emailed you. Check your email box!`,
					id,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${agentId}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});


				let msg = {
					from: `${environment.appName} <${environment.noreplyEmail}>`,
					to: userInfo.email,
					subject: `${environment.appName}: You have a message about your Agent Profile`,
					html:`<p>From: ${name}</p>
								<p>Message: ${message}</p>
								<p>To contact him, reply to: ${email}</p>`
				};
				
				

				const nodemailer = new Nodemailer(msg);

				await nodemailer.sendMail();
				await batch.commit();
				resolve();
				

			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}



	private deleteFiles(files:string[]){
		const bucket = admin.storage().bucket();
		let arrayOfPromises = [];
		for(let file of files){
			arrayOfPromises.push(bucket.file(file).delete());
		}
		return Promise.all(arrayOfPromises);
	}


}