export const serviceProviderSkills = [
  {
    name:'General Labor',
    id:'generalLabor'
  },
  {
    name:'Computer Support',
    id:'computerSupport'
  },
  {
    name:'Handyman',
    id:'handyman'
  },
  {
    name:'Painting',
    id:'painting'
  },
  {
    name:'Drafting',
    id:'drafting'
  },
  {
    name:'Housework',
    id:'housework'
  },
  {
    name:'Electric Repair',
    id:'electricRepair'
  },
  {
    name:'Shopping',
    id:'shopping'
  },
  {
    name:'Appliance Repair',
    id:'applianceRepair'
  },
  {
    name:'Carpentry',
    id:'carpentry'
  },
  {
    name:'Cooking And Recipes',
    id:'cookingAndRecipes'
  },
  {
    name:'Extension And Additions',
    id:'extensionAndAdditions'
  },
  {
    name:'Plumbing',
    id:'plumbing'
  },
  {
    name:'Sewing',
    id:'sewing'
  },
  {
    name:'Appliance Installation',
    id:'applianceInstallation'
  },
  {
    name:'Building',
    id:'building'
  },
  {
    name:'Drone Photography',
    id:'dronePhotography'
  },
  {
    name:'Event Staffing',
    id:'eventStaffing'
  },
  {
    name:'Home Automation',
    id:'homeAutomation'
  },
  {
    name:'Interiors',
    id:'interiors'
  },
  {
    name:'Make up',
    id:'makeUp'
  },
  {
    name:'Cooking / Backing',
    id:'cooking_backing'
  },
  {
    name:'Furniture Assembly',
    id:'furnitureAssembly'
  },
  {
    name:'House Cleaning',
    id:'houseCleaning'
  },
  {
    name:'Landscape Design',
    id:'landscapeDesign'
  },
  {
    name:'Mural Painting',
    id:'muralPainting'
  },
  {
    name:'Commercial Cleaning',
    id:'commercialCleaning'
  },
  {
    name:'Domestic Cleaning',
    id:'domesticCleaning'
  },
  {
    name:'Air Conditioning',
    id:'airConditioning'
  },
  {
    name:'Bathroom',
    id:'bathroom'
  },
  {
    name:'Building Certification',
    id:'buildingCertification'
  },
  {
    name:'Carpet Cleaning',
    id:'carpetCleaning'
  },
  {
    name:'Carpet Repair And Laying',
    id:'carpetRepairAndLaying'
  },
  {
    name:'Decking',
    id:'decking'
  },
  {
    name:'Decoration',
    id:'decoration'
  },
  {
    name:'Embroidery',
    id:'embroidery'
  },
  {
    name:'Gardening',
    id:'gardening'
  },
  {
    name:'Home Organization',
    id:'homeOrganization'
  },
  {
    name:'Installation',
    id:'installation'
  },
  {
    name:'Hot Water Installation',
    id:'hotWaterInstallation'
  },
  {
    name:'Kitchen',
    id:'kitchen'
  },
  {
    name:'Landscape and Gardening',
    id:'landscapeAndGardening'
  },
  {
    name:'Lighting',
    id:'lighting'
  },
  {
    name:'Piping',
    id:'piping'
  },
  {
    name:'Antenna Services',
    id:'antennaServices'
  },
  {
    name:'Asphalt',
    id:'asphalt'
  },
  {
    name:'Bracket Installation',
    id:'bracketInstallation'
  },
  {
    name:'Car Washing',
    id:'carWashing'
  },
  {
    name:'Gas Fitting',
    id:'gasFitting'
  },
  {
    name:'Gutter Installation',
    id:'gutterInstallation'
  },
  {
    name:'Heating Systems',
    id:'heatingSystems'
  },
  {
    name:'Pet Sitting',
    id:'petSitting'
  },
  {
    name:'Pavement',
    id:'pavement'
  },
  {
    name:'Tiling',
    id:'tiling'
  },
  {
    name:'Lawn Mowing',
    id:'lawnMowing'
  },
  {
    name:'Laundry and Ironing',
    id:'laundryAndIroning'
  },
  {
    name:'Locksmith',
    id:'locksmith'
  },
  {
    name:'Bricklaying',
    id:'bricklaying'
  },
];

