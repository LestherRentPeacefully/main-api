import * as web3 from 'web3';
import * as Tx from 'ethereumjs-tx';
import * as solc from 'solc';
import * as admin from 'firebase-admin';
import {environment} from '../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {smartContract} from './smartContract';


export class RentalAgreementService{
	private web3:any;

	constructor() {
		this.web3 = new web3();
		this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));

	}


	public estimateGasAndGasPriceContractCreation(clauses):Promise<any>{
		let gasPrice = this.web3.eth.gasPrice.toString(10);
		return new Promise((resolve,reject)=>{		
			try{

				let compiledContract = solc.compile(smartContract(clauses), 1);
				let byteCode = compiledContract.contracts[':RentalAgreement'].bytecode;
				let estimateGas = this.web3.eth.estimateGas({
					// nonce: this.web3.toHex(this.web3.eth.getTransactionCount(clauses.parties.landlord.address)),
					gasPrice: this.web3.toHex(gasPrice),
					from: clauses.parties.landlord.address,
					value: '0x00',
					data: this.addPrefix(byteCode)
				});
					
		  	resolve({estimateGas:estimateGas, gasPrice:gasPrice});
			}catch(err){
				reject(err.message || err);
			}
		});
	}


	public create(clauses:any):Promise<any>{
		return new Promise((resolve, reject)=>{

			try{
				let contractCode = smartContract(clauses)
				let compiledContract = solc.compile(contractCode,1); //optimizer activated
				let byteCode = compiledContract.contracts[':RentalAgreement'].bytecode;
				let contractABI = typeof (compiledContract.contracts[':RentalAgreement'].interface)=='string'? 
													JSON.parse(compiledContract.contracts[':RentalAgreement'].interface) : 
													compiledContract.contracts[':RentalAgreement'].interface;
				let rawTx = {
	        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(clauses.parties.landlord.address)),
	        gasPrice: this.web3.toHex(clauses.gasPrice),
	        gasLimit: this.web3.toHex(clauses.gasLimit),
	        value: '0x00',
	        data: this.addPrefix(byteCode)
		   	};

		   	let tx = new Tx(rawTx);
		   	tx.sign(Buffer.from(clauses.parties.landlord.privateKey, 'hex')); //Sign transaction
		   	let serializedTx = `0x${tx.serialize().toString('hex')}`;

	    	this.web3.eth.sendRawTransaction(serializedTx, async (err, hash)=> {
	        if(!err){
						let txHash = hash.toLowerCase();
						await this.saveCreationInfo(clauses, txHash, contractCode, contractABI, solc.version());
						resolve(txHash);
							
					} else{
						reject('Unexpected error creating contract');
					}               
	    	});

			}catch(err){
				reject('Unexpected error creating contract');
			}
		
		});
	}



	private saveCreationInfo(clauses:any, txHash:string, contractCode:string, contractABI:any, compilerVersion:string){

			const db = admin.firestore();
			const batch = db.batch();
			const id = db.collection('uniqueID').doc().id;

			let agreementObj = {
				txHash:txHash,
				status:'pending',
				type:'contractExecution',
				action:'contractCreation',
				currency:'ETH',
				contract:{
					code:contractCode,
					ABI:contractABI,
					version:environment.contractVersion,
					compilerVersion:compilerVersion,
				},
				gasPrice:clauses.gasPrice,
				gasLimit:clauses.gasLimit,
				additionalClauses:{
					clause:clauses.finalDetails.additionalClauses.clause
				},
				propertyRentedID:clauses.propertyRentedID,
				landlordID:clauses.parties.landlord.uid,
				tenantID:clauses.parties.tenant.uid,
				id:id
			}

			let txObj:any = {
				txHash:txHash,
				status:'pending',
				type:'contractExecution',
				action:'contractCreation',
				currency:'ETH',
				property:{
					type:clauses.general.property.typeString,
					address:clauses.general.property.address,
					rentAmount:clauses.terms.rent.amount,
					securityDepositAmount:clauses.terms.securityDeposit.amount,
					location:clauses.general.property.location,
					paymentDateDay:clauses.terms.rent.paymentDateDay,
					id:clauses.propertyRentedID
				},
				landlordID:clauses.parties.landlord.uid,
				tenantID:clauses.parties.tenant.uid,
				id:id
			};

			batch.set(db.doc(`rentalAgreements/${id}`), agreementObj);

			batch.set(db.doc(`transactions/${id}`), txObj);

			return batch.commit();


	}



	public estimateGasAndGasPriceRentalAgreementAction(from:string,action:string,contractAddress:string,rent?):Promise<any>{
		let gasPrice = this.web3.eth.gasPrice.toString(10);
		const db = admin.firestore();
		return new Promise(async (resolve, reject)=>{
			try{
				let estimateGas;
				let rentalAgreementInfo = (await db.collection('rentalAgreements').where('contract.address','==',contractAddress).limit(1).get()).docs[0].data();
				let ABI = rentalAgreementInfo.contract.ABI;
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);

					
				switch (action) {

					case "approveAgreement":
						estimateGas = myContractInstance.approveAgreement.estimateGas({from:from, value:0});
					break;

					case "modifyRent":
						estimateGas = myContractInstance.modifyRent.estimateGas(rent,{from:from,value:0});
					break;

					case "deposit":
						estimateGas = this.web3.eth.estimateGas({
							// nonce: this.web3.toHex(this.web3.eth.getTransactionCount(from)),
							gasPrice: this.web3.toHex(gasPrice),
							from:from,
							to: contractAddress,
							value: this.web3.toHex(rent)
						});
					break;

					case "withdraw":
						estimateGas = myContractInstance.withdraw.estimateGas({from:from,value:0});
					break;

					case "startDispute":
						estimateGas = myContractInstance.startDispute.estimateGas({from:from,value:0});
					break;

					case "endDispute":
						estimateGas = myContractInstance.endDispute.estimateGas({from:from,value:0});
					break;

					case "openTicket":
						estimateGas = myContractInstance.openTicket.estimateGas({from:from,value:0});
						break;

					case "closeTicket":
						estimateGas = myContractInstance.closeTicket.estimateGas({from:from,value:0});
					break;

					default:
						estimateGas = '21000';
					break;
				}
				resolve({estimateGas:estimateGas, gasPrice:gasPrice});
			}catch(err){
				reject(err.message || err);
			}
		});
	}


	public rentalAgreementAction(uid:string, gasPrice:string, gasLimit:string, action:string, contractAddress:string, rent?):Promise<any>{
		const db = admin.firestore();
		return new Promise(async(resolve,reject)=>{

			try{

				let [identityWalletSnapshot, rentalAgreementSnapshot] = await Promise.all([
					db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get(),
					db.collection('rentalAgreements').where('contract.address','==',contractAddress).limit(1).get()
				]);

				let identityWallet = identityWalletSnapshot.docs[0].data();
				let ABI = rentalAgreementSnapshot.docs[0].data().contract.ABI;
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);

				
				if(action=='approveAgreement' && myContractInstance.agreementApproved()){
					
					reject('Approval already done');

				} else {
					
					let rawTx:any = {
						nonce: this.web3.toHex(this.web3.eth.getTransactionCount(identityWallet.address)),
						gasPrice: this.web3.toHex(gasPrice),
						gasLimit: this.web3.toHex(gasLimit),
						to: contractAddress,
						value: '0x00'
					}

					switch (action) {

						case "approveAgreement":
							rawTx.data = myContractInstance.approveAgreement.getData();
						break;

						case "modifyRent":
							rawTx.data = myContractInstance.modifyRent.getData(rent);
						break;

						case "deposit":
							rawTx.value = this.web3.toHex(rent);
						break;

						case "withdraw":
							rawTx.data = myContractInstance.withdraw.getData();
						break;

						case "startDispute":
							rawTx.data = myContractInstance.startDispute.getData();
						break;

						case "endDispute":
							rawTx.data = myContractInstance.endDispute.getData();
						break;

						case "openTicket":
							rawTx.data = myContractInstance.openTicket.getData();
						break;

						case "closeTicket":
							rawTx.data = myContractInstance.closeTicket.getData();
						break;
						
						default:
							
						break;
					}
						// Generate tx
					let tx = new Tx(rawTx);
					tx.sign(Buffer.from(identityWallet.privateKey, 'hex')); //Sign transaction
					let serializedTx = `0x${tx.serialize().toString('hex')}`;
					this.web3.eth.sendRawTransaction(serializedTx, (err, hash)=> {

						if(err){
							reject(err.message || err);
						} else{
							let txHash = hash.toLowerCase();

							this.saveTransaction(txHash, action, contractAddress, uid)
								.then(()=> resolve(txHash))
								.catch((err)=> reject('Error generating transaction'));
						}               
					});

				}
			}catch(err){
				reject('Unexpected error sending transaction');
			}
					
			
		});
	}


	private saveTransaction(txHash:string, action:string, contractAddress:string, uid:string){
			const db = admin.firestore();
			const batch = db.batch();
			const id = db.collection('uniqueID').doc().id;

			let txObj:any = {
				txHash:txHash,
				type:'contractExecution',
				action:action,
				contractAddress:contractAddress,
				status:'pending',
				uid:uid,
				id:id
			};

			batch.set(db.doc(`transactions/${id}`),txObj);
			return batch.commit();

	}


	public checkApproval(uid:string, contractAddress:string){
		const db = admin.firestore();
		return new Promise(async(resolve,reject)=>{
			try {
				let state:any = {};
				
				let ABI = (await db.collection('rentalAgreements').where('contract.address','==',contractAddress).limit(1).get()).docs[0].data().contract.ABI;
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);
				
				state.approved = myContractInstance.agreementApproved();

				if (state.approved) {
					state.currentBlock = this.web3.eth.blockNumber;

					let filter = this.web3.eth.filter({
						address:contractAddress,
						fromBlock: 0,
						toBlock:'latest',
						topics:[this.web3.sha3('Approved(address,uint256)')]
					});

					filter.get((err, logs)=> {
						if(!err){
							const abiDecoder = require('abi-decoder');
							abiDecoder.addABI(ABI);
							const decodedLogs = abiDecoder.decodeLogs(logs);
							
							state.txHash = logs[0].transactionHash;
							state.blockNumber = logs[0].blockNumber;
							state.timestamp = decodedLogs[0].events.find(param=>param.name=='timestamp').value;
							resolve(state);
						
						}else{
							reject('Unexpected error. Try again later');
						}
					});
					// let event = myContractInstance.Approved({fromBlock:0, toBlock:'latest'});
					// event.get((err, results)=>{
					// 	if(!err){
					// 		state.txHash = results[0].transactionHash;
					// 		state.timestamp = results[0].args.timestamp;
					// 		state.blockNumber = results[0].blockNumber;
					// 		resolve(state);
					// 	}else{
					// 		reject('Unexpected error. Try again later');
					// 	}
					// });
				} else {
					let querySnapshot = await db.collection(`transactions`)
																.where('uid','==',uid)
																.where('contractAddress','==',contractAddress)
																.where('status','==','pending')
																.where('type','==','contractExecution')
																.where('action','==','approveAgreement')
																.get();
				
					state.pendingTx = !querySnapshot.empty;
					resolve(state);
				}
		
			} catch (err) {
				reject('Error checking approval status');
			}
		});
	}


	private addPrefix(data:string){
		return data.indexOf('0x')==0? data:`0x${data}`;
	}


	public viewClauses(contractAddress:string){
		return new Promise(async (resolve, reject)=>{
			const db = admin.firestore();
			try {
				let ABI = (await db.collection('rentalAgreements').where('contract.address','==',contractAddress).limit(1).get()).docs[0].data().contract.ABI;
				
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);
	
				let clauses = [];
	
				let batch = this.web3.createBatch();
	
				batch.add(myContractInstance.viewFirstBatchOfClauses.request((err, response)=>{
					if (!err) { 
						for(let clause of response){
							clauses.push(clause.toString(10));
						}
					} else {
						reject(err.message || err);
					}
				}));
	
				batch.add(myContractInstance.viewSecondBatchOfClauses.request((err, response)=>{
					if (!err) { 
						for(let clause of response){
							clauses.push(clause.toString(10));
						}
					} else {
						reject(err.message || err);
					}
				}));
				batch.add(myContractInstance.viewThirdBatchOfClauses.request((err, response)=>{
					if (!err) { 
						for(let clause of response){
							clauses.push(clause.toString(10));
						}
						resolve(clauses);
					} else {
						reject(err.message || err);
					}
				}));
	
				batch.execute();
				
			} catch (err) {
				reject('Unexpected error getting clauses from smart contract');
			}

		});
	}


	 viewContractState(contractAddress:string, uid:string):Promise<any>{
		return new Promise(async(resolve, reject)=>{
			const db = admin.firestore();
			
			let pendingTxs = {
				modifyRent :false,
				deposit:false,
				withdraw:false,
				startDispute:false,
				endDispute:false,
				openTicket:false,
				closeTicket:false,
			};

			try {
				let ABI = (await db.collection('rentalAgreements').where('contract.address', '==', contractAddress).limit(1).get()).docs[0].data().contract.ABI;
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);
				let querySnapshot = await db.collection(`transactions`)
															.where('uid', '==', uid)
															.where('type','==','contractExecution')
															.where('contractAddress','==',contractAddress)
															.where('status', '==', 'pending')
															.get();

				querySnapshot.forEach(doc=> pendingTxs[doc.data().action] = true);

				let batch = this.web3.createBatch();
				let state:any = {};
				state.pendingTxs = pendingTxs;


				batch.add(this.web3.eth.getBlockNumber.request((err, blockNumber)=>{
					if (!err) { 
						state.currentBlock = blockNumber;
					} else {
						reject(err.message || err);
					}
				}));

				batch.add(myContractInstance.viewFirstBatchOfContractState.request((err, response)=>{
					if (!err) {
						let contractState = response;

						state.depositInfo = {
							amount:new BigNumber(contractState[0]).toNumber(),
							timestamp:new BigNumber(contractState[1]).toNumber(),
							blockNumber:new BigNumber(contractState[2]).toNumber()
						};
						state.withdrawalInfo = {
							amount:new BigNumber(contractState[3]).toNumber(),
							timestamp:new BigNumber(contractState[4]).toNumber(),
							blockNumber:new BigNumber(contractState[5]).toNumber()
						};
						state.rentModificationInfo = {
							amount:new BigNumber(contractState[6]).toNumber(),
							timestamp:new BigNumber(contractState[7]).toNumber(),
							blockNumber:new BigNumber(contractState[8]).toNumber()
						};
						
					} else {
						reject(err.message || err);
					}
				}));

				batch.add(myContractInstance.viewSecondBatchOfContractState.request((err, response)=>{
					if (!err) { 
						let contractState = response;

						state.ticketInfo = {
							open:contractState[0],
							timestamp:new BigNumber(contractState[1]).toNumber(),
							blockNumber:new BigNumber(contractState[2]).toNumber()
						};
						state.disputeInfo = {
							onGoing:contractState[3],
							startedBy:contractState[4],
							timestamp:new BigNumber(contractState[5]).toNumber(),
							blockNumber:new BigNumber(contractState[6]).toNumber()
						};
						state.lockedAmount = new BigNumber(contractState[7]).toNumber();
						state.availableAmount = new BigNumber(contractState[8]).toNumber();
						state.rent = new BigNumber(contractState[9]).toNumber();

						resolve(state);

					} else {
						reject(err.message || err);
					}
				}));

				batch.execute();

			} catch (err) {
				reject('Error getting contract state');
			}


		});
	}

	

	public getRentalAgreementActions(contractAddress:string):Promise<any>{
		return new Promise(async(resolve,reject)=>{
			try{
				const db = admin.firestore();
				let ABI = (await db.collection('rentalAgreements').where('contract.address', '==', contractAddress).limit(1).get()).docs[0].data().contract.ABI;
				let myContractInstance = this.web3.eth.contract(ABI).at(contractAddress);
				let events = myContractInstance.allEvents({fromBlock:0, toBlock:'latest'});
				
				events.get((err, results)=>{
					if(!err){
						if(results.length>0){

							let blockNumber = this.web3.eth.blockNumber;
							results.map((result)=>{
								result.currentBlock = blockNumber; 
								return result;
							});
						}

						resolve(results);
					}else{
						reject('Error loading list. Try again later');
					}
				});

			}catch(err){
				reject('Unexpected error loading list');
			}
		});
	}




}