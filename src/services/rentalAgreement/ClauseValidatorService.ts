import * as admin from 'firebase-admin';
import {BigNumber} from 'bignumber.js';


export class ClauseValidatorService{



	public async validateClauses(clauses:any){
		await this.general(clauses);
		await this.property(clauses);
		await this.parties(clauses)
		await this.terms(clauses);
		await this.finalDetails(clauses);
	}






	/*-------------------------------------------------GENERAL VALIDATIONS------------------------------*/
	private async general(clauses:any){
		await this.isFilledPropertyType(clauses);
		await this.isValidPropertyAddress(clauses);
		await this.isFilledLeaseDetails(clauses);
		// await this.isValidLeaseDates(clauses);
	}


	private isFilledPropertyType(clauses:any):Promise<void>{
		return new Promise((resolve, reject)=>{
			if(clauses.general.property.type){
				resolve();
			}else{
				reject('Please, fill the Property Type Form in General Section');
			}
		});
	}

	private isValidPropertyAddress(clauses:any):Promise<void>{
		return new Promise((resolve, reject)=>{
			if(clauses.general.property.address && 
				 clauses.general.property.location && 
				 clauses.general.property.location.lat && 
				 clauses.general.property.location.lng && 
				 clauses.general.property.location.geohash){

				clauses.general.property.location.geopoint = new admin.firestore.GeoPoint(clauses.general.property.location.lat, clauses.general.property.location.lng);
				delete clauses.general.property.location.lat, delete clauses.general.property.location.lng;
				resolve();
			}else{
				reject('Please, Fill the Property Address form correctly');
			}
		});
	}

	private isFilledLeaseDetails(clauses:any):Promise<void>{
		return new Promise((resolve, reject)=>{
			clauses.general.leaseDetails = {
				dateLeaseStarts:new BigNumber(new Date().getTime()).dividedBy('1000').integerValue().toNumber()
			};
			// if(!clauses.general.leaseDetails || !clauses.general.leaseDetails.leaseDateSelected || 
			// 	 (clauses.general.leaseDetails.leaseDateSelected=='1' && !clauses.general.leaseDetails.dateLeaseEnds) ||
			// 	 (clauses.general.leaseDetails.leaseDateSelected=='2' && !clauses.general.leaseDetails.renewalSelected)){
			// 	reject('Please, fill the Lease Details Form in General Section');

			// } else {
				// clauses.general.leaseDetails.dateLeaseStarts = new BigNumber(new Date().getTime()).dividedBy('1000').integerValue().toNumber();
				// clauses.general.leaseDetails.dateLeaseEnds = clauses.general.leaseDetails.leaseDateSelected=='1'? clauses.general.leaseDetails.dateLeaseEnds:'0';
				// clauses.general.leaseDetails.renewalSelected = clauses.general.leaseDetails.leaseDateSelected=='2'? clauses.general.leaseDetails.renewalSelected:'0';
				resolve();
			// }
		});
	}

	// private isValidLeaseDates(clauses:any):Promise<void>{
	// 	return new Promise((resolve, reject)=>{
	// 		if(clauses.general.leaseDetails.dateLeaseEnds<0 || 
	// 			(clauses.general.leaseDetails.leaseDateSelected=='1' && clauses.general.leaseDetails.dateLeaseEnds <= clauses.general.leaseDetails.dateLeaseStarts)) { 
	// 			reject('Wrong dates in General Section');
	// 		} else{
	// 			resolve();
	// 		}
	// 	});
	// }






	/*--------------------------------------PROPERTY VALIDATIONS---------------------------------------*/
	private async property(clauses:any){
		await this.isValidPropertyDetails(clauses);
	}


	private isValidPropertyDetails(clauses:any):Promise<any>{
		return new Promise((resolve,reject)=>{
			if(!clauses.property.details || !clauses.property.details.selectedAccessToParking) {
				reject('Please, fill the Property Details Form in Property Section');
			} else{
				resolve();
			}
		});
	}






	/*------------------------------------------PARTIES VALIDATIONS------------------------------------*/
	private async parties(clauses:any){
		await this.isValidOccupantsOptions(clauses);
		await this.getPartiesInfo(clauses);
	}


	private isValidOccupantsOptions(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if (!clauses.parties.selectedOccupantsOptions) { 
				reject("Please, fill the Tenant's info Form in Parties Section");
			} else{
				resolve();
			}
		});
	}

	private getPartiesInfo(clauses:any):Promise<void>{
		const db = admin.firestore();
		return new Promise(async (resolve, reject)=>{
			try{
				let [propertySnapshot, landlordIDWalletDoc] = await Promise.all([
					db.doc(`propertiesRented/${clauses.propertyRentedID}`).get(), 
					db.collection(`wallets/identity/private`).where('uid','==',clauses.uid).limit(1).get()
				]);
					
				let propertyRented = propertySnapshot.data();
	
				if(propertySnapshot.exists){
					if(clauses.uid==propertyRented.landlord.uid){
						clauses.parties.landlord = propertyRented.landlord;
						clauses.parties.landlord.privateKey = landlordIDWalletDoc.docs[0].data().privateKey;
						clauses.parties.tenant = propertyRented.tenant;
						resolve();
					}else{
						reject("You're not the Landlord of this property");
					}
				}else{
					reject('Property does not exist');
				}
			}catch(err){
				reject('Unexpected error getting Parties info');
			}
		});
	}






	/*----------------------------------------TERMS VALIDATIONS-----------------------------------*/
	private async terms(clauses:any){
		await this.isFilledRent(clauses);
		await this.isValidRentAmount(clauses);
		await this.isFilledRentIncrease(clauses);
		await this.isValidSecurityDepositAmount(clauses);
		await this.isValidDaysNoticeBeforeIncreasingRent(clauses);
		await this.isFilledUseOfProperty(clauses);
		await this.isFilledLatePayments(clauses);
		await this.isValidLatePaymentAmount(clauses);
		await this.isFilledUtilitiesDetails(clauses);
	}


	private isFilledRent(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if (!clauses.terms.rent || !clauses.terms.rent.amount) { 
				reject('Please, fill the Rent Form in Terms Section');
			} else {
				resolve();
			}
		});
	}

	private isValidRentAmount(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if (clauses.terms.rent.amount<1) { 
				reject('Rent amount must be greater than zero in Terms Section');
			}else{
				clauses.terms.rent.amount = Math.round(clauses.terms.rent.amount);
				resolve();
			}
		});
	}

	private isFilledRentIncrease(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if(!clauses.terms.rentIncrease || !clauses.terms.rentIncrease.selectedIncrementNotice || 
							(clauses.terms.rentIncrease.selectedincrementNotice=='2' && !clauses.terms.rentIncrease.daysNoticeBeforeIncreasingRent)){
				reject('Please, fill the Rent Increase Form in Terms Section');
			}else { 
				clauses.terms.rentIncrease.daysNoticeBeforeIncreasingRent = clauses.terms.rentIncrease.selectedincrementNotice=='2'? clauses.terms.rentIncrease.daysNoticeBeforeIncreasingRent:'0';
				resolve();
			}
		});
	}

	private isValidSecurityDepositAmount(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if (!clauses.terms.securityDeposit || !(clauses.terms.securityDeposit.amount>=0)) { 
				reject('Security deposit must be greater than or equal to 0 USD');
			} else {
				resolve();
			}
		});
	}

	private isValidDaysNoticeBeforeIncreasingRent(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if (clauses.terms.rentIncrease.daysNoticeBeforeIncreasingRent<0) { 
				reject('Days notice must be greater than');
			}else{
				resolve();
			}
		});
	}

	private isFilledUseOfProperty(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if(!clauses.terms.useOfProperty || !clauses.terms.useOfProperty.selectedPetsAllowance || !clauses.terms.useOfProperty.selectedSmokingAllowance) {
				reject('Please, fill the Use of Property Form in Terms Section');
			}else{
				resolve();
			}
		});
	}

	private isFilledLatePayments(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if(!clauses.terms.latePayments || !clauses.terms.latePayments.selectedLatePayment ||
				 (clauses.terms.latePayments.selectedLatePayment=='2' && !clauses.terms.latePayments.percentage) || 
				 ((clauses.terms.latePayments.selectedLatePayment=='3' || clauses.terms.latePayments.selectedLatePayment=='4') && 
					 !clauses.terms.latePayments.amount) ) {
				reject('Please, fill the Late Payments Form in Terms Section');

			} else {
				clauses.terms.latePayments.percentage = clauses.terms.latePayments.selectedLatePayment=='2'? 
																								Math.round(10*clauses.terms.latePayments.percentage):'0';

				clauses.terms.latePayments.amount = (clauses.terms.latePayments.selectedLatePayment=='3' || clauses.terms.latePayments.selectedLatePayment=='4')? 
																								Math.round(clauses.terms.latePayments.amount):'0';
				resolve();
			}
		});
	}

	private isValidLatePaymentAmount(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if ((clauses.terms.latePayments.selectedLatePayment=='2' && (clauses.terms.latePayments.percentage<0 || clauses.terms.latePayments.percentage>100*10)) || 
			   ((clauses.terms.latePayments.selectedLatePayment=='3' || clauses.terms.latePayments.selectedLatePayment=='4') && clauses.terms.latePayments.amount<1)		
			) { 
				reject('Late Payment amount must be greater than 0 in Terms Section');
			} else{
				resolve();
			}
		});
	}

	private isFilledUtilitiesDetails(clauses:any):Promise<void>{
		let valid = true;
		return new Promise((resolve,reject)=>{
			if(!clauses.terms.utilitiesDetails || !clauses.terms.utilitiesDetails.selectedUtilitiesDetails || 
				 !clauses.terms.utilitiesDetails.utilities || clauses.terms.utilitiesDetails.utilities.length<9){
				reject('Please, fill the Utilities Detais Form in Terms Section');

			}	else {
				for(let utility of clauses.terms.utilitiesDetails.utilities){
					if(clauses.terms.utilitiesDetails.selectedUtilitiesDetails=='3' && !utility){
						valid = false;
						break;
					}
				}

				if(valid){
					if(clauses.terms.utilitiesDetails.selectedUtilitiesDetails!='3'){
						for(let i = 0; i< clauses.terms.utilitiesDetails.utilities.length; i++){
							clauses.terms.utilitiesDetails.utilities[i] = '0';
						}
					}
					
					resolve();

				} else{
					reject('Please, fill the Utilities Detais Form in Terms Section');
				}
			}
		});
	}






	/*-------------------------------------Final Details Validation--------------------------------------*/
	private async finalDetails(clauses:any){
		await this.isFilledDisputeResolution(clauses);
		await this.isFilledAdditionalClauses(clauses);
	}


	private isFilledDisputeResolution(clauses:any):Promise<void>{
		return new Promise((resolve,reject)=>{
			if(!clauses.finalDetails.disputeResolution || !clauses.finalDetails.disputeResolution.selectedDisputeResolution ||
				(clauses.finalDetails.disputeResolution.selectedDisputeResolution!='1' && !clauses.finalDetails.disputeResolution.selectedDisputeResolutionCost)){
				reject('Please, fill the Dispute Resolution Form in Final Details Section');
			} else {
				clauses.finalDetails.disputeResolution.selectedDisputeResolutionCost = clauses.finalDetails.disputeResolution.selectedDisputeResolution!='1'? clauses.finalDetails.disputeResolution.selectedDisputeResolutionCost:'0';
				resolve();
			}
		});
	}

	private isFilledAdditionalClauses(clauses:any):Promise<void>{	
		return new Promise((resolve, reject)=>{
			if(!clauses.finalDetails.additionalClauses || !clauses.finalDetails.additionalClauses.selectedAdditionalClause || 
				 (clauses.finalDetails.additionalClauses.selectedAdditionalClause=='1' && !clauses.finalDetails.additionalClauses.clause)){
				reject('Please, fill the Additional Clauses Form in Final Details Section');
			} else{
				clauses.finalDetails.additionalClauses.clause = clauses.finalDetails.additionalClauses.selectedAdditionalClause=='1'? clauses.finalDetails.additionalClauses.clause:'';
				resolve();
			}
		});
	}




}