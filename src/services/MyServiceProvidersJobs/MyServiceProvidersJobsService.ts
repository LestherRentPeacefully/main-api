import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import {Database} from '../Database';
import {SharedService} from '../Shared/SharedService';

const shared = new SharedService();

export class MyServiceProvidersJobsService {

  async acceptJob(uid: string, serviceProviderJobID: string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        
        let serviceProviderJob = (await db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`).get()).data();

        if(!serviceProviderJob) {
          reject("The job doesn't exists"); return;
        }
        
        let [employerInfo, serviceProviderInfo] = (await db.getAll(
          db.doc(`users/${serviceProviderJob.employer.uid}`), 
          db.doc(`users/${serviceProviderJob.serviceProvider.uid}`))
        ).map(doc => doc.data());
        

        if (uid !== serviceProviderInfo.uid) {
          reject('Invalid user'); return;
        }

        if (serviceProviderJob.status !== 'wating for acceptance') {
          reject('The job was already either accepted or cancelled'); return;
        }


        batch.update(db.doc(`users/${employerInfo.uid}`), {
          [`${serviceProviderJob.employer.role}.employer.activeJobs`]:employerInfo[serviceProviderJob.employer.role].employer.activeJobs + 1
        });
        // batch.update(db.doc(`public/${employerInfo.uid}`), {
        //   [`${serviceProviderJob.employer.role}.employer.activeJobs`]:employerInfo[serviceProviderJob.employer.role].employer.activeJobs + 1
        // });

        batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
          'serviceProvider.activeJobs':serviceProviderInfo.serviceProvider.activeJobs + 1
        });
        // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
        //   'serviceProvider.activeJobs':serviceProviderInfo.serviceProvider.activeJobs + 1
        // });

        batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`), {
          status:'ongoing'
        });

        const notifId = shared.generateId();

				batch.set(db.doc(`users/${employerInfo.uid}/notifications/${notifId}`), {
					title: `Job Accepted`,
					content: `${shared.capitalize(serviceProviderInfo.firstName)} ${shared.capitalize(serviceProviderInfo.lastName)} has accepted your job offer`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${employerInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});
        
        await batch.commit();
        resolve();

        
      } catch (err) {
        reject('Unexpected error accepting job');
      }
    });
  }


  cancelJob(uid: string, serviceProviderJobID: string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        let serviceProviderJob = (await db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`).get()).data();

        if(!serviceProviderJob) {
          reject("The job doesn't exists"); return;
        }


        let [[employerSnapshot, serviceProviderSnapshot], milestonesProposedSnapshot, milestonesCreatedSnapshot] = await Promise.all([
          db.getAll(db.doc(`users/${serviceProviderJob.employer.uid}`), db.doc(`users/${serviceProviderJob.serviceProvider.uid}`)),
          db.collection(`milestones`).where('serviceProviderJobID','==',serviceProviderJobID).where('status','==','proposed').limit(1).get(),
          db.collection(`milestones`).where('serviceProviderJobID','==',serviceProviderJobID).where('status','==','created').limit(1).get()
        ]);

        let employerInfo = employerSnapshot.data();
        let serviceProviderInfo = serviceProviderSnapshot.data();

        if (uid !== employerInfo.uid && uid !== serviceProviderInfo.uid) {
          reject('Invalid user'); return;
        }

        if(serviceProviderJob.status !== 'wating for acceptance' && 
          (serviceProviderJob.status !== 'ongoing' || !milestonesProposedSnapshot.empty || !milestonesCreatedSnapshot.empty )) {

          if(serviceProviderJob.status === 'ongoing' && (!milestonesProposedSnapshot.empty || !milestonesCreatedSnapshot.empty) ) {
            reject("You can't cancel until there aren't pending milestones"); return;
          }

          reject("Job already canceled"); return;
        }


        if(serviceProviderJob.status=='ongoing'){
          batch.update(db.doc(`users/${employerInfo.uid}`), {
            [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${employerInfo.uid}`), {
          //   [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          // });

          batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
            'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
          //   'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          // });
        }

        batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`), {
          status:'canceled'
        });

        const userToNotify = uid !== employerInfo.uid? employerInfo: serviceProviderInfo;

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${userToNotify.uid}/notifications/${notifId}`), {
					title: `Job canceled`,
					content: `Job offer has been canceled`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${userToNotify.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});


        await batch.commit();
        resolve();
            

      } catch (err) {
        reject('Unexpected error cancelling job');
      }
    });
  }

  proposeMilestone(uid: string, serviceProviderJobID: string, amount: number, details: string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        const serviceProviderJob = (await db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`).get()).data();
        const employerInfo = serviceProviderJob.employer;
        const serviceProviderInfo = serviceProviderJob.serviceProvider;
        
        if(!serviceProviderJob) {
          reject("The job doesn't exists"); return;
        }

        if (uid !== serviceProviderInfo.uid) {
          reject('Invalid user'); return;
        }

        if (serviceProviderJob.status !== 'ongoing' && serviceProviderJob.status !== 'completed') {
          reject('You can only create milestone for either ongoing or completed jobs'); return;
        }

        const id = shared.generateId();

        batch.set(db.doc(`milestones/${id}`), {
          amount:amount.toString(),
          details,
          serviceProviderJobID,
          creationTS:Date.now(),
          id,
          status:'proposed',
          sender:{
            firstName:employerInfo.firstName,
            lastName:employerInfo.lastName,
            email:employerInfo.email,
            uid:employerInfo.uid
          },
          receiver:{
            firstName:serviceProviderInfo.firstName,
            lastName:serviceProviderInfo.lastName,
            email:serviceProviderInfo.email,
            uid:serviceProviderInfo.uid
          }
        });

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${employerInfo.uid}/notifications/${notifId}`), {
					title: `Milestone proposed`,
					content: `A milestone has been proposed`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${employerInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});

        await batch.commit();
        resolve(id);


      } catch (err) {
        reject('Unexpected error proposing milestone');
      }
    });
  }


  deleteProposedMilestone(uid: string, milestoneID: string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        
        const milestone = (await db.doc(`milestones/${milestoneID}`).get()).data();

        if(!milestone) {
          reject("Milestone doesn't exist"); return;
        }

        if (uid !== milestone.sender.uid && uid !== milestone.receiver.uid) {
          reject("Invalid user"); return;
        }

        if (milestone.status !== 'proposed') {
          reject('This is not a proposed milestone'); return;
        }

        batch.delete(db.doc(`milestones/${milestoneID}`));

        const userToNotify = uid !== milestone.sender.uid? milestone.sender: milestone.receiver;

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${userToNotify.uid}/notifications/${notifId}`), {
					title: `Milestone proposed deleted`,
					content: `A milestone proposed has been deleted`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${userToNotify.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});

        await batch.commit();
        resolve();


      } catch (err) {
        reject('Unexpected error deleting milestone');
      }
    });
  }


  public async getCurrencyPrice(uid:string, currency:string){
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForMilestone/${uid}`).set({currency, price, name});
    return {currency, price};
  }


  public createMilestone(uid:string, currency:string, milestoneID:string, serviceProviderJobID:string, amount:number, details:number){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();

        let balanceSnaphot:FirebaseFirestore.QuerySnapshot;
        let lastPriceForMilestoneSnapshot:FirebaseFirestore.DocumentSnapshot;
        let milestoneSnapshot:FirebaseFirestore.DocumentSnapshot;
        let serviceProviderJobSnapshot:FirebaseFirestore.DocumentSnapshot;
        let serviceProviderJob;
        let milestone;
        let newID = null;

        if(milestoneID){
          [balanceSnaphot, [lastPriceForMilestoneSnapshot, milestoneSnapshot]] = await Promise.all([
            db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
            db.getAll(db.doc(`lastPriceForMilestone/${uid}`), db.doc(`milestones/${milestoneID}`))
          ]);

          milestone = milestoneSnapshot.data();

        }else{
          [balanceSnaphot, [lastPriceForMilestoneSnapshot, serviceProviderJobSnapshot]] = await Promise.all([
            db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
            db.getAll(db.doc(`lastPriceForMilestone/${uid}`), db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`))
          ]);

          serviceProviderJob = serviceProviderJobSnapshot.data();
          newID = shared.generateId();

          milestone = {
            amount:amount.toString(),
            details,
            serviceProviderJobID,
            creationTS:Date.now(),
            id:newID,
            status:'created',
            sender:{
              firstName:serviceProviderJob.employer.firstName,
              lastName:serviceProviderJob.employer.lastName,
              email:serviceProviderJob.employer.email,
              uid:serviceProviderJob.employer.uid
            },
            receiver:{
              firstName:serviceProviderJob.serviceProvider.firstName,
              lastName:serviceProviderJob.serviceProvider.lastName,
              email:serviceProviderJob.serviceProvider.email,
              uid:serviceProviderJob.serviceProvider.uid
            }
          };
          
        }

        if (balanceSnaphot.empty) {
          reject('Insufficient funds'); return;
        }

        const balance = balanceSnaphot.docs[0].data();
        const lastPriceForMilestone = lastPriceForMilestoneSnapshot.data();

        if (lastPriceForMilestone.currency !== currency) {
          reject('Something went wrong. Try again later'); return;
        }

        const amountToPay = new BigNumber(milestone.amount).div(lastPriceForMilestone.price.USD).decimalPlaces(8).toString(10);

        if (balanceSnaphot.empty || !(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))) {
          reject('Insufficient funds'); return;
        }

        if (uid !== milestone.sender.uid) {
          reject('Invalid user'); return;
        }

        if (milestoneID && milestone.status !== 'proposed') {
          reject('This is not a proposed milestone'); return;
        }

        if (!milestoneID && serviceProviderJob.status !== 'ongoing' && serviceProviderJob.status !== 'completed') {
          reject('You can only create milestone for either ongoing or completed jobs'); return;
        }

        batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).minus(amountToPay).toString(10),
          onEscrow: new BigNumber(balance.onEscrow).plus(amountToPay).toString(10)
        });

        if (milestoneID) {
          batch.update(db.doc(`milestones/${milestoneID}`), {
            status:'created',
            currencyForPayment:{
              currency:currency,
              amount:amountToPay,
              name:lastPriceForMilestone.name
            }
          });
          
        } else {
          batch.set(db.doc(`milestones/${newID}`), {
            ...milestone, 
            currencyForPayment:{
              currency:currency,
              amount:amountToPay,
              name:lastPriceForMilestone.name
            }
          });
        }


        const notifId = shared.generateId();
        batch.set(db.doc(`users/${milestone.receiver.uid}/notifications/${notifId}`), {
					title: `Milestone created`,
					content: `A milestone has been created`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${milestone.receiver.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});

        await batch.commit();
        resolve(newID);
               
     
      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }


  public cancelCreatedMilestone(uid:string, milestoneID:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        const milestone = (await db.doc(`milestones/${milestoneID}`).get()).data();

        if (!milestone) {
          reject("Milestone doesn't exist"); return;
        }

        if (uid !== milestone.receiver.uid) {
          reject('Only service provider can cancel a milestone'); return;
        }

        if (milestone.status !== 'created') {
          reject('This is not a created milestone'); return;
        }

        const balance = (await db.collection(`balances`)
          .where('uid','==',milestone.sender.uid)
          .where('currency','==',milestone.currencyForPayment.currency)
          .limit(1).get()
          ).docs[0].data();

        batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).plus(milestone.currencyForPayment.amount).toString(10),
          onEscrow: this.BigNumberSafeSub(balance.onEscrow, milestone.currencyForPayment.amount)
        });

        batch.update(db.doc(`milestones/${milestoneID}`), {
          status:'canceled'
        });
        
        await batch.commit();
        resolve();
        

      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }


  public releaseMilestone(uid:string, milestoneID:string, serviceProviderJobID:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();

        const [milestone, serviceProviderJob] =  (await db.getAll(
          db.doc(`milestones/${milestoneID}`), 
          db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`)
        )).map(doc => doc.data());


        if(milestone.status !== 'created') {
          reject('This is not a created milestone'); return;
        }

        if(milestone.serviceProviderJobID !== serviceProviderJob.id) {
          reject('Invalid job'); return;
        }

        if (uid !== milestone.sender.uid) {
          reject('Invalid user'); return;
        }

        const [senderBalanceSnapshot, receiverBalanceSnapshot, [employerSnapshot, serviceProviderSnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',milestone.sender.uid).where('currency','==',milestone.currencyForPayment.currency).limit(1).get(),
          db.collection(`balances`).where('uid','==',milestone.receiver.uid).where('currency','==',milestone.currencyForPayment.currency).limit(1).get(),
          db.getAll(db.doc(`users/${serviceProviderJob.employer.uid}`), db.doc(`users/${serviceProviderJob.serviceProvider.uid}`))
        ]);

        const totalUsersBalance = (await db.doc(`totalUsersBalances/${milestone.currencyForPayment.currency}`).get()).data();
          
        if (receiverBalanceSnapshot.empty) {
          reject("Service provider doesn't have a wallet for this currency. Ask him/her to create one"); return;
        }
            
        const employerInfo = employerSnapshot.data();
        const serviceProviderInfo = serviceProviderSnapshot.data();
      
        const [senderBalance, receiverBalance] = [senderBalanceSnapshot.docs[0].data(), receiverBalanceSnapshot.docs[0].data()];

        const percentageFee = '5';
        const fee = new BigNumber(milestone.currencyForPayment.amount).times(percentageFee).div(100).decimalPlaces(8).toString(10);
        const amountToReceive = new BigNumber(milestone.currencyForPayment.amount).minus(fee).toString(10);

        batch.update(db.doc(`milestones/${milestoneID}`), {
          status:'released'
        });

        batch.update(db.doc(`balances/${senderBalance.id}`), {
          onEscrow: this.BigNumberSafeSub(senderBalance.onEscrow, milestone.currencyForPayment.amount)
        });
        batch.update(db.doc(`totalUsersBalances/${milestone.currencyForPayment.currency}`), {
          amount: new BigNumber(totalUsersBalance.amount).minus(fee).toString()
        });

        batch.update(db.doc(`balances/${receiverBalance.id}`), {
          available: new BigNumber(receiverBalance.available).plus(amountToReceive).toString(10)
        });

        const id1 = shared.generateId();
        const id2 = shared.generateId();

        batch.set(db.doc(`transactions/${id1}`), {
          currency: milestone.currencyForPayment.currency,
          type: 'payment sent',
          details: `Milestone released for job: ${serviceProviderJob.name}`,
          serviceProviderJobID,
          processed: true,
          status: 'confirmed',
          amount: milestone.currencyForPayment.amount,
          uid: milestone.sender.uid,
          id: id1,
          creationTS: Date.now()
        });

        batch.set(db.doc(`transactions/${id2}`), {
          currency: milestone.currencyForPayment.currency,
          type: 'payment received',
          details: `Milestone released for job: ${serviceProviderJob.name}`,
          serviceProviderJobID,
          processed: true,
          status: 'confirmed',
          amount: amountToReceive,
          fee,
          uid: milestone.receiver.uid,
          id: id2,
          creationTS: Date.now()
        });

        const amountPaid = new BigNumber(serviceProviderJob.amountPaid).plus(milestone.amount).toString(10);
        batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJob.id}`), { amountPaid });

        if(serviceProviderJob.status=='ongoing' && new BigNumber(amountPaid).isGreaterThanOrEqualTo(serviceProviderJob.price)){

          batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJob.id}`), { status:'completed', culminationTS:Date.now() });
          
          batch.update(db.doc(`users/${employerInfo.uid}`), {
            [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${employerInfo.uid}`), {
          //   [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          // });
          batch.update(db.doc(`users/${employerInfo.uid}`), {
            [`${serviceProviderJob.employer.role}.employer.serviceProvidersHired`]:employerInfo[serviceProviderJob.employer.role].employer.serviceProvidersHired + 1
          });
          // batch.update(db.doc(`public/${employerInfo.uid}`), {
          //   [`${serviceProviderJob.employer.role}.employer.serviceProvidersHired`]:employerInfo[serviceProviderJob.employer.role].employer.serviceProvidersHired + 1
          // });

          batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
            'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
          //   'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          // });
          batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
            'serviceProvider.jobsCompleted':serviceProviderInfo.serviceProvider.jobsCompleted + 1
          });
          // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
          //   'serviceProvider.jobsCompleted':serviceProviderInfo.serviceProvider.jobsCompleted + 1
          // });

          const notifId = shared.generateId();
          batch.set(db.doc(`users/${serviceProviderInfo.uid}/notifications/${notifId}`), {
            title: `Job completed`,
            content: `Full payment for job has been sent`,
            id: notifId,
            creationTS: Date.now(),
          });

          batch.update(db.doc(`users/${serviceProviderInfo.uid}/settings/preferences`), {
            'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
          });
        
        }

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${serviceProviderInfo.uid}/notifications/${notifId}`), {
					title: `Milestone released`,
					content: `A milestone has been released`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${serviceProviderInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});

        const request = Database.pool.request();

        const SQLcommand = `
          UPDATE serviceProviders SET 
          [jobsCompleted] = ${serviceProviderInfo.serviceProvider.jobsCompleted + 1}
          WHERE uid = '${serviceProviderInfo.uid}'`;
          
        await request.query(SQLcommand);
        await batch.commit();
        resolve();


      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }


  public sendPayment(uid:string, currency:string, serviceProviderJobID:string, amount:number, details:number){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        let [lastPriceForMilestoneSnapshot, serviceProviderJobSnapshot] = await db.getAll(
          db.doc(`lastPriceForMilestone/${uid}`), 
          db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`)
        )

        const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();

        const serviceProviderJob = serviceProviderJobSnapshot.data();
        const newID = shared.generateId();

        const milestone = {
          amount:amount.toString(),
          details,
          serviceProviderJobID,
          creationTS:Date.now(),
          id:newID,
          status:'released',
          sender:{
            firstName:serviceProviderJob.employer.firstName,
            lastName:serviceProviderJob.employer.lastName,
            email:serviceProviderJob.employer.email,
            uid:serviceProviderJob.employer.uid
          },
          receiver:{
            firstName:serviceProviderJob.serviceProvider.firstName,
            lastName:serviceProviderJob.serviceProvider.lastName,
            email:serviceProviderJob.serviceProvider.email,
            uid:serviceProviderJob.serviceProvider.uid
          },
        };

        let [senderBalanceSnapshot, receiverBalanceSnapshot, [employerSnapshot, serviceProviderSnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',milestone.sender.uid).where('currency','==',currency).limit(1).get(),
          db.collection(`balances`).where('uid','==',milestone.receiver.uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`users/${serviceProviderJob.employer.uid}`), db.doc(`users/${serviceProviderJob.serviceProvider.uid}`))
        ]);
        

        if (uid !== milestone.sender.uid) {
          reject('Invalid user'); return;
        }

        if(receiverBalanceSnapshot.empty) {
          reject("Service provider doesn't have a wallet for this currency. Ask him/her to create one"); return;
        }

        if (serviceProviderJob.status !== 'ongoing' && serviceProviderJob.status !== 'completed') {
          reject('You can only send payments for either ongoing or completed jobs'); return;
        }   
        

        let employerInfo = employerSnapshot.data();
        let serviceProviderInfo = serviceProviderSnapshot.data();

        if (senderBalanceSnapshot.empty) {
          reject('Insufficient funds'); return;
        }
        
        let [senderBalance, receiverBalance] = [senderBalanceSnapshot.docs[0].data(), receiverBalanceSnapshot.docs[0].data()];

        let lastPriceForMilestone = lastPriceForMilestoneSnapshot.data();

        if (lastPriceForMilestone.currency !== currency) {
          reject('Something went wrong. Try again later'); return;
        }

        let amountToPay = new BigNumber(milestone.amount).div(lastPriceForMilestone.price.USD).decimalPlaces(8).toString(10);

        if (senderBalanceSnapshot.empty || !(new BigNumber(senderBalance.available).isGreaterThanOrEqualTo(amountToPay))) {
          reject('Insufficient funds'); return;
        }

        const percentageFee = '5';
        let fee = new BigNumber(amountToPay).times(percentageFee).div(100).decimalPlaces(8).toString(10);
        let amountToReceive = new BigNumber(amountToPay).minus(fee).toString(10);

        batch.update(db.doc(`balances/${senderBalance.id}`), {
          available: this.BigNumberSafeSub(senderBalance.available, amountToPay)
        });

        batch.update(db.doc(`totalUsersBalances/${currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(fee).toString()
				});


        batch.update(db.doc(`balances/${receiverBalance.id}`), {
          available: new BigNumber(receiverBalance.available).plus(amountToReceive).toString(10)
        });


        batch.set(db.doc(`milestones/${newID}`), {
          ...milestone, 
          currencyForPayment:{
            currency,
            amount:amountToPay,
            name:lastPriceForMilestone.name
          }
        });

        const id1 = shared.generateId();
        const id2 = shared.generateId();

        batch.set(db.doc(`transactions/${id1}`), {
          currency,
          type: 'payment sent',
          details: `Milestone released for job: ${serviceProviderJob.name}`,
          serviceProviderJobID,
          processed: true,
          status: 'confirmed',
          amount: amountToPay,
          uid: milestone.sender.uid,
          id: id1,
          creationTS: Date.now()
        });

        batch.set(db.doc(`transactions/${id2}`), {
          currency,
          type: 'payment received',
          details: `Milestone released for job: ${serviceProviderJob.name}`,
          serviceProviderJobID,
          processed: true,
          status: 'confirmed',
          amount: amountToReceive,
          fee,
          uid: milestone.receiver.uid,
          id: id2,
          creationTS: Date.now()
        });

        const amountPaid = new BigNumber(serviceProviderJob.amountPaid).plus(milestone.amount).toString(10);
        batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJob.id}`), { amountPaid });

        if(serviceProviderJob.status=='ongoing' && new BigNumber(amountPaid).isGreaterThanOrEqualTo(serviceProviderJob.price)){

          batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJob.id}`), { status:'completed', culminationTS:Date.now() });

          batch.update(db.doc(`users/${employerInfo.uid}`), {
            [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${employerInfo.uid}`), {
          //   [`${serviceProviderJob.employer.role}.employer.activeJobs`]:this.safeSub(employerInfo[serviceProviderJob.employer.role].employer.activeJobs, 1)
          // });
          batch.update(db.doc(`users/${employerInfo.uid}`), {
            [`${serviceProviderJob.employer.role}.employer.serviceProvidersHired`]:employerInfo[serviceProviderJob.employer.role].employer.serviceProvidersHired + 1
          });
          // batch.update(db.doc(`public/${employerInfo.uid}`), {
          //   [`${serviceProviderJob.employer.role}.employer.serviceProvidersHired`]:employerInfo[serviceProviderJob.employer.role].employer.serviceProvidersHired + 1
          // });

          batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
            'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          });
          // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
          //   'serviceProvider.activeJobs':this.safeSub(serviceProviderInfo.serviceProvider.activeJobs, 1)
          // });
          batch.update(db.doc(`users/${serviceProviderInfo.uid}`), {
            'serviceProvider.jobsCompleted':serviceProviderInfo.serviceProvider.jobsCompleted + 1
          });
          // batch.update(db.doc(`public/${serviceProviderInfo.uid}`), {
          //   'serviceProvider.jobsCompleted':serviceProviderInfo.serviceProvider.jobsCompleted + 1
          // });

          const notifId = shared.generateId();
          batch.set(db.doc(`users/${serviceProviderInfo.uid}/notifications/${notifId}`), {
            title: `Job completed`,
            content: `Full payment for job has been sent`,
            id: notifId,
            creationTS: Date.now(),
          });

          batch.update(db.doc(`users/${serviceProviderInfo.uid}/settings/preferences`), {
            'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
          });

        }

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${serviceProviderInfo.uid}/notifications/${notifId}`), {
					title: `Payment sent`,
					content: `A payment has been set for your job`,
					id: notifId,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${serviceProviderInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
				});

        const request = Database.pool.request();
        const SQLcommand = `
          UPDATE serviceProviders SET 
          [jobsCompleted] = ${serviceProviderInfo.serviceProvider.jobsCompleted + 1}
          WHERE uid = '${serviceProviderInfo.uid}'`;
          
        await request.query(SQLcommand);
        await batch.commit();
        resolve(newID);
          
      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }


  public provideFeedBack(uid:string, serviceProviderJobID:string, feedback:any){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        const id = shared.generateId();
        let serviceProviderJob = (await db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`).get()).data();
        let employerInfo = serviceProviderJob.employer;
        let serviceProviderInfo = serviceProviderJob.serviceProvider;
        let userToRate;

        if (uid==employerInfo.uid && serviceProviderJob.status=='completed' && !serviceProviderJob.employerProvidedFeedback && 
           feedback.score>0 && feedback.score<=5 && feedback.qualityOfWork>0 && feedback.qualityOfWork<=5 && 
           feedback.finishedOnTime>0 && feedback.finishedOnTime<=5){

          userToRate = (await db.doc(`users/${serviceProviderInfo.uid}`).get()).data();
          
          let serviceProviderObj = {
            'serviceProvider.score':new BigNumber( ((userToRate.serviceProvider.score*userToRate.serviceProvider.reviews) + feedback.score)/(userToRate.serviceProvider.reviews+1) ).decimalPlaces(1).toNumber(),
            'serviceProvider.reviews':userToRate.serviceProvider.reviews+1,
            'serviceProvider.qualityOfWork':new BigNumber( ((userToRate.serviceProvider.qualityOfWork*userToRate.serviceProvider.reviews) + feedback.qualityOfWork)/(userToRate.serviceProvider.reviews+1) ).decimalPlaces(1).toNumber(),
            'serviceProvider.finishedOnTime':new BigNumber( ((userToRate.serviceProvider.finishedOnTime*userToRate.serviceProvider.reviews) + feedback.finishedOnTime)/(userToRate.serviceProvider.reviews+1) ).decimalPlaces(1).toNumber(),
          };

          batch.update(db.doc(`users/${userToRate.uid}`), serviceProviderObj);
					// batch.update(db.doc(`public/${userToRate.uid}`), serviceProviderObj);
					batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`), {employerProvidedFeedback:true});
					batch.set(db.doc(`users/${userToRate.uid}/reviews/${id}`), {
            role:'service provider',
            reason:'serviceProvidersJob',
            serviceProviderJobID,
						feedback:{
							score:feedback.score,
              qualityOfWork:feedback.qualityOfWork,
              finishedOnTime:feedback.finishedOnTime,
							review:feedback.review || ''
						},
						providedBy:{
							uid,
							role:employerInfo.role
						},
						id,
						creationTS: Date.now()
          });
          
          const request = Database.pool.request();

          const SQLcommand = `
            UPDATE serviceProviders SET 
            [score] = ${serviceProviderObj['serviceProvider.score']},
            [reviews] = ${serviceProviderObj['serviceProvider.reviews']} 
            WHERE uid = '${userToRate.uid}'`;

          await request.query(SQLcommand);
					await batch.commit();
					resolve();

        }else if(uid==serviceProviderInfo.uid && serviceProviderJob.status=='completed' && !serviceProviderJob.serviceProviderProvidedFeedback && 
                feedback.score>0 && feedback.score<=5 && feedback.paymentPromptness>0 && feedback.paymentPromptness<=5){
          
          userToRate = (await db.doc(`users/${employerInfo.uid}`).get()).data();

          let employerObj = {
            [`${employerInfo.role}.score`]:new BigNumber( ((userToRate[employerInfo.role].score*userToRate[employerInfo.role].reviews) + feedback.score)/(userToRate[employerInfo.role].reviews+1) ).decimalPlaces(1).toNumber(),
            [`${employerInfo.role}.reviews`]:userToRate[employerInfo.role].reviews+1,
            [`${employerInfo.role}.employer.paymentPromptness`]:new BigNumber( ((userToRate[employerInfo.role].employer.paymentPromptness*userToRate[employerInfo.role].employer.reviews) + feedback.paymentPromptness)/(userToRate[employerInfo.role].employer.reviews+1) ).decimalPlaces(1).toNumber(),
            [`${employerInfo.role}.employer.reviews`]:userToRate[employerInfo.role].employer.reviews+1
          };

          batch.update(db.doc(`users/${userToRate.uid}`), employerObj);
					// batch.update(db.doc(`public/${userToRate.uid}`), employerObj);
					batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`), {serviceProviderProvidedFeedback:true});
					batch.set(db.doc(`users/${userToRate.uid}/reviews/${id}`), {
            role:employerInfo.role,
            reason:'serviceProvidersJob',
            serviceProviderJobID,
						feedback:{
							score:feedback.score,
							paymentPromptness:feedback.paymentPromptness,
							review:feedback.review || ''
						},
						providedBy:{
							uid,
							role:'service provider'
						},
						id:id,
						creationTS:Date.now()
					});

					await batch.commit();
					resolve();

        }else{
          reject("You can't rate this user");
        }

      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }





  startChat(uid:string, serviceProviderJobID:string, content:string){
    return new Promise(async (resolve, reject)=>{
      try {

        const db = admin.firestore();
        const batch = db.batch();
        const chatID = shared.generateId();
        const messageID = shared.generateId();
        let serviceProviderJob = (await db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`).get()).data();
        let employerInfo = serviceProviderJob.employer;
        let serviceProviderInfo = serviceProviderJob.serviceProvider;

        if (uid==employerInfo.uid || uid==serviceProviderInfo.uid) {
          if(!serviceProviderJob.chatID){

            batch.update(db.doc(`myServiceProvidersJobs/${serviceProviderJobID}`), { chatID });

            batch.set(db.doc(`chats/${chatID}`), {
              chatName: serviceProviderJob.name,
              id: chatID,
              lastMessage: {
                content,
                uid,
                creationTS: Date.now()
              },
              uids: [employerInfo.uid, serviceProviderInfo.uid]
            });

            batch.set(db.doc(`chats/${chatID}/messages/${messageID}`), {
              content,
              uid,
              creationTS: Date.now(),
              id: messageID
            });

            await batch.commit();
            resolve({chatID, messageID});

          }else{
            reject('Unexpected error. Close and open the chat window');
          }
          
        } else {
          reject('Invalid user');
        }
        
      } catch (err) {
        reject('Unexpected error sending message. Try again later');
      }
    });
  }





  private safeSub(a:number, b:number){
    return a-b>=0? a-b : 0;
  }

  private BigNumberSafeSub(a, b){
    return new BigNumber(a).minus(b).isGreaterThanOrEqualTo(0)? new BigNumber(a).minus(b).toString(10) : '0'
  }



}



