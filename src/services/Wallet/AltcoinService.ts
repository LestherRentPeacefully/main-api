const bitcoin = require('bitcoinjs-lib');
const zcash = require('zcashjs-lib');
import {environment} from '../../environments/environment';
const fetch = require('node-fetch');
import {BigNumber} from 'bignumber.js';
const coinSelect = require('coinselect');
const bchaddr = require('bchaddrjs');


export class AltcoinService{ //CHECK BCH ADDRESS
  
  public createPersonalWallet(currency:string):Promise<{address:string, privateKey:string, wif:string, legacyAddress:string}>{
    return new Promise(async(resolve,reject)=>{
      try {
        // const altcoin = currency=='ZEC'? zcash : bitcoin;        
        // const keyPair = altcoin.ECPair.makeRandom({network:environment.blockchain[currency].network});
        // const { address } = altcoin.payments.p2pkh({ pubkey: keyPair.publicKey, network:environment.blockchain[currency].network });
        // const address =  await this.handleRequest(currency, 'getnewaddress', 'depositWallets');
        const address =  await this.handleRequest(currency, 'getnewaddress');
        const wif = await this.handleRequest(currency, 'dumpprivkey', address);
        const privateKey = await this.toPrivateKey(currency, wif);

        // await this.handleRequest(currency, 'importaddress', address,'depositWallets', false);

        let cashAddr:string;
        if(currency=='BCH') cashAddr = await this.toCashAddress(address);

        resolve({
          address: currency=='BCH'? cashAddr.substring(cashAddr.indexOf(':') + 1) : address,
          legacyAddress: currency=='BCH'? address : undefined,
          privateKey,
          wif
        });
        
      } catch (err) {
        reject(err.message || err);
      }
    });
  }

  public transfer(currency:string, from:string, privateKey:string, to:string, amount:number){
    return new Promise(async(resolve, reject)=>{
      try {

        const altcoin = currency=='ZEC'? zcash : bitcoin;
        const zcashFee = currency=='ZEC'? 0.0001 : 0;
        
        let utxos:any[] = await this.listUnspentUTXO(currency, from);

        let balance = '0';

        for(let i in utxos){
          utxos[i].value = +this.toSatoshi(utxos[i].amount, currency);
          balance = new BigNumber(balance).plus(utxos[i].amount).toString(10);
          delete utxos[i].amount;
        }

        if (!(new BigNumber(balance).isGreaterThan(+amount+zcashFee))) {
          reject('Insufficient funds'); return;
        }


        let estimatefeePerKb:number; //0.000054 5.7884298 doge

        if (currency=='ZEC') estimatefeePerKb = 0;
        else estimatefeePerKb = (await this.estimatefeePerKb(currency)).feerate;
        // else estimatefeePerKb = 5.7884298;

        if(currency!='ZEC' && !(estimatefeePerKb>0)){
          reject(`Wrong fee per Kb value: ${this.estimatefeePerKb}`); return;
        }


        let estimatefeeInSatoshiPerByte = this.toSatoshi(new BigNumber(estimatefeePerKb).div(1000), currency);

        let { inputs, outputs, fee } = coinSelect(utxos, 
          [{
            address: currency=='BCH'? await this.toLegacyAddress(to) : to, 
            value:+this.toSatoshi(+amount+zcashFee, currency) 
          }], 
          +estimatefeeInSatoshiPerByte);
        
        fee = this.toBitcoin(fee, currency); //CHECK INSUFFICIENT FUNDS COND.

        const txb = new altcoin.TransactionBuilder(environment.blockchain[currency].network);
        if(currency=='DASH') txb.setVersion(2);
        else txb.setVersion(1);

        
        inputs.forEach(input => txb.addInput(input.txid, input.vout));

        for(let output of outputs){
          // watch out, outputs may have been added that you need to provide an output address/script for
          if (!output.address) output.address = currency=='BCH'? await this.toLegacyAddress(from) : from;
          else output.value = +output.value - (+this.toSatoshi(zcashFee, currency));

          txb.addOutput(output.address, output.value);
        }


        const keyPair = altcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'), {network:environment.blockchain[currency].network});

        let rawTx = currency=='BCH'? txb.buildIncomplete().toHex() : '';
        
        for(let i = 0; i<inputs.length; i++){
          if(currency=='BCH') rawTx = (await this.signrawtransaction('BCH', rawTx, keyPair.toWIF())).hex;
          else txb.sign(i, keyPair);
        }

        let hash = await this.sendrawtransaction(currency, currency=='BCH'? rawTx : txb.build().toHex());

        resolve(hash);

      

        
      } catch (err) {
        reject(err.message || err);
      }

    });
  }

  public estimatePlaformWithdrawalFee(currency: string, to: string, amount: string) {
    return new Promise(async(resolve, reject)=>{
      try {
        
        const zcashFee = currency === 'ZEC'? 0.0001 : 0;
        
        const utxos: any[] = await this.listPlatformUnspentUTXO(currency);

        // let balance = '0';

        for(const i in utxos){
          utxos[i].value = +this.toSatoshi(utxos[i].amount, currency);
          delete utxos[i].amount;
          // balance = new BigNumber(balance).plus(utxos[i].amount).toString(10);
        }

        // if (!(new BigNumber(balance).isGreaterThan(+amount + zcashFee))) {
        //   reject('Insufficient funds'); return;
        // }


        let estimatefeePerKb: number; //0.000054 5.7884298 doge

        if (currency=='ZEC') estimatefeePerKb = 0;
        else estimatefeePerKb = (await this.estimatefeePerKb(currency)).feerate;
        // else estimatefeePerKb = 5.7884298;

        if(currency !== 'ZEC' && !(estimatefeePerKb>0)){
          reject(`Wrong fee per Kb value: ${this.estimatefeePerKb}`); return;
        }


        const estimatefeeInSatoshiPerByte = this.toSatoshi(new BigNumber(estimatefeePerKb).div(1000), currency);

        let { inputs, outputs, fee } = coinSelect(utxos, 
          [{
            address: currency=='BCH'? await this.toLegacyAddress(to) : to, 
            value:+this.toSatoshi(+amount+zcashFee, currency) 
          }], 
          +estimatefeeInSatoshiPerByte);
        
        fee = new BigNumber(this.toBitcoin(fee, currency)).plus(zcashFee).toString();

        resolve(fee);


      } catch (err) {
        console.log(err.message || err);
        reject('Unexpected error. Try again later');
      }
    });
  }


  public async estimatefeePerKb(currency:string){
    if(currency=='BCH') return {feerate: await this.handleRequest(currency, 'estimatefee', 2)};
    if(currency=='PPC') return {feerate: 0.01};
    return this.handleRequest(currency, 'estimatesmartfee', 2);
  }

  public listUnspentUTXO(currency:string, address:string){
    return this.handleRequest(currency, 'listunspent', 1, 9999999, [address]);
  }

  public listPlatformUnspentUTXO(currency: string){
    return this.handleRequest(currency, 'listunspent', environment.blockchain[currency].minConfirm);
  }

  public signrawtransaction(currency:string, rawTx:string, wif:string){
    return this.handleRequest(currency, 'signrawtransaction', rawTx, null, [wif]);
  }

  public toLegacyAddress(address:string){
    return new Promise((resolve, reject)=>{
      try {
        resolve(bchaddr.toLegacyAddress(address));
        
      } catch (err) {
        reject(err.message);
      }
    });
  }

  public toCashAddress(address:string):Promise<string>{
    return new Promise((resolve, reject)=>{
      try {
        resolve(bchaddr.toCashAddress(address));
        
      } catch (err) {
        reject(err.message);
      }
    });
  }

  public sendrawtransaction(currency:string, rawTx:string){
    return this.handleRequest(currency, 'sendrawtransaction', rawTx);
  }

  public toBitcoin(amount:number | string | BigNumber, currency:string){
    let decimals = environment.blockchain[currency].decimals;
    return new BigNumber(amount).times(new BigNumber(10).pow(-decimals)).decimalPlaces(decimals).toString(10);
  }

  public toSatoshi(amount:number | string | BigNumber, currency:string){
    let decimals = environment.blockchain[currency].decimals;
    return new BigNumber(amount).decimalPlaces(decimals).times(new BigNumber(10).pow(decimals)).toString(10);
  }
  
  public async toWIF(currency:string, privateKey:string){
    const altcoin = currency=='ZEC'? zcash : bitcoin;
    const keyPair = altcoin.ECPair.fromPrivateKey(Buffer.from(privateKey, 'hex'), {network:environment.blockchain[currency].network});
    return keyPair.toWIF();
  }

  public async toPrivateKey(currency:string, wif:string){
    const altcoin = currency=='ZEC'? zcash : bitcoin;
    const keyPair = altcoin.ECPair.fromWIF(wif, environment.blockchain[currency].network);
    return keyPair.privateKey.toString('hex');
  }

  public async isValidAddress(currency:string, address:string):Promise<boolean>{
    return (await this.handleRequest(currency, 'validateaddress', address)).isvalid;
  }

  
  private handleRequest(currency:string, method:string, ...params:(string | number | boolean | string[])[]):Promise<any>{
    return new Promise(async (resolve, reject)=>{
      try {
        let res = await fetch(environment.blockchain[currency].nodeURL, { 
          method: 'POST',
          body: JSON.stringify({
            id:"curltest", 
            method, 
            params
          }),
          headers: { 
            'Content-Type': 'application/json',
            'Authorization': `Basic ${Buffer.from(`${environment.altcoinNodesAuth.user}:${environment.altcoinNodesAuth.password}`).toString('base64')}`
          },
        });
  
        let response = await res.json();
        
        if (!response.error) {
          resolve(response.result);
          
        } else {
          if (method=='getrawtransaction') resolve(null);
          else reject(response.error.message);
        }
        
      } catch (err) {
        reject(err.message || err);
        // reject('Unexpected error. Try again later!');
      }


    });
  }


}