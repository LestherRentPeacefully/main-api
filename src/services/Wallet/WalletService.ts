import {EthereumService} from './EthereumService';
import {AltcoinService} from './AltcoinService';
import * as admin from "firebase-admin";
import {SharedService} from '../Shared/SharedService';
import {BigNumber} from 'bignumber.js';
import {Nodemailer} from "../Email/Nodemailer";
import {environment} from '../../environments/environment';

const sharedService = new SharedService();

const ethereumService = new EthereumService();

const altcoinService = new AltcoinService();



export class WalletService{


  create(currency:string, uid:string){
    return new Promise(async (resolve, reject)=>{

      try {
        const db = admin.firestore();

        const walletSnapshot = (await db.collection(`wallets/deposit/public`)
          .where('uid','==',uid)
          .where('currency','==',currency)
          .limit(1)
          .get());

        if (!walletSnapshot.empty) {
          reject('Wallet already created'); return;
        }

        if(!sharedService.currencies.includes(currency)){
          reject('Currency not supported'); return;
        }
          
        const generatedWallet = await this.createPersonalWallet(currency);
        
        const batch = db.batch();
        const id = sharedService.generateId();

        const walletObj = {
          currency,
          uid,
          id,
          searchable:generatedWallet.address.toLowerCase(),
          address:generatedWallet.address
        };

        if(currency=='BCH') walletObj['legacyAddress'] = generatedWallet.legacyAddress;

        batch.set(db.doc(`wallets/deposit/public/${id}`), walletObj);
        
        walletObj['privateKey'] = generatedWallet.privateKey;
        
        if(currency!='ETH') walletObj['wif'] = generatedWallet.wif;

        batch.set(db.doc(`wallets/deposit/private/${id}`), walletObj);

        batch.set(db.doc(`balances/${id}`), {
          currency,
          uid,
          id,
          available:'0',
          onOrders:'0',
          onEscrow:'0'
        });
        
        await batch.commit();
        resolve(generatedWallet.address);


        
      } catch (err) {
        reject('Unexpected error creating wallet');
      }
    });

  }


  async createPersonalWallet(currency:string):Promise<{address:string, privateKey:string, wif?, legacyAddress?}>{ //CHECK BCH ADDRESS
    return currency=='ETH'? ethereumService.createPersonalWallet() : altcoinService.createPersonalWallet(currency);
  }

  
  async isValidAddress(currency:string, address:string){
    if (currency=='ETH') return ethereumService.isValidAddress(address);
    return altcoinService.isValidAddress(currency, address);
  }


  async getWithdrawalFee(currency:string){
    if (currency=='ZEC')  return '0.001'; //0.0001

    if(currency=='ETH'){
      let estimateGas = '21000';
      let gasPrice = new BigNumber(ethereumService.getGasPrice()).times(2.0).toString(10);
      return ethereumService.toEther(new BigNumber(estimateGas).times(gasPrice) );
    }

     //in*180 + out*34 + 10 plus or minus 'in' => (40)*180 + (16)*34 + 10 +- (40)
    //NEW => in*148 + out*34 + 10 plus or minus 'in'
    let estimatefeePerKb = (await altcoinService.estimatefeePerKb(currency)).feerate; //0.000054 5.7884298 doge
    return sharedService.formatCryptoAmount(currency, new BigNumber(estimatefeePerKb).div(1000).times((5*148) + (1*34) + 10 + 5) );
  }


  checkWithdrawalAddress(uid:string, currency:string, address:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const isValidAddress = await this.isValidAddress(currency, address);

        if (!isValidAddress) {
          reject('Invalid Address'); return;
        }

        const db = admin.firestore();

        let [receiverWalletSnapshot] = await Promise.all([
          db.collection(`wallets/deposit/public`).where('currency','==',currency).where('searchable','==',address.toLowerCase()).limit(1).get()
        ]);

        const isInternal = !receiverWalletSnapshot.empty;

        if (isInternal && receiverWalletSnapshot.docs[0].data().uid==uid) {
          reject("You can't transfer to yourself"); return;
        }
        
        resolve({isInternal});

        
      } catch (err) {
        reject('Unexpected error. Try again later');
      }
    });
  }


  async sendWithdrawalCode(uid:string){
    const db = admin.firestore();

    const code = String(Math.floor(Math.random()*10)) + 
                String(Math.floor(Math.random()*10)) + 
                String(Math.floor(Math.random()*10)) +
                String(Math.floor(Math.random()*10)) +
                String(Math.floor(Math.random()*10));
              
    let userInfo = (await db.doc(`users/${uid}`).get()).data();

    let msg = {
      from:`${environment.appName} <${environment.noreplyEmail}>`,
      to:userInfo.email,
      subject:`${environment.appName}: Withdrawal Confirmation`,
      html:`<p>Hello, ${sharedService.capitalize(userInfo.firstName)}</p>
            <p>This is your verification code to confirm your withdrawal: <strong>${code}</strong></p>`
    };
    const nodemailer = new Nodemailer(msg);
    
    await db.doc(`withdrawalCodes/${uid}`).set({
      code,
      uid,
      expirationDate:new Date().setHours(new Date().getHours()+2)
    });
    
    return nodemailer.sendMail();
  }


  withdraw(uid:string, currency:string, code:string, to:string, amount:number){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();

        const [isValidAddress, codeOnDBSnapshot] = await Promise.all([this.isValidAddress(currency, to), db.doc(`withdrawalCodes/${uid}`).get()]);

        const codeOnDB = codeOnDBSnapshot.data().code;

        if (!isValidAddress) {
          reject('Invalid Address'); return;
        }

        if (code!=codeOnDB) {
          reject('Wrong code'); return;
        }
          
        
        let [senderBalanceSnapshot, fee, receiverWalletSnapshot] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          this.getWithdrawalFee(currency),
          db.collection(`wallets/deposit/public`).where('currency','==',currency).where('searchable','==',to.toLowerCase()).limit(1).get()
        ]);

        if (senderBalanceSnapshot.empty) {
          reject('Insufficient funds'); return;
        }

        const senderbalance = senderBalanceSnapshot.docs[0].data();
        const formatedAmount = sharedService.formatCryptoAmount(currency, amount);
        const isInternal = !receiverWalletSnapshot.empty;

        if(isInternal) fee = '0';  //It's internal withdrawal

        const amountToPay = new BigNumber(formatedAmount).plus(fee).toString(10);

        if (senderBalanceSnapshot.empty || !(new BigNumber(senderbalance.available).isGreaterThanOrEqualTo(amountToPay))) {
          reject('Insufficient funds'); return;
        }

        const id1 = sharedService.generateId();

        batch.delete(db.doc(`withdrawalCodes/${uid}`));

        batch.update(db.doc(`balances/${senderbalance.id}`), {
          available: new BigNumber(senderbalance.available).minus(amountToPay).toString(10)
        });

        batch.set(db.doc(`transactions/${id1}`), {
          currency,
          type: 'withdrawal',
          isInternal,
          processed: true,
          status: isInternal? 'confirmed' : 'pending',
          amount: formatedAmount,
          to,
          fee,
          uid,
          id: id1,
          creationTS: Date.now()
        });


        if(isInternal){
          const receiverWallet = receiverWalletSnapshot.docs[0].data();

          if (receiverWallet.uid == uid) {
            reject("You can't transfer to yourself"); return;
          }

          const receiverBalance = (await db.collection(`balances`).where('uid','==',receiverWallet.uid).where('currency','==',currency).limit(1).get()).docs[0].data();
          
          const id2 = sharedService.generateId();

          batch.update(db.doc(`balances/${receiverBalance.id}`), {
            available: new BigNumber(receiverBalance.available).plus(formatedAmount).toString(10)
          });          

          batch.set(db.doc(`transactions/${id2}`), {
            currency,
            type: 'deposit',
            isInternal,
            processed: true,
            status: 'confirmed',
            amount: formatedAmount,
            fee,
            uid: receiverWallet.uid,
            id: id2,
            creationTS: Date.now()
          });

        }
        
        await batch.commit();
        resolve();


      } catch (err) {
        reject('Unexpected error processing withdrawal. Try again later');
      }
    });
  }



  
}