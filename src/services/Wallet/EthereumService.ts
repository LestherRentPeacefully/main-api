const ethereumjsWallet = require('ethereumjs-wallet');
import * as web3 from 'web3';
import * as Tx from 'ethereumjs-tx';
import * as admin from "firebase-admin";
const https = require("https");
import {environment} from '../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {Nodemailer} from "../Email/Nodemailer";


export class EthereumService{
	private web3:any;

	constructor(){
		this.web3 = new web3();
		this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));
	}

	public create(password: string): Promise<{address: string, privateKey: string, V3: any}>{
		return new Promise((resolve,reject)=>{		
			if (!password) {
				reject('Password required'); return;
			}

			const generatedWallet = ethereumjsWallet.generate();

			if (!generatedWallet) {
				reject('There was an error. Try again later'); return;
			}

			resolve({
				V3:generatedWallet.toV3(password), 
				address:generatedWallet.getChecksumAddressString(),
				privateKey:this.cleanPrefix(generatedWallet.getPrivateKeyString())
			});

		});
	}

	public createPersonalWallet():Promise<{address:string, privateKey:string}>{
		return new Promise((resolve,reject)=>{
			try{
			let generatedWallet = ethereumjsWallet.generate();

			resolve({
				address:generatedWallet.getChecksumAddressString(), 
				privateKey:this.cleanPrefix(generatedWallet.getPrivateKeyString())
			});
			}catch(err){
				reject('Unexpected error generating wallet. Try again later');
			}
		});
	}


	public getGasPrice(){
		return this.web3.eth.gasPrice.toString(10);
	}


	public getBalances(uid:string){
		const db = admin.firestore();
		return new Promise(async (resolve, reject)=>{
			try {
				
			let identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get()).docs[0].data();
			
			let balances = {
				ETH: this.web3.eth.getBalance(identityWallet.address).toString(10)
			}

			resolve({success:true,data:balances,error:null});
				
			} catch (err) {
				reject('Unable to get ETH account');
			}
			
		});
	}

	public estimateGasAndGasPrice(from:string,to:string,amount:string):Promise<any>{
		let gasPrice = this.web3.eth.gasPrice.toString(10);

		return new Promise((resolve,reject)=>{		
			try{

				let	estimateGas = this.web3.eth.estimateGas({
					nonce: this.web3.toHex(this.web3.eth.getTransactionCount(from)),
					from,
					to,
					value: this.web3.toHex(amount)
				});

		  	resolve({estimateGas, gasPrice});
			}catch(err){
				console.log(err.message || err);

				reject('Not enough funds to send the transaction');
			}
		});

	}

	public send(uid:string,to:string,amount:string,gasPrice:string,gasLimit:string):Promise<any>{
		const db = admin.firestore();
		return new Promise(async (resolve,reject)=>{
			try{
					let identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get()).docs[0].data();

					let	rawTx = {
						nonce: this.web3.toHex(this.web3.eth.getTransactionCount(identityWallet.address)),
						gasPrice: this.web3.toHex(gasPrice),
						gasLimit: this.web3.toHex(gasLimit),
						to: to,
						value: this.web3.toHex(amount)
					}
					
					// Generate tx
	        let tx = new Tx(rawTx);
	        tx.sign(Buffer.from(identityWallet.privateKey, 'hex')); //Sign transaction
	        let serializedTx = `0x${tx.serialize().toString('hex')}`;
	          this.web3.eth.sendRawTransaction(serializedTx, (err, hash)=> {
	            if(err){
	            	reject(err.message || err);
	            } else{
	            	resolve(hash);
	            }               
	        });

				}catch(err){
					reject('Error sending transaction. Please, check the parameters introduced');
				}
		});
	}

	public getTransaction(txHash:string):Promise<any>{
		return new Promise((resolve,reject)=>{
			try{
				let transactionReceipt;
				let batch = this.web3.createBatch();

				batch.add(this.web3.eth.getTransaction.request(txHash,(err,response)=>{
					if (!err) { 
						transactionReceipt = response;
						transactionReceipt.gasLimit = transactionReceipt.gas;
						
						delete transactionReceipt.gas, delete transactionReceipt.publicKey, delete transactionReceipt.r;
						delete transactionReceipt.raw, delete transactionReceipt.s, delete transactionReceipt.v;
						delete transactionReceipt.standardV, delete transactionReceipt.input, delete transactionReceipt.chainId;
						delete transactionReceipt.condition;
					} else {
						reject(String(err));
					}
				}));

				batch.add(this.web3.eth.getTransactionReceipt.request(txHash,(err,response)=>{
					if (!err) { 
						transactionReceipt.gasUsed = response.gasUsed;
						transactionReceipt.status = response.status;

						resolve(transactionReceipt);
					} else {
						reject(String(err));
					}
				}));

				batch.execute();

			}catch(err){
				reject(String(err));
			}
		});
	}


  public getTransactions(address:string, currency:string):Promise<any>{
  	//let path = currency =='ETH'? `/api?module=account&action=txlist&address=${address}&sort=desc` :`/api?module=account&action=tokenbalance&contractaddress=${environment.tokenContractAddress}&address=${address}&tag=latest`;
    let path =`/api?module=account&action=txlist&address=${address}&sort=desc`
    const options = {
      host: environment.etherscanURL,
      path: path, 
      method: 'GET',
      headers: {
      'Content-Type': 'application/json'
      }
    };
  	return this.handleRequest(options);
	}
	

	public sendAndUploadWalletFileVerificationCode(uid: string):Promise<any>{
		return new Promise(async(resolve, reject)=>{
      try {
        const db = admin.firestore();
  
        const code = String(Math.floor(Math.random()*10)) + 
                  String(Math.floor(Math.random()*10)) + 
                  String(Math.floor(Math.random()*10)) +
                  String(Math.floor(Math.random()*10)) +
                  String(Math.floor(Math.random()*10));
  
        const userInfo = (await db.doc(`users/${uid}`).get()).data();
  
        await db.doc(`walletFileVerificationCodes/${uid}`).set({code});

        let msg = {from:`${environment.appName} <${environment.noreplyEmail}>`,
                to:userInfo.email,
                subject:'Verification code',
                html:`<p>This is your verification code to export your encrypted Wallet File: <strong>${code}</strong><p>.
                      <p>Rembember: your Wallet File is encrypted with your user password.<p>`
        };

        const nodemailer = new Nodemailer(msg);

        await nodemailer.sendMail()
        resolve();
        
      } catch (err) {
        reject('An error has ocurred. Try again later');
      }

		});
  }


  public getWalletFileAndVerifyCode(uid: string, code: string):Promise<any>{
  	const db = admin.firestore();
  	return new Promise(async(resolve,reject)=>{
			try {
				let [privateInfoSnapshot, idWalletSnapshot] =  await Promise.all([
					db.doc(`walletFileVerificationCodes/${uid}`).get(), 
					db.collection(`wallets/identity/private`).where('uid','==',uid).limit(1).get()
				]);
	
				let verificationCode = privateInfoSnapshot.data().code;

				if (verificationCode==String(code)) {

					let walletFile = idWalletSnapshot.docs[0].data().walletFile;

					resolve(walletFile);
					
				} else {
					reject({success:false,data:null,error:{message:'Wrong verfication code'}});
				}
				
			} catch (err) {
				reject('Unexpected error verifying code');
			}
  	});
	}
	

	public toEther(amount:number | string | BigNumber){
    return new BigNumber(amount).times(new BigNumber(10).pow(-18)).decimalPlaces(18).toString(10);
  }

  public toWei(amount:number | string | BigNumber){
    return new BigNumber(amount).decimalPlaces(18).times(new BigNumber(10).pow(18)).toString(10);
	}
	
	public isValidAddress(address:string):boolean{
		return this.web3.isAddress(address);
	}



	private cleanPrefix(data:string){
		return data.indexOf('0x')==0? data.substring(2): data;
	}


	private handleRequest(options: any, info?:any): Promise<any> {
    return new Promise((resolve,reject)=>{
    	try{
        const req = https.request(options, (response)=>{
            let data = '';
            response.on('data',  (chunk)=> {
                data += chunk;
            });
            response.on('end',  ()=> {
                if (options.method =='DELETE') {
                    resolve(true);
                }else{
                    try{
                        resolve(JSON.parse(data));
                    }catch(err){
                        reject(err);
                    }
                }
            });
            response.on('error',(e) =>{
                reject(e);
            })
        });
        if(info!=undefined)
            req.write(JSON.stringify(info));
        req.end();

    	}catch(err){
    		reject(err);
    	}
    });
   }

}