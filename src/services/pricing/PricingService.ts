import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import {SharedService} from '../Shared/SharedService';

const shared = new SharedService();

export class PricingService {

  public async getCurrencyPrice(uid:string, currency:string){
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForPlan/${uid}`).set({currency, price, name});
    return {currency, price};
  }


  public buyPricingPlan(uid:string, currency:string, plan:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();

        let [balanceSnaphot, [lastPriceForPlanSnapshot, userSnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`lastPriceForPlan/${uid}`), db.doc(`users/${uid}`))
        ]);

        if (balanceSnaphot.empty) {
          reject('Insufficient funds'); return;
        }

        const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();
        
        let balance = balanceSnaphot.docs[0].data();
        let lastPriceForPlan = lastPriceForPlanSnapshot.data();
        let userInfo = userSnapshot.data();

        if (lastPriceForPlan.currency !== currency || !shared.pricings[plan].amount) {
          reject('Something went wrong. Try again later'); return
        }
  
        const amountToPay = new BigNumber(shared.pricings[plan].amount).div(lastPriceForPlan.price.USD).decimalPlaces(8).toString(10);
  
        if (balanceSnaphot.empty || !(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))) {
          reject('Insufficient funds'); return;
        }

        if (userInfo.membership.pricingPlan === plan) {
          reject('You already have this plan'); return;
        }

        const batch = db.batch();
        const id = shared.generateId();

        batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).minus(amountToPay).toString(10)
        });

        batch.update(db.doc(`totalUsersBalances/${currency}`), {
          amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
        });

        batch.update(db.doc(`users/${uid}`), {
          membership:{
            pricingPlan: plan,
            paymentMethod:{
              currency,
              name: lastPriceForPlan.name,
            },
            nextPayment: this.addMonths(new Date(), 1).getTime()
          }
        });

        // batch.update(db.doc(`public/${uid}`), {
        //   membership:{
        //     pricingPlan: plan,
        //     paymentMethod: {
        //       currency,
        //       name: lastPriceForPlan.name,
        //     },
        //     nextPayment: this.addMonths(new Date(), 1).getTime()
        //   }
        // });

        batch.set(db.doc(`transactions/${id}`), {
          currency: currency,
          type: 'membership',
          details: `Payment for ${plan} plan (${shared.pricings[plan].amount})`,
          processed: true,
          status: 'confirmed',
          amount: amountToPay,
          uid,
          id,
          creationTS: Date.now()
        });

        const notifId = shared.generateId();
        batch.set(db.doc(`users/${userInfo.uid}/notifications/${notifId}`), {
          title: `Pricing plan purchased`,
          content: `You're now a ${plan} member`,
          id: notifId,
          creationTS: Date.now(),
        });

        batch.update(db.doc(`users/${userInfo.uid}/settings/preferences`), {
          'counters.unreads.notifications': admin.firestore.FieldValue.increment(1)
        });

        
        await batch.commit();
        resolve();
          

      } catch (err) {
        reject('Unexpected error. Try again later');
      }
      
    });
  }


  // private pricingInUSD(){
  //   return { basic:'4.95', plus:'9.95', professional:'14.95', enterprise:'19.95' };
  // }


  private addMonths (oldDate:number | Date, count:number) {
    const date = new Date(oldDate);
    const day = date.getDate();
    date.setMonth(date.getMonth() + count, 1);
    const month = date.getMonth();
    date.setDate(day);
    if (date.getMonth() !== month) date.setDate(0);
    return date;
  }

  public cancelPricingPlan(uid:string){
    const db = admin.firestore();
    const batch = db.batch();

    batch.update(db.doc(`users/${uid}`), {
      membership:{
        pricingPlan:'free'
      }
    });

    // batch.update(db.doc(`public/${uid}`), {
    //   membership:{
    //     pricingPlan:'free'
    //   }
    // });

    return batch.commit();
  }

  
  public selectPaymentMethod(uid:string, currency:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
    
        let [currencySnapshot, balanceSnaphot] = await Promise.all([
          db.doc(`currencies/${currency}`).get(),
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get()
        ]);

        if(balanceSnaphot.empty){
          reject('Create this wallet first'); return;
        }
    

        let {name} = currencySnapshot.data();
    
        batch.update(db.doc(`users/${uid}`), {
          'membership.paymentMethod':{ currency, name }
        });
    
        // batch.update(db.doc(`public/${uid}`), {
        //   'membership.paymentMethod':{ currency, name }
        // });
    
        await batch.commit();
        resolve();
        
      } catch (err) {
        reject('Unexpected error. Try again later');
      }

    });
  }



}