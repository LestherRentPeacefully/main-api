import {environment} from '../../environments/environment';
import {SharedService} from '../Shared/SharedService';
import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import * as web3 from 'web3';
const hellosign = require('hellosign-sdk')({ key: environment.hellosign.privateKey });
import * as ipfsClient from 'ipfs-http-client';
import * as Tx from 'ethereumjs-tx';
import { smartContract } from '../myProperties/smartContract';
import * as solc from 'solc';
import {Database} from '../Database';


const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: '5001', protocol: 'https' });

const sharedService = new SharedService();



export class Agreements {

  private web3:any;

	constructor() {
		try {
			this.web3 = new web3();
			this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));
		} catch (err) {
			
		}
  }
  

  async getArrayOfSignatures(signaturesRequestID:string[]){
    let arrayOfPromises = [];
    let arrayOfSignatures = [];
  
    for(let signatureRequestID of signaturesRequestID){
      arrayOfPromises.push(hellosign.signatureRequest.get(signatureRequestID));
    }
  
    const arrayOfResponses = await Promise.all(arrayOfPromises);
  
    for(let response of arrayOfResponses){
      for(let i in response.signature_request.signatures){
        delete response.signature_request.signatures[i].has_pin; delete response.signature_request.signatures[i].signer_name; 
        delete response.signature_request.signatures[i].signer_role;
        delete response.signature_request.signatures[i].order; 
        delete response.signature_request.signatures[i].last_viewed_at; delete response.signature_request.signatures[i].last_reminded_at;
      }
  
      arrayOfSignatures.push({
        signatureRequestID:response.signature_request.signature_request_id,
        signatures:response.signature_request.signatures
      });
    
    }
  
    return arrayOfSignatures;
  }

  public async getCurrencyPriceForEsign(uid:string, currency:string){
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForEsign/${uid}`).set({currency, price, name});
    return {currency, price};
  }
  

  payForEsignWithCrypto(uid:string, currency: string, agreementId: string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();

				let [balanceSnaphot, [lastPriceForEsignSnapshot, agreementSnapshot] ] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`lastPriceForEsign/${uid}`), db.doc(`agreements/${agreementId}`))
				]);

				const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();

				if (balanceSnaphot.empty) {
          reject('Insufficient funds'); return;
        }
				
				let balance = balanceSnaphot.docs[0].data();
				let lastPriceForEsign = lastPriceForEsignSnapshot.data();
				let agreement = agreementSnapshot.data();

				let amountToPay = new BigNumber(10).div(lastPriceForEsign.price.USD).decimalPlaces(8).toString(10);

				if (!(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))) {
          reject('Insufficient funds'); return;
        }
					
        if (agreement.realEstateAgent.uid !== uid) {
          reject('Invalid user'); return;
        }
						
        const id = sharedService.generateId();
        const txID = sharedService.generateId();
        const batch = db.batch();
						
        batch.update(db.doc(`balances/${balance.id}`), {
          available: new BigNumber(balance.available).minus(amountToPay).toString(10)
				});
				
				batch.update(db.doc(`totalUsersBalances/${currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
				});

        batch.set(db.doc(`agreements/${agreementId}/signaturesRequest/${id}`), {
          id,
          signatureRequestID: null,
          creationTS: Date.now()
        });

        batch.set(db.doc(`transactions/${txID}`), {
          currency,
          type: 'fees and charges',
          processed: true,
          status: 'confirmed',
          amount: amountToPay,
          details: `Fee charged for Esigned document`,
          uid,
          id: txID,
          creationTS: Date.now()
        });
						
        await batch.commit();
        resolve(id);



				
			} catch (err) {
				reject('Unexpected error sending payment');
			}
		});
  }
  


  async getSignUrl(signatureID:string){
		return (await hellosign.embedded.getSignUrl(signatureID)).embedded.sign_url;
  }


  uploadDocument(uid: string, agreementId: string, signatureRequestSelectedID: string, path: string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();

				let [agreement, _signatureRequest] = (await db.getAll(
					db.doc(`agreements/${agreementId}`), 
					db.doc(`agreements/${agreementId}/signaturesRequest/${signatureRequestSelectedID}`)
				)).map(doc=>doc.data());

        if (_signatureRequest.signatureRequestID) {
          reject('Document already uploaded'); return;
        }

        if (agreement.realEstateAgent.uid !== uid) {
          reject('Invalid user'); return;
        }
	
					
        const opts = {
          test_mode: environment.hellosign.test_mode,
          clientId: environment.hellosign.clientId,
          subject: 'eSigned document on RentPeacefully',
          // message: 'Please sign this NDA and then we can discuss more.',
          signers: [
            {
              email_address: agreement.realEstateAgent.email,
              name: `${sharedService.capitalize(agreement.realEstateAgent.firstName)} ${sharedService.capitalize(agreement.realEstateAgent.lastName)}`
            },
            {
              email_address: agreement.landlord.email,
              name: `${sharedService.capitalize(agreement.landlord.firstName)} ${sharedService.capitalize(agreement.landlord.lastName)}`
            }
          ],
          files: [path]
        };

						
        const signatureRequest = (await hellosign.signatureRequest.createEmbedded(opts)).signature_request;
        const signatureRequestID = signatureRequest.signature_request_id;
        const signatures = signatureRequest.signatures;
        for(let i in signatures){
          delete signatures[i].has_pin; delete signatures[i].signer_name; delete signatures[i].signer_role;
          delete signatures[i].order;	delete signatures[i].last_viewed_at; delete signatures[i].last_reminded_at;
        }
        const landlordSignatureID = signatures[0].signature_id;
        await db.doc(`agreements/${agreementId}/signaturesRequest/${signatureRequestSelectedID}`).update({	signatureRequestID });
        const signUrl = await this.getSignUrl(landlordSignatureID);
						
        resolve({signatureRequestID, signUrl, signatures});


				
			} catch (err) {
				reject('Unexpected error uploading document');
			}
		});
  }
  

  public estimateGasAndGasPriceDocSaving(uid: string, agreementId: string){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const [agreementSnapshot, identityWalletSnapshot] = await Promise.all([
					db.doc(`agreements/${agreementId}`).get(),
					db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()
				]);
				
				const identityWallet = identityWalletSnapshot.docs[0].data();
				const agreement = agreementSnapshot.data();


				let gasPrice =	this.web3.eth.gasPrice.toString(10);

				const myContractInstance = this.web3.eth.contract(environment.propertyContractABI).at(agreement.smartContract.address);

				const tenant = '0x0a55a3c6693727b92936f7ce609b2c55c46923dd'; //fake
				const ipfsHash = 'Qmb6Q5soQJjfuNxdSYUEFU6qPLPDZtECV3zPZs15fPPpHT'; //fake

				const estimateGas = myContractInstance.saveDocLocation.estimateGas(ipfsHash, tenant, {from: identityWallet.address, value:0});

				resolve({estimateGas, gasPrice});
				
			} catch (err) {
				reject('Unexpected error. Try again later');
			}

		});
  }
  
  getDocUrl(signatureRequestID:string){
		return new Promise((resolve, reject)=>{
			hellosign.signatureRequest.download(signatureRequestID, { file_type: 'pdf', get_url:true }, (err, res) => {
				if (!err) resolve(res.file_url);
				else reject(err.message || err);
			});
		});
  }
  

  uploadToIPFS(url:string) {
		return new Promise(async(resolve, reject)=>{
			ipfs.addFromURL(url, (err, result: {path:string, hash:string, size:number}[]) => {
				if (err) {
					reject(err.message || err); return;
				}
				const {hash} = result.find(data => data.path.endsWith('.pdf'));
				resolve(hash);

			});
		});
  }
  

  public saveDocOnBlockchain(uid: string, ipfsHash: string, agreementId: string, landlordId: string, signatureRequestSelectedID: string, gasPrice: number, gasLimit: number){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();

				const [agreementSnapshot, landlordIdentityWalletSnapshot, tenantIdentityWalletSnapshot] = await Promise.all([
					db.doc(`agreements/${agreementId}`).get(),
					db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get(),
					db.collection(`wallets/identity/private`).where('uid','==',landlordId).where('currency','==','ETH').get()
				]);
				
				const landlordIdentityWallet = landlordIdentityWalletSnapshot.docs[0].data();
				const tenantIdentityWallet = tenantIdentityWalletSnapshot.docs[0].data();
				const agreement = agreementSnapshot.data();

				const myContractInstance = this.web3.eth.contract(environment.propertyContractABI).at(agreement.smartContract.address);

				const rawTx = {
	        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(landlordIdentityWallet.address)),
	        gasPrice: this.web3.toHex(gasPrice),
	        gasLimit: this.web3.toHex(gasLimit),
					value: '0x00',
					to: agreement.smartContract.address,
	        data: myContractInstance.saveDocLocation.getData(ipfsHash, tenantIdentityWallet.address)
				 };
				 
				const txHash = await this.sendRawTransaction(landlordIdentityWallet.privateKey, rawTx);


				batch.set(db.doc(`transactions/${id}`), {
					id,
					txHash,
					uid,
					status:'pending',
					type:'contractExecution',
					action:'saveDocLocation',
					currency:'ETH',
					agreement:{
						id: agreementId,
						landlord: agreement.landlord.uid,
						signatureRequestSelectedID
					},
					creationTS: Date.now()
				});

				batch.update(db.doc(`agreements/${agreementId}/signaturesRequest/${signatureRequestSelectedID}`), {
					smartContract: {
						status: 'pending',
						txHash,
						ipfsHash,
						gasPrice,
						gasUsed: null
					}
				});
				await batch.commit();

				resolve(txHash);


			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
  }




	public estimateGasAndGasPriceContractCreation(uid:string):Promise<any>{
		return new Promise(async(resolve, reject)=>{		
			try{
				const db = admin.firestore();
				const identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()).docs[0].data();

				let gasPrice =	this.web3.eth.gasPrice.toString(10);

				const input = {
					language: 'Solidity',
					sources: {
						'PropertyContract.sol': {
							content: smartContract
						}
					},
					settings: {
						outputSelection: {
							'*': {
								'*': [ '*' ]
							}
						}
					}
				};

				const compiledContract = JSON.parse(solc.compile(JSON.stringify(input)));
				const byteCode = compiledContract.contracts['PropertyContract.sol']['PropertyContract'].evm.bytecode.object;
				const estimateGas =	this.web3.eth.estimateGas({
					gasPrice:	this.web3.toHex(gasPrice),
					from: identityWallet.address,
					value: '0x00',
					data: this.addPrefix(byteCode)
				});
					
		  	resolve({estimateGas, gasPrice});
			}catch(err){
				reject('Unexpected error. Try again later');
			}
		});
  }




  createSmartContract(uid: string, agreementId: string, gasPrice: number, gasLimit: number){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();
				const identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()).docs[0].data();

				const input = {
					language: 'Solidity',
					sources: {
						'PropertyContract.sol': {
							content: smartContract
						}
					},
					settings: {
						outputSelection: {
							'*': {
								'*': [ '*' ]
							}
						}
					}
				};

				const compiledContract = JSON.parse(solc.compile(JSON.stringify(input)));
				const byteCode = compiledContract.contracts['PropertyContract.sol']['PropertyContract'].evm.bytecode.object;

				const rawTx = {
	        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(identityWallet.address)),
	        gasPrice: this.web3.toHex(gasPrice),
	        gasLimit: this.web3.toHex(gasLimit),
	        value: '0x00',
	        data: this.addPrefix(byteCode)
		   	};

				let txHash = (await this.sendRawTransaction(identityWallet.privateKey, rawTx)).toLowerCase();

				batch.set(db.doc(`transactions/${id}`), {
					id,
					txHash,
					uid,
					status:'pending',
					type:'contractExecution',
					action:'contractCreation',
					currency:'ETH',
					agreement:{
						id: agreementId
					},
					creationTS: Date.now()
				});

				batch.update(db.doc(`agreements/${agreementId}`), {
					smartContract: {
						status: 'pending',
						txHash,
						address: null,
						gasPrice,
						gasUsed: null
					}
				});
				await batch.commit();

				resolve(txHash);
							
					              
			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}
	



	public endLease(uid: string, agreementId: string){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let agreement = (await db.doc(`agreements/${agreementId}`).get()).data();

				if(agreement.realEstateAgent.uid !== uid && agreement.landlord.uid !== uid){
					reject("You're not an user of this Agreement"); return;
				}

				if(agreement.realEstateAgent.uid === uid && agreement.listingID) {
					reject('You need to delete or set as rented the listing before ending the lease'); return;
				}

				if(agreement.landlord.uid === uid && agreement.listingID) {
					reject('Ask your Agent to delete or set as rented the listing before ending the lease'); return;
				}

				if(agreement.status === 'active') {
					batch.update(db.doc(`agreements/${agreementId}`), {
						status:'endingLease',
						requestedBy: uid
					});
					await batch.commit();
					resolve();
					return;
				}

				if(agreement.status !== 'endingLease' || agreement.requestedBy === uid){
					reject("You can't end the lease"); return;
				}

				let [realEstateAgentInfo, landlordInfo] = (await db.getAll(
					db.doc(`users/${agreement.realEstateAgent.uid}`), 
					db.doc(`users/${agreement.landlord.uid}`), 
				)).map(doc=>doc.data());

				let [signaturesRequests] = (await Promise.all([
					db.collection(`agreements/${agreementId}/signaturesRequest`).get()
				])).map(snapshot=>snapshot.docs.map(doc=>doc.data()));

				batch.delete(db.doc(`agreements/${agreementId}`));
				
				for(let signaturesRequest of signaturesRequests){
					batch.delete(db.doc(`agreements/${agreementId}/signaturesRequest/${signaturesRequest.id}`))
				}

				let realEstateAgentObj = {
					'realEstateAgent.totalLandlords': realEstateAgentInfo.realEstateAgent.totalLandlords + 1
				}

				batch.update(db.doc(`users/${realEstateAgentInfo.uid}`), realEstateAgentObj);

				// batch.update(db.doc(`public/${realEstateAgentInfo.uid}`), realEstateAgentObj);


				const id1 = sharedService.generateId();
				const id2 = sharedService.generateId();

				batch.set(db.doc(`users/${landlordInfo.uid}/notifications/${id1}`), {
					title: `Lease Ended`,
					content: `Lease Ended for: ${sharedService.capitalize(realEstateAgentInfo.firstName)} ${sharedService.capitalize(realEstateAgentInfo.lastName)}`,
					id: id1,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${landlordInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});

				batch.set(db.doc(`users/${realEstateAgentInfo.uid}/notifications/${id2}`), {
					title: `Lease Ended`,
					content: `Lease Ended for: ${sharedService.capitalize(landlordInfo.firstName)} ${sharedService.capitalize(landlordInfo.lastName)}`,
					id: id1,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${realEstateAgentInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});


				if(agreement.isPropertyRented) {
					const id3 = sharedService.generateId();
					const id4 = sharedService.generateId();
				
					batch.set(db.doc(`users/${landlordInfo.uid}/notifications/${id3}`), {
						title: `Provide feedback`,
						content: `Provide feedback for ${sharedService.capitalize(realEstateAgentInfo.firstName)} as Real Estate Agent`,
						hasButton: true,
						buttonText: 'Click here',
						action: 'provide agreement real estate agent feedback',
						feedbackFor: realEstateAgentInfo.uid,
						id: id3,
						creationTS: Date.now(),
					});

					batch.update(db.doc(`users/${landlordInfo.uid}/settings/preferences`), {
						'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
					});

					batch.set(db.doc(`users/${realEstateAgentInfo.uid}/notifications/${id4}`), {
						title: `Provide feedback`,
						content: `Provide feedback for ${sharedService.capitalize(landlordInfo.firstName)} as Landlord`,
						hasButton: true,
						buttonText: 'Click here',
						action: 'provide agreement landlord feedback',
						feedbackFor: landlordInfo.uid,
						id: id4,
						creationTS: Date.now(),
					});

					batch.update(db.doc(`users/${realEstateAgentInfo.uid}/settings/preferences`), {
						'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
					});
				
				}
				const request = Database.pool.request();
				const SQLcommand = `
				UPDATE realEstateAgents SET 
				[totalLandlords] = ${realEstateAgentObj['realEstateAgent.totalLandlords']} 
				WHERE uid = '${uid}'`;
				await request.query(SQLcommand);


				await batch.commit();
				resolve();


			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}


	public cancelCulmination(uid: string, agreementId: string){
		return new Promise(async(resolve, reject)=>{
			try{
				const db = admin.firestore();
				const batch = db.batch();

				const agreement = (await db.doc(`agreements/${agreementId}`).get()).data();

				if(agreement.status !== 'endingLease' || agreement.requestedBy !== uid) {
					reject('Uknown action'); return;
				}

				batch.update(db.doc(`agreements/${agreementId}`), {
					status:'active',
					requestedBy:admin.firestore.FieldValue.delete()
				});

				await batch.commit();
				resolve();

			}catch(err){
				reject('Unexpected error canceling request');
			}
		});
	}




	startChat(uid: string, content: string, agreementId: string) {
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const chatID = sharedService.generateId();
				const messageID = sharedService.generateId();

				const agreement = (await db.doc(`agreements/${agreementId}`).get()).data();


				batch.update(db.doc(`agreements/${agreementId}`), { chatID });

				batch.set(db.doc(`chats/${chatID}`), {
					chatName: `(${sharedService.capitalize(agreement.landlord.firstName)} ${agreement.landlord.lastName[0].toUpperCase()}) Property Agreement`,
					id: chatID,
					lastMessage: {
						content,
						uid,
						creationTS: Date.now()
					},
					uids: [agreement.landlord.uid, agreement.realEstateAgent.uid]
				});

				batch.set(db.doc(`chats/${chatID}/messages/${messageID}`), {
					content,
					uid,
					creationTS: Date.now(),
					id: messageID
				});

				await batch.commit();
				resolve({chatID, messageID});


			} catch (err) {
				reject('Unexpected error');
			}
		});
	}

	
  


  private addPrefix(data:string){
		return data.indexOf('0x')==0? data:`0x${data}`;
	}
  

  

  private sendRawTransaction(privateKey:string, rawTx:any): Promise<string>{
		return new Promise(async(resolve, reject)=>{
			
			const tx = new Tx(rawTx);
			tx.sign(Buffer.from(privateKey, 'hex')); //Sign transaction
			let serializedTx = `0x${tx.serialize().toString('hex')}`;
			this.web3.eth.sendRawTransaction(serializedTx, (err, hash)=> {
				if(err){
					reject(err.message || err);
				} else{
					resolve(hash);
				}               
			});
		});
	}
  



}
