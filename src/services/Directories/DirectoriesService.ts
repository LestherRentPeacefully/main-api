import * as admin from "firebase-admin";
const sql = require('mssql');
import {Database} from '../Database';
import {serviceProviderSkills} from '../User/Skills';



export class DirectoriesService{
  
  async getServiceProviders(filter:any, offset:number){
    if(filter.skills){
      for(let skill of filter.skills){
				filter[skill] = 1;
			}
      delete filter.skills;
    }

    const db = admin.firestore();

    let conditionString = 'distance < @radius AND status = @status';
    let columsString = 'serviceProviders.uid, serviceProviders.score, serviceProviders.status, serviceProviders.reviews';

    let request = Database.pool.request()
      .input('lat', sql.Float, filter.lat)
      .input('lng', sql.Float, filter.lng)
      .input('radius', sql.Float, filter.radius)
      .input('status', sql.NVarChar(50), 'verified');
    
    for(let key of Object.keys(filter)){
      if(filter[key]){
         if(key!='radius' && key!='lat' && key!='lng'){
          request = request.input(key, filter[key]);
          conditionString += ` AND ${key} = @${key}`;
          columsString += `, serviceProviders.${key}`;
        }
      }
    }

    let SQLcommand = `
      SELECT uid, MAX(reviews) as reviews FROM (
        SELECT uid, score, reviews FROM (
          SELECT ${columsString},
          ( 6371 * acos( cos(radians(@lat))*cos(radians(areasServing.lat))*cos(radians(areasServing.lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(areasServing.lat)) ) )
          AS distance FROM areasServing 
          JOIN serviceProviders ON areasServing.uid = serviceProviders.uid 
        ) AS tb1 
        WHERE ${conditionString}
      ) AS tb2
      GROUP BY uid
      ORDER BY reviews DESC`; // OFFSET 0 ROWS FETCH NEXT 9 ROWS ONLY

    let usersRefs: FirebaseFirestore.DocumentReference[] = [];
    let serviceProvidersRefs: FirebaseFirestore.DocumentReference[] = [];
    let editableRefs: FirebaseFirestore.DocumentReference[] = [];

    let usersInfo = [];

    let serviceProvidersRecordSet:any[] = (await request.query(SQLcommand)).recordset;

    if(serviceProvidersRecordSet.length>0){
      for(let serviceProvider of serviceProvidersRecordSet){
        usersRefs.push(db.doc(`users/${serviceProvider.uid}`));
        serviceProvidersRefs.push(db.doc(`users/${serviceProvider.uid}/profile/serviceProvider`));
        editableRefs.push(db.doc(`users/${serviceProvider.uid}/profile/editable`));
      }

      let [usersSnapshot, serviceProvidersSnapshot, editableSnapshot] = await Promise.all([
        db.getAll(...usersRefs), 
        db.getAll(...serviceProvidersRefs),
        db.getAll(...editableRefs)
      ]);
      
      for(let i = 0; i<usersSnapshot.length; i++){
        let userData = usersSnapshot[i].data();
        let serviceProviderData = serviceProvidersSnapshot[i].data();
        let editableData = editableSnapshot[i].data();
  
        usersInfo.push({
          firstName:userData.firstName,
          lastName:userData.lastName,
          membership:userData.membership,
          photo:userData.photo,
          serviceProvider:userData.serviceProvider,
          uid:userData.uid,
          skills:serviceProviderData.skills,
          shortDescription:editableData.shortDescription
        });
      }

    }

    return usersInfo;
  }



  async getRealEstateAgents(filter:any, offset:number){

    const db = admin.firestore();

    let conditionString = `distance < ${filter.radius} AND status = 'verified'`;
    let columsString = 'realEstateAgents.uid, realEstateAgents.score, realEstateAgents.status, realEstateAgents.reviews, realEstateAgents.totalRented';

    let request = Database.pool.request()
      .input('lat', sql.Float, filter.lat)
      .input('lng', sql.Float, filter.lng);
    

    let SQLcommand = `
      SELECT uid, MAX(totalRented) as totalRented FROM (
        SELECT uid, score, reviews, totalRented FROM (
          SELECT ${columsString},
          ( 6371 * acos( cos(radians(@lat))*cos(radians(areasServing.lat))*cos(radians(areasServing.lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(areasServing.lat)) ) )
          AS distance FROM areasServing 
          JOIN realEstateAgents ON areasServing.uid = realEstateAgents.uid 
        ) AS tb1 
        WHERE ${conditionString}
      ) AS tb2
      GROUP BY uid
      ORDER BY totalRented DESC`; // OFFSET 0 ROWS FETCH NEXT 9 ROWS ONLY

    let usersRefs: FirebaseFirestore.DocumentReference[] = [];
    let editableRefs: FirebaseFirestore.DocumentReference[] = [];

    let usersInfo = [];

    let realEstateAgentsRecordSet:any[] = (await request.query(SQLcommand)).recordset;

    if(realEstateAgentsRecordSet.length>0){
      for(let realEstateAgent of realEstateAgentsRecordSet){
        usersRefs.push(db.doc(`users/${realEstateAgent.uid}`));
        editableRefs.push(db.doc(`users/${realEstateAgent.uid}/profile/editable`));
      }

      let [usersSnapshot, editableSnapshot] = await Promise.all([
        db.getAll(...usersRefs), 
        db.getAll(...editableRefs)
      ]);
      
      for(let i = 0; i<usersSnapshot.length; i++){
        let userData = usersSnapshot[i].data();
        let editableData = editableSnapshot[i].data();
  
        usersInfo.push({
          firstName: userData.firstName,
          lastName: userData.lastName,
          membership: userData.membership,
          photo: userData.photo,
          realEstateAgent: userData.realEstateAgent,
          uid: userData.uid,
          shortDescription: editableData.shortDescription,
          totalRented: userData.realEstateAgent.totalRented,
          totalTenants: userData.realEstateAgent.totalTenants,
          totalLandlords: userData.realEstateAgent.totalLandlords
        });
      }

    }

    return usersInfo;
  }



  async getTotalServiceProviders(filter:any){
    if(filter.skills){
      for(let skill of filter.skills){
				filter[skill] = 1;
			}
      delete filter.skills;
    }


    let conditionString = 'distance < @radius AND status = @status';
    let columsString = 'serviceProviders.uid, serviceProviders.score, serviceProviders.status, serviceProviders.reviews';

    let request = Database.pool.request()
      .input('lat', sql.Float, filter.lat)
      .input('lng', sql.Float, filter.lng)
      .input('radius', sql.Float, filter.radius)
      .input('status', sql.NVarChar(50), 'verified');
    
    for(let key of Object.keys(filter)){
      if(filter[key]){
         if(key!='radius' && key!='lat' && key!='lng'){
          request = request.input(key, filter[key]);
          conditionString += ` AND ${key} = @${key}`;
          columsString += `, serviceProviders.${key}`;
        }
      }
    }

    let SQLcommand = `
      SELECT COUNT(*) as total FROM(
        SELECT uid, MAX(reviews) as reviews FROM (
          SELECT uid, score, reviews FROM (
            SELECT ${columsString},
            ( 6371 * acos( cos(radians(@lat))*cos(radians(areasServing.lat))*cos(radians(areasServing.lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(areasServing.lat)) ) )
            AS distance FROM areasServing 
            JOIN serviceProviders ON areasServing.uid = serviceProviders.uid 
          ) AS tb1 
          WHERE ${conditionString}
        ) AS tb2
        GROUP BY uid
      ) AS tb3`;

    let total = (await request.query(SQLcommand)).recordset[0].total;
    return total;
  }




  async getTotalRealEstateAgents(filter:any){

    const conditionString = `distance < ${filter.radius} AND status = 'verified'`;
    const columsString = 'realEstateAgents.uid, realEstateAgents.score, realEstateAgents.status, realEstateAgents.reviews, realEstateAgents.totalRented';

    const request = Database.pool.request()
      .input('lat', sql.Float, filter.lat)
      .input('lng', sql.Float, filter.lng);

      const SQLcommand = `
      SELECT COUNT(*) as total FROM(
        SELECT uid, MAX(totalRented) as totalRented FROM (
          SELECT uid, score, reviews, totalRented FROM (
            SELECT ${columsString},
            ( 6371 * acos( cos(radians(@lat))*cos(radians(areasServing.lat))*cos(radians(areasServing.lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(areasServing.lat)) ) )
            AS distance FROM areasServing 
            JOIN realEstateAgents ON areasServing.uid = realEstateAgents.uid 
          ) AS tb1 
          WHERE ${conditionString}
        ) AS tb2
        GROUP BY uid
      ) AS tb3`;

    const total = (await request.query(SQLcommand)).recordset[0].total;
    return total;
  }



  async getTotalAndServiceProviders(filter:any, offset:number){
    let [serviceProviders, total] = await Promise.all([this.getServiceProviders(filter, offset), this.getTotalServiceProviders(filter)]);
    return {serviceProviders, total};
  }


  async getTotalAndRealEstateAgents(filter:any, offset:number){
    let [realEstateAgents, total] = await Promise.all([this.getRealEstateAgents(filter, offset), this.getTotalRealEstateAgents(filter)]);
    return {realEstateAgents, total};
  }




}