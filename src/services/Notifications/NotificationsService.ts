import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import { SharedService } from '../Shared/SharedService';
import {Database} from '../Database';

const shared = new SharedService();


export class NotificationsService {


  provideFeedback(uid: string, notificationID: string, feedback: any){
    return new Promise(async(resolve, reject)=>{
      try {
        
        const db = admin.firestore();
        const batch = db.batch();
        
        let notification = (await db.doc(`users/${uid}/notifications/${notificationID}`).get()).data();

        let userForFeedback = (await db.doc(`users/${notification.feedbackFor}`).get()).data();

        const id = shared.generateId();

        let feedbackObj;


        if(notification.action == 'provide property tenant feedback'){

          if(!(feedback.score>0) || !(feedback.score<=5) || !(feedback.ontimePayments>0) || !(feedback.ontimePayments<=5)){
            reject('Invalid feedback'); return;
          }

          feedbackObj = {
						'tenant.score':new BigNumber( ((userForFeedback.tenant.score*userForFeedback.tenant.reviews) + feedback.score)/(userForFeedback.tenant.reviews+1)).decimalPlaces(1).toNumber(),
						'tenant.reviews':userForFeedback.tenant.reviews + 1,
						'tenant.ontimePayments':new BigNumber(((userForFeedback.tenant.ontimePayments*userForFeedback.tenant.reviews) + feedback.ontimePayments)/(userForFeedback.tenant.reviews+1)).decimalPlaces(1).toNumber(),
          }

          batch.set(db.doc(`users/${userForFeedback.uid}/reviews/${id}`), {
            role: 'tenant',
            reason: 'myProperty',
						feedback: {
							score: feedback.score,
							ontimePayments: feedback.ontimePayments,
							review: feedback.review || ''
						},
						providedBy: {
							uid,
							role: 'landlord'
						},
						id,
						creationTS: Date.now()
					});

        


        } else if(notification.action == 'provide property landlord feedback'){

          if(!(feedback.score>0) || !(feedback.score<=5) || !(feedback.avgTimeOfTicketResolution>0) || !(feedback.avgTimeOfTicketResolution<=5)){
            reject('Invalid feedback'); return;
          }

          feedbackObj = {
						'landlord.score':new BigNumber( ((userForFeedback.landlord.score*userForFeedback.landlord.reviews) + feedback.score)/(userForFeedback.landlord.reviews+1) ).decimalPlaces(1).toNumber(),
						'landlord.reviews':userForFeedback.landlord.reviews+1,
						'landlord.avgTimeOfTicketResolution':new BigNumber( ((userForFeedback.landlord.avgTimeOfTicketResolution*userForFeedback.landlord.reviews) + feedback.avgTimeOfTicketResolution)/(userForFeedback.landlord.reviews+1)).decimalPlaces(1).toNumber(),
          }
          
          batch.set(db.doc(`users/${userForFeedback.uid}/reviews/${id}`), {
            role: 'landlord',
            reason: 'myProperty',
						feedback: {
							score: feedback.score,
							avgTimeOfTicketResolution: feedback.avgTimeOfTicketResolution,
							review: feedback.review || ''
						},
						providedBy: {
							uid,
							role: 'tenant'
						},
						id,
						creationTS: Date.now()
          }); 
        

          
        } else if(notification.action == 'provide agreement real estate agent feedback'){

          if(!(feedback.score>0) || !(feedback.score<=5)){
            reject('Invalid feedback'); return;
          }

          feedbackObj = {
						'realEstateAgent.score':new BigNumber( ((userForFeedback.realEstateAgent.score*userForFeedback.realEstateAgent.reviews) + feedback.score)/(userForFeedback.realEstateAgent.reviews+1) ).decimalPlaces(1).toNumber(),
						'realEstateAgent.reviews':userForFeedback.realEstateAgent.reviews+1
          }

          batch.set(db.doc(`users/${userForFeedback.uid}/reviews/${id}`), {
            role: 'real estate agent',
            reason: 'agreement',
						feedback: {
							score: feedback.score,
							review: feedback.review || ''
						},
						providedBy: {
							uid,
							role: 'landlord'
						},
						id,
						creationTS: Date.now()
          });

          const request = Database.pool.request();
          const SQLcommand = `
          UPDATE realEstateAgents SET 
          [score] = ${feedbackObj['realEstateAgent.score']},
          [reviews] = ${feedbackObj['realEstateAgent.reviews']} 
          WHERE uid = '${uid}'`;
          await request.query(SQLcommand);

          



        } else if(notification.action == 'provide agreement landlord feedback'){
          if(!(feedback.score>0) || !(feedback.score<=5)){
            reject('Invalid feedback'); return;
          }

          feedbackObj = {
						'landlord.score':new BigNumber( ((userForFeedback.landlord.score*userForFeedback.landlord.reviews) + feedback.score)/(userForFeedback.landlord.reviews+1) ).decimalPlaces(1).toNumber(),
						'landlord.reviews':userForFeedback.landlord.reviews+1
          }

          batch.set(db.doc(`users/${userForFeedback.uid}/reviews/${id}`), {
            role: 'landlord',
            reason: 'agreement',
						feedback: {
							score: feedback.score,
							review: feedback.review || ''
						},
						providedBy: {
							uid,
							role: 'real estate agent'
						},
						id,
						creationTS: Date.now()
          }); 


        }
        
        
        
        else{
          reject('Invalid action'); return;
        }

        batch.update(db.doc(`users/${userForFeedback.uid}`), feedbackObj);

        // batch.update(db.doc(`public/${userForFeedback.uid}`), feedbackObj);

        batch.update(db.doc(`users/${uid}/notifications/${notificationID}`), {
          hasButton: admin.firestore.FieldValue.delete(),
          buttonText: admin.firestore.FieldValue.delete(),
          action: admin.firestore.FieldValue.delete(),
          feedbackFor: admin.firestore.FieldValue.delete(),
        });
        
        await batch.commit();
        resolve();


      } catch (err) {
        reject('Unexpected error providing feedback');
      }
      
    });
  }


  confirmPropertyListing(uid:string, notificationID:string){
    return new Promise(async(resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();

        let [notification, landlord] = (await db.getAll(
        db.doc(`users/${uid}/notifications/${notificationID}`), 
        db.doc(`users/${uid}`))).map(doc=>doc.data());

        let [listing, realEstateAgent] = (await db.getAll(
          db.doc(`listings/${notification.listingID}`),
          db.doc(`users/${notification.realEstateAgentID}`)
        )).map(doc=>doc.data());

        if(!landlord.status.IDVerified) {
          reject('Please, verify your identity'); return;
        }


        if(!listing) {
          batch.update(db.doc(`users/${uid}/notifications/${notificationID}`), {
            hasButton: admin.firestore.FieldValue.delete(),
            buttonText: admin.firestore.FieldValue.delete(),
            action: admin.firestore.FieldValue.delete(),
            listingID: admin.firestore.FieldValue.delete(),
          });

          await batch.commit();

          reject("Listing no longer exists"); return;
        }

        if(notification.action != 'confirm property listing'){
          reject('Invalid action'); return;
        }

        
        const notifId = shared.generateId();
        const agreementId = shared.generateId();

        batch.update(db.doc(`listings/${notification.listingID}`), {
          status: 'pending',
          agreementId
        });

        batch.update(db.doc(`users/${uid}/notifications/${notificationID}`), {
          hasButton: admin.firestore.FieldValue.delete(),
          buttonText: admin.firestore.FieldValue.delete(),
          action: admin.firestore.FieldValue.delete(),
          listingID: admin.firestore.FieldValue.delete(),
        });

        batch.set(db.doc(`users/${notification.realEstateAgentID}/notifications/${notifId}`), {
          title: `Property listing`,
          content: `${shared.capitalize(landlord.firstName)} ${shared.capitalize(landlord.lastName)} has confirmed the property listing. It'll be visible once RentPeacefully team verify it`,
          creationTS: Date.now(),
          notifId
        });

        batch.update(db.doc(`users/${notification.realEstateAgentID}/settings/preferences`), {
          'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
        });

        batch.set(db.doc(`agreements/${agreementId}`), {
          listingID: notification.listingID,
          isPropertyRented: false,
          creationTS: Date.now(),
          status: 'active',
          id: agreementId,
          realEstateAgent: {
            firstName: realEstateAgent.firstName,
            lastName: realEstateAgent.lastName,
            uid: realEstateAgent.uid,
            email: realEstateAgent.email
          },
          landlord: {
            firstName: landlord.firstName,
            lastName: landlord.lastName,
            uid: landlord.uid,
            email: landlord.email
          }
        });

        await batch.commit();
        resolve();


      } catch (err) {
        reject('Unexpected error providing feedback');
      }
    });
  }



}