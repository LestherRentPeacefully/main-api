

let smartContract = 
  `
    pragma solidity ^0.5.11;
    pragma experimental ABIEncoderV2;



    contract owned { //Contract used to only allow the owner to call some functions
      address public owner;

      constructor() public {
        owner = msg.sender;
      }

      modifier onlyOwner {
        require(msg.sender == owner);
        _;
      }

      function transferOwnership(address newOwner) onlyOwner public {
        owner = newOwner;
      }
    }


    contract PropertyContract is owned {

      event DocSaved(string ipfsHash, address tenant, uint256 timestamp);

      mapping (address => string[]) public docs;

      function saveDocLocation(string memory ipfsHash, address tenant) onlyOwner public {
        docs[tenant].push(ipfsHash);
        emit DocSaved(ipfsHash, tenant, now);
      }

      function getDocs(address tenant) public view returns(string[] memory) {
        return docs[tenant];
      }

    }
  `;

export {smartContract}
