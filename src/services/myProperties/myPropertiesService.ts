import * as admin from "firebase-admin";
const sql = require('mssql');
import {Database} from '../Database';
import {environment} from '../../environments/environment';
import {BigNumber} from 'bignumber.js';
import {SharedService} from '../Shared/SharedService';
import * as web3 from 'web3';
import * as Tx from 'ethereumjs-tx';
import * as solc from 'solc';
import {smartContract} from './smartContract';
import * as ipfsClient from 'ipfs-http-client';

const ipfs = ipfsClient({ host: 'ipfs.infura.io', port: '5001', protocol: 'https' });

const hellosign = require('hellosign-sdk')({ key: environment.hellosign.privateKey });

const sharedService = new SharedService();

export class myPropertiesService{

	private web3:any;

	constructor() {
		try {
			this.web3 = new web3();
			this.web3.setProvider(new web3.providers.HttpProvider(environment.blockchain.ETH.nodeURL));
		} catch (err) {
			
		}
	}
	


	public new(fields:any, uid:string):Promise<any>{
		return new Promise(async (resolve,reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let userInfo = (await db.doc(`users/${uid}`).get()).data();	

				const myPropertyID = sharedService.generateId();
				fields.location.geopoint = new admin.firestore.GeoPoint(fields.location.lat, fields.location.lng);
				delete fields.location.lat, delete fields.location.lng;

				batch.set(db.doc(`myProperties/${myPropertyID}`), {
					user:{
						firstName:userInfo.firstName,
						lastName:userInfo.lastName,
						email:userInfo.email,
						uid:userInfo.uid,
						role:'landlord',
					},
					name:fields.name,
					address:fields.address,
					location:fields.location,
					unit:fields.unit || '',
					type:fields.type,
					// typeOfContract:fields.typeOfContract,
					photos:[fields.photos[0]],
					id:myPropertyID,	
					creationTS:Date.now(),
					tenantsID:[],
					listingID:null
				});


				await batch.commit();
				resolve(myPropertyID);
				
			} catch (err) {
				reject('Unexpected error creating your property');
			}
		});
	}



	public edit(uid:string, fields:any, myPropertyID:string, oldFiles:string[]){
		return new Promise(async (resolve,reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let myProperty = (await db.doc(`myProperties/${myPropertyID}`).get()).data();

				fields.location.geopoint = new admin.firestore.GeoPoint(fields.location.lat, fields.location.lng);
				delete fields.location.lat, delete fields.location.lng;

				if(myProperty.user.uid != uid){
					reject("You're not the landlord of this property"); return;
				}
				

				batch.update(db.doc(`myProperties/${myPropertyID}`), {
					name:fields.name,
					address:fields.address,
					location:fields.location,
					unit:fields.unit || '',
					type:fields.type,
					// typeOfContract:fields.typeOfContract,
					photos:[fields.photos[0]],
				});

				await batch.commit();
				if(oldFiles && oldFiles.length>0) sharedService.deleteFiles(oldFiles).catch(()=>'');
				resolve();

			} catch (err) {
				reject('Unexpected error editing your property');
			}
		});
	}


	
	public delete(uid:string, myPropertyID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let myProperty = (await db.doc(`myProperties/${myPropertyID}`).get()).data();

				if(myProperty.user.uid != uid){
					reject("You're not the landlord of this property"); return;
				}
				if(myProperty.tenantsID.length>0){
					reject('You need to end lease with every tenant to delete the Property'); return;
				}
				if(!myProperty.createdByRealEstateAgent && myProperty.listingID){
					reject('You need to delete or set as rented the listing before deleting the property'); return;
				}

				batch.delete(db.doc(`myProperties/${myPropertyID}`));
				
				await batch.commit();
				sharedService.deleteFiles(myProperty.photos).catch(()=>'');
				resolve();


			} catch (err) {
				reject('Unexpected error deleting your property');
			}
		});
	}


	public getTenants(tenantsID:string[]){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				let docRefs:FirebaseFirestore.DocumentReference[] = [];
				
				let tenantsInfo = [];

				if(tenantsID.length>0){
					
						for(let id of tenantsID){
							docRefs.push(db.doc(`users/${id}`));
					}
					
					let tenantsDocs = await db.getAll(...docRefs);
					
					
					for(let tenantsDoc of tenantsDocs){
						let tenantInfo = tenantsDoc.data();
						delete tenantInfo.landlord, delete tenantInfo.realEstateAgent, delete tenantInfo.serviceProvider, delete tenantInfo.status;
						tenantsInfo.push(tenantInfo);
					}
				}

				resolve(tenantsInfo);

			} catch (err) {
				reject('Unexpected error getting Tenants');
			}
		});
	}


	async getArrayOfSignatures(signaturesRequestID:string[]){
		let arrayOfPromises = [];
		let arrayOfSignatures = [];

		for(let signatureRequestID of signaturesRequestID){
			arrayOfPromises.push(hellosign.signatureRequest.get(signatureRequestID));
		}

		const arrayOfResponses = await Promise.all(arrayOfPromises);

		for(let response of arrayOfResponses){
			for(let i in response.signature_request.signatures){
				delete response.signature_request.signatures[i].has_pin; delete response.signature_request.signatures[i].signer_name; 
				delete response.signature_request.signatures[i].signer_role;
				delete response.signature_request.signatures[i].order; 
				delete response.signature_request.signatures[i].last_viewed_at; delete response.signature_request.signatures[i].last_reminded_at;
			}

			arrayOfSignatures.push({
				signatureRequestID:response.signature_request.signature_request_id,
				signatures:response.signature_request.signatures
			});
		
		}

		return arrayOfSignatures;
	}


	public async getCurrencyPriceForEsign(uid:string, currency:string){
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForEsign/${uid}`).set({currency, price, name});
    return {currency, price};
	}
	



	payForEsignWithCrypto(uid:string, currency:string, myPropertyID:string, tenantID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();

				let [balanceSnaphot, [lastPriceForEsignSnapshot, myPropertySnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`lastPriceForEsign/${uid}`), db.doc(`myProperties/${myPropertyID}`))
				]);

				const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();

				if (balanceSnaphot.empty) {
          reject('Insufficient funds'); return;
        }
				
				let balance = balanceSnaphot.docs[0].data();
				let lastPriceForEsign = lastPriceForEsignSnapshot.data();
				let myProperty = myPropertySnapshot.data();

				let amountToPay = new BigNumber(10).div(lastPriceForEsign.price.USD).decimalPlaces(8).toString(10);

				if (!(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))) {
					reject('Insufficient funds'); return;
				}
					
				if (myProperty.user.uid !==uid) {
					reject('Invalid user'); return;
				}
					
				const id = sharedService.generateId();
				const txID = sharedService.generateId();
				const batch = db.batch();
				
				batch.update(db.doc(`balances/${balance.id}`), {
					available: new BigNumber(balance.available).minus(amountToPay).toString(10)
				});

				batch.update(db.doc(`totalUsersBalances/${currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
				});

				batch.set(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest/${id}`), {
					id,
					signatureRequestID: null,
					creationTS: Date.now()
				});

				batch.set(db.doc(`transactions/${txID}`), {
					currency,
					type: 'fees and charges',
					processed: true,
					status: 'confirmed',
					amount: amountToPay,
					details: `Fee charged for Esigned document`,
					uid,
					id: txID,
					creationTS: Date.now()
				});
				
				await batch.commit();
				resolve(id);



			} catch (err) {
				reject('Unexpected error sending payment');
			}
		});
	}


	uploadDocument(uid:string, myPropertyID:string, signatureRequestSelectedID:string, tenantID:string, path:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();

				let [myProperty, tenant, _signatureRequest] = (await db.getAll(
					db.doc(`myProperties/${myPropertyID}`), 
					db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`),
					db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest/${signatureRequestSelectedID}`)
				)).map(doc=>doc.data());

	
				if (!_signatureRequest.signatureRequestID) {
					if (myProperty.user.uid==uid) {
					
						const opts = {
							test_mode: environment.hellosign.test_mode,
							clientId: environment.hellosign.clientId,
							subject: 'eSigned document on RentPeacefully',
							// message: 'Please sign this NDA and then we can discuss more.',
							signers: [
								{
									email_address: myProperty.user.email,
									name: `${sharedService.capitalize(myProperty.user.firstName)} ${sharedService.capitalize(myProperty.user.lastName)}`
								},
								{
									email_address: tenant.email,
									name: `${sharedService.capitalize(tenant.firstName)} ${sharedService.capitalize(tenant.lastName)}`
								}
							],
							files: [path]
						};

						
						const signatureRequest = (await hellosign.signatureRequest.createEmbedded(opts)).signature_request;
						const signatureRequestID = signatureRequest.signature_request_id;
						const signatures = signatureRequest.signatures;
						for(let i in signatures){
							delete signatures[i].has_pin; delete signatures[i].signer_name; delete signatures[i].signer_role;
							delete signatures[i].order;	delete signatures[i].last_viewed_at; delete signatures[i].last_reminded_at;
						}
						const landlordSignatureID = signatures[0].signature_id;
						await db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest/${signatureRequestSelectedID}`).update({	signatureRequestID });
						const signUrl = await this.getSignUrl(landlordSignatureID);
						
						resolve({signatureRequestID, signUrl, signatures});

					} else {
						reject('Invalid user');
					}

				} else {
					reject('Document already uploaded');
				}
				
			} catch (err) {
				reject('Unexpected error uploading document');
			}
		});
	}


	async getSignUrl(signatureID:string){
		return (await hellosign.embedded.getSignUrl(signatureID)).embedded.sign_url;
	}


	getDocUrl(signatureRequestID:string){
		return new Promise((resolve, reject)=>{
			hellosign.signatureRequest.download(signatureRequestID, { file_type: 'pdf', get_url:true }, (err, res) => {
				if (!err) resolve(res.file_url);
				else reject(err.message || err);
			});
		});
	}


	public depositWithCrypto(uid:string, myPropertyID:string, currency:string, amount:number){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let [myProperty, currencyInfo, tenant] = (await db.getAll(
					db.doc(`myProperties/${myPropertyID}`), 
					db.doc(`currencies/${currency}`),
					db.doc(`myProperties/${myPropertyID}/tenants/${uid}`)
				)).map(doc=>doc.data());

				let [landlordBalanceSnapshot, tenantBalanceSnapshot] = await Promise.all([
          db.collection(`balances`).where('uid','==',myProperty.user.uid).where('currency','==',currency).limit(1).get(),
          db.collection(`balances`).where('uid','==',tenant.uid).where('currency','==',currency).limit(1).get()
				]);
				

				if (landlordBalanceSnapshot.empty) {
					reject("Landlord doesn't have a wallet for this currency. Ask him/her to create one"); return;
				}

				let [landlordBalance, tenantBalance] = [landlordBalanceSnapshot.docs[0].data(), tenantBalanceSnapshot.docs[0].data()];

				const amountToPay = new BigNumber(amount).decimalPlaces(8).toString(10);

				if(tenantBalanceSnapshot.empty || !(new BigNumber(tenantBalance.available).isGreaterThanOrEqualTo(amountToPay))){
					reject('Insufficient funds'); return;
				}

				batch.update(db.doc(`balances/${tenantBalance.id}`), {
					available: new BigNumber(tenantBalance.available).minus(amountToPay).toString(10)
				});

				batch.update(db.doc(`balances/${landlordBalance.id}`), {
					available: new BigNumber(landlordBalance.available).plus(amountToPay).toString(10)
				});

				batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${uid}`), {
					'state.depositInfo':{
						creationTS: Date.now(),
						currencyForPayment:{
							currency,
							amount:amountToPay,
							name:currencyInfo.name
						}
					}
				});	

				const id1 = sharedService.generateId();
				const id2 = sharedService.generateId();
				const id3 = sharedService.generateId();

				batch.set(db.doc(`myProperties/${myPropertyID}/tenants/${uid}/history/${id1}`), {
					// currency,
					action:'deposit',
					user: {
						firstName:tenant.firstName,
						lastName:tenant.lastName,
						email:tenant.email,
						uid:tenant.uid
					},
					currencyForPayment:{
						currency,
						amount:amountToPay,
						name:currencyInfo.name
					},
					id:id1,
					creationTS: Date.now(),
				});

				batch.set(db.doc(`transactions/${id2}`), {
					currency,
					type: 'payment sent',
					details: `Rent payment for property: ${myProperty.address}`,
					myPropertyID,
					processed: true,
					status: 'confirmed',
					amount: amountToPay,
					uid: tenant.uid,
					id: id2,
					creationTS: Date.now()
				});

				batch.set(db.doc(`transactions/${id3}`), {
					currency,
					type: 'payment received',
					details: `Rent payment for property: ${myProperty.address}`,
					myPropertyID,
					processed: true,
					status: 'confirmed',
					amount: amountToPay,
					fee: '0',
					uid: myProperty.user.uid,
					id: id3,
					creationTS: Date.now()
				});

				await batch.commit();
				resolve();


					
			} catch (err) {
				reject('Unexpected error sending payment');
			}
		});
	}



	ticket(uid:string, myPropertyID:string, details:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();
				let [tenant] = (await db.getAll(db.doc(`myProperties/${myPropertyID}/tenants/${uid}`))).map(doc=>doc.data());
				const open = tenant.state && tenant.state.ticketInfo && tenant.state.ticketInfo.open;
				
				// if (propertyRented.status=='rented') {
					batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${uid}`), {
						'state.ticketInfo':{
							details:!open? details : null,
							open:!open,
							creationTS: Date.now(),
						}
					});

					batch.set(db.doc(`myProperties/${myPropertyID}/tenants/${uid}/history/${id}`), {
						creationTS: Date.now(),
						details:!open? details : null,
						action:'ticket',
						open:!open,
						user: {
							firstName:tenant.firstName,
							lastName:tenant.lastName,
							email:tenant.email,
							uid:tenant.uid
						},
						id
					});

					await batch.commit();
					resolve();
					
				// } else {
				// 	reject("You can't modify the rent");
				// }

				
			} catch (err) {
				reject('Unexpected error');
			}
		});
	}



	modifyRent(uid:string, myPropertyID:string, tenantID:string, newRent:number){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();
				let [myProperty, tenant] = (await db.getAll(
					db.doc(`myProperties/${myPropertyID}`),
					db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`)
					)).map(doc=>doc.data());

				if (myProperty.user.uid==uid) {
					// if (propertyRented.status=='rented') {
						batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`), {
							rentAmount: newRent,
							'state.rentModificationInfo':{
								creationTS: Date.now()
							}
						});
	
						batch.set(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}/history/${id}`), {
							creationTS: Date.now(),
							action: 'rentModified',
							newRent,
							user: myProperty.user,
							id
						});
	
						await batch.commit();
						resolve();
						
					// } else {
					// 	reject("You can't modify the rent");
					// }
				} else {
					reject('Invalid user');
				}

			} catch (err) {
				reject('Unexpected error Modifying Rent');
			}
		});
	}

	public estimateGasAndGasPriceContractCreation(uid:string):Promise<any>{
		return new Promise(async(resolve, reject)=>{		
			try{
				const db = admin.firestore();
				const identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()).docs[0].data();

				let gasPrice =	this.web3.eth.gasPrice.toString(10);

				const input = {
					language: 'Solidity',
					sources: {
						'PropertyContract.sol': {
							content: smartContract
						}
					},
					settings: {
						outputSelection: {
							'*': {
								'*': [ '*' ]
							}
						}
					}
				};

				const compiledContract = JSON.parse(solc.compile(JSON.stringify(input)));
				const byteCode = compiledContract.contracts['PropertyContract.sol']['PropertyContract'].evm.bytecode.object;
				const estimateGas =	this.web3.eth.estimateGas({
					gasPrice:	this.web3.toHex(gasPrice),
					from: identityWallet.address,
					value: '0x00',
					data: this.addPrefix(byteCode)
				});
					
		  	resolve({estimateGas, gasPrice});
			}catch(err){
				reject('Unexpected error. Try again later');
			}
		});
	}

	private addPrefix(data:string){
		return data.indexOf('0x')==0? data:`0x${data}`;
	}

	

	createSmartContract(uid: string, myPropertyID: string, gasPrice: number, gasLimit: number){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();
				const identityWallet = (await db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()).docs[0].data();

				const input = {
					language: 'Solidity',
					sources: {
						'PropertyContract.sol': {
							content: smartContract
						}
					},
					settings: {
						outputSelection: {
							'*': {
								'*': [ '*' ]
							}
						}
					}
				};

				const compiledContract = JSON.parse(solc.compile(JSON.stringify(input)));
				const byteCode = compiledContract.contracts['PropertyContract.sol']['PropertyContract'].evm.bytecode.object;

				const rawTx = {
	        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(identityWallet.address)),
	        gasPrice: this.web3.toHex(gasPrice),
	        gasLimit: this.web3.toHex(gasLimit),
	        value: '0x00',
	        data: this.addPrefix(byteCode)
		   	};

				let txHash = (await this.sendRawTransaction(identityWallet.privateKey, rawTx)).toLowerCase();

				batch.set(db.doc(`transactions/${id}`), {
					id,
					txHash,
					uid,
					status:'pending',
					type:'contractExecution',
					action:'contractCreation',
					currency:'ETH',
					myProperty:{
						id: myPropertyID
					},
					creationTS: Date.now()
				});

				batch.update(db.doc(`myProperties/${myPropertyID}`), {
					smartContract: {
						status: 'pending',
						txHash,
						address: null,
						gasPrice,
						gasUsed: null
					}
				});
				await batch.commit();

				resolve(txHash);
							
					              
			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}


	uploadToIPFS(url:string) {
		return new Promise(async(resolve, reject)=>{
			ipfs.addFromURL(url, (err, result: {path:string, hash:string, size:number}[]) => {
				if (err) {
					reject(err.message || err); return;
				}
				const {hash} = result.find(data => data.path.endsWith('.pdf'));
				resolve(hash);

			});
		});
	}

	public estimateGasAndGasPriceDocSaving(uid: string, myPropertyID:string){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const [myPropertySnapshot, identityWalletSnapshot] = await Promise.all([
					db.doc(`myProperties/${myPropertyID}`).get(),
					db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get()
				]);
				
				const identityWallet = identityWalletSnapshot.docs[0].data();
				const myProperty = myPropertySnapshot.data();


				let gasPrice =	this.web3.eth.gasPrice.toString(10);

				const myContractInstance = this.web3.eth.contract(environment.propertyContractABI).at(myProperty.smartContract.address);

				const tenant = '0x0a55a3c6693727b92936f7ce609b2c55c46923dd'; //fake
				const ipfsHash = 'Qmb6Q5soQJjfuNxdSYUEFU6qPLPDZtECV3zPZs15fPPpHT'; //fake

				const estimateGas = myContractInstance.saveDocLocation.estimateGas(ipfsHash, tenant, {from: identityWallet.address, value:0});

				resolve({estimateGas, gasPrice});
				
			} catch (err) {
				reject('Unexpected error. Try again later');
			}

		});
	}


	public saveDocOnBlockchain(uid: string, ipfsHash: string, myPropertyID: string, tenantID: string, signatureRequestSelectedID: string, gasPrice: number, gasLimit: number){
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = sharedService.generateId();

				const [myPropertySnapshot, landlordIdentityWalletSnapshot, tenantIdentityWalletSnapshot] = await Promise.all([
					db.doc(`myProperties/${myPropertyID}`).get(),
					db.collection(`wallets/identity/private`).where('uid','==',uid).where('currency','==','ETH').get(),
					db.collection(`wallets/identity/private`).where('uid','==',tenantID).where('currency','==','ETH').get()
				]);
				
				const landlordIdentityWallet = landlordIdentityWalletSnapshot.docs[0].data();
				const tenantIdentityWallet = tenantIdentityWalletSnapshot.docs[0].data();
				const myProperty = myPropertySnapshot.data();

				const myContractInstance = this.web3.eth.contract(environment.propertyContractABI).at(myProperty.smartContract.address);

				const rawTx = {
	        nonce: this.web3.toHex(this.web3.eth.getTransactionCount(landlordIdentityWallet.address)),
	        gasPrice: this.web3.toHex(gasPrice),
	        gasLimit: this.web3.toHex(gasLimit),
					value: '0x00',
					to: myProperty.smartContract.address,
	        data: myContractInstance.saveDocLocation.getData(ipfsHash, tenantIdentityWallet.address)
				 };
				 
				const txHash = await this.sendRawTransaction(landlordIdentityWallet.privateKey, rawTx);


				batch.set(db.doc(`transactions/${id}`), {
					id,
					txHash,
					uid,
					status:'pending',
					type:'contractExecution',
					action:'saveDocLocation',
					currency:'ETH',
					myProperty:{
						id: myPropertyID,
						tenant: tenantID,
						signatureRequestSelectedID
					},
					creationTS: Date.now()
				});

				batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest/${signatureRequestSelectedID}`), {
					smartContract: {
						status: 'pending',
						txHash,
						ipfsHash,
						gasPrice,
						gasUsed: null
					}
				});
				await batch.commit();

				resolve(txHash);


			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}


	private sendRawTransaction(privateKey:string, rawTx:any): Promise<string>{
		return new Promise(async(resolve, reject)=>{
			
			const tx = new Tx(rawTx);
			tx.sign(Buffer.from(privateKey, 'hex')); //Sign transaction
			let serializedTx = `0x${tx.serialize().toString('hex')}`;
			this.web3.eth.sendRawTransaction(serializedTx, (err, hash)=> {
				if(err){
					reject(err.message || err);
				} else{
					resolve(hash);
				}               
			});
		});
	}


	public endLease(uid:string, myPropertyID:string, tenantID:string){
		return new Promise(async(resolve, reject)=>{
			try{
				
				const db = admin.firestore();
				const batch = db.batch();

				let [myProperty, tenant] = (await db.getAll(
					db.doc(`myProperties/${myPropertyID}`),
					db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`)
				)).map(doc=>doc.data());
					
				if(myProperty.user.uid!=uid && tenant.uid!=uid){
					reject("You're not an user of this property"); return;
				}
				
				if (tenant.status=='active') {
					batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${tenant.uid}`), {
						status:'endingLease',
						requestedBy:uid
					});
					await batch.commit();
					resolve();
					return;
				}

				if(tenant.status!='endingLease' || tenant.requestedBy==uid){
					reject("You can't end the lease"); return;
				}
					

				let [landlordInfo, tenantInfo] = (await db.getAll(
					db.doc(`users/${myProperty.user.uid}`), 
					db.doc(`users/${tenant.uid}`), 
				)).map(doc=>doc.data());

				let [history, signaturesRequests] = (await Promise.all([
					db.collection(`myProperties/${myPropertyID}/tenants/${tenantID}/history`).get(),
					db.collection(`myProperties/${myPropertyID}/tenants/${tenantID}/signaturesRequest`).get()
				])).map(snapshot=>snapshot.docs.map(doc=>doc.data()));


				batch.delete(db.doc(`myProperties/${myPropertyID}/tenants/${tenant.uid}`));						

				for(let historyItem of history){
					batch.delete(db.doc(`myProperties/${myPropertyID}/tenants/${tenant.uid}/history/${historyItem.id}`))
				}

				for(let signaturesRequest of signaturesRequests){
					batch.delete(db.doc(`myProperties/${myPropertyID}/tenants/${tenant.uid}/signaturesRequest/${signaturesRequest.id}`))
				}							

				batch.update(db.doc(`myProperties/${myPropertyID}`), {
					tenantsID:admin.firestore.FieldValue.arrayRemove(tenant.uid)
				});


				let tenantObj = {
					'tenant.currentlyRented':tenantInfo.tenant.currentlyRented-1>=0? tenantInfo.tenant.currentlyRented-1:0,
				};

				let landlordObj = {
					'landlord.currentTenants':landlordInfo.landlord.currentTenants-1>=0? landlordInfo.landlord.currentTenants-1:0,
				};

				batch.update(db.doc(`users/${tenantInfo.uid}`), tenantObj);

				// batch.update(db.doc(`public/${tenantInfo.uid}`), tenantObj);

				batch.update(db.doc(`users/${landlordInfo.uid}`), landlordObj);

				// batch.update(db.doc(`public/${landlordInfo.uid}`), landlordObj);


				const id1 = sharedService.generateId();
				const id2 = sharedService.generateId();
				

				batch.set(db.doc(`users/${tenantInfo.uid}/notifications/${id1}`), {
					title: `Lease Ended`,
					content: `Lease Ended for: ${myProperty.address}`,
					id: id1,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${tenantInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});

				batch.set(db.doc(`users/${landlordInfo.uid}/notifications/${id2}`), {
					title: `Lease Ended`,
					content: `Lease Ended for: ${sharedService.capitalize(tenantInfo.firstName)} ${sharedService.capitalize(tenantInfo.lastName)} and property: ${myProperty.address}`,
					id: id2,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${landlordInfo.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});



				if(Date.now() - myProperty.creationTS >= 0){ //  7*24*3600*1000 7 days
					const id3 = sharedService.generateId();
					const id4 = sharedService.generateId();
				
					batch.set(db.doc(`users/${tenantInfo.uid}/notifications/${id3}`), {
						title: `Provide feedback`,
						content: `Provide feedback for ${sharedService.capitalize(landlordInfo.firstName)} as Landlord`,
						hasButton: true,
						buttonText: 'Click here',
						action: 'provide property landlord feedback',
						feedbackFor: landlordInfo.uid,
						id: id3,
						creationTS: Date.now(),
					});

					batch.update(db.doc(`users/${tenantInfo.uid}/settings/preferences`), {
						'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
					});

					batch.set(db.doc(`users/${landlordInfo.uid}/notifications/${id4}`), {
						title: `Provide feedback`,
						content: `Provide feedback for ${sharedService.capitalize(tenantInfo.firstName)} as Tenant`,
						hasButton: true,
						buttonText: 'Click here',
						action: 'provide property tenant feedback',
						feedbackFor: tenantInfo.uid,
						id: id4,
						creationTS: Date.now(),
					});

					batch.update(db.doc(`users/${landlordInfo.uid}/settings/preferences`), {
						'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
					});
					
				}

				await batch.commit();
				resolve();

				
			}catch(err){
				reject('Unexpected error ending lease');
			}
		});
	}
	

	
	public cancelCulmination(uid:string, myPropertyID:string, tenantID:string){
		return new Promise(async(resolve, reject)=>{
			try{
				const db = admin.firestore();
				const batch = db.batch();

				let [tenant] = (await db.getAll(
					db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`)
				)).map(doc=>doc.data());

				if (tenant.status=='endingLease' && tenant.requestedBy==uid) {
					batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`), {
						status:'active',
						requestedBy:admin.firestore.FieldValue.delete()
					});
					await batch.commit();
					resolve();

				} else {
					reject('Uknown action');
				}
			}catch(err){
				reject('Unexpected error canceling request');
			}
		});
	}
	

	startChat(uid: string, content: string, myPropertyID: string, tenantID: string) {
		return new Promise(async(resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const chatID = sharedService.generateId();
				const messageID = sharedService.generateId();

				const [tenantInfo, myProperty] = (await db.getAll(db.doc(`users/${tenantID}`), db.doc(`myProperties/${myPropertyID}`))).map(docs => docs.data());



				batch.update(db.doc(`myProperties/${myPropertyID}/tenants/${tenantID}`), { chatID });

				batch.set(db.doc(`chats/${chatID}`), {
					chatName: `(${sharedService.capitalize(tenantInfo.firstName)} ${tenantInfo.lastName[0].toUpperCase()}) ${myProperty.name}`,
					id: chatID,
					lastMessage: {
						content,
						uid,
						creationTS: Date.now()
					},
					uids: [myProperty.user.uid, tenantInfo.uid]
				});

				batch.set(db.doc(`chats/${chatID}/messages/${messageID}`), {
					content,
					uid,
					creationTS: Date.now(),
					id: messageID
				});

				await batch.commit();
				resolve({chatID, messageID});


			} catch (err) {
				reject('Unexpected error');
			}
		});
	}









	private deleteFiles(files:string[]){
		const bucket = admin.storage().bucket();
		let arrayOfPromises = [];
		for(let file of files){
			arrayOfPromises.push(bucket.file(file).delete());
		}
		// let [res] = await bucket.getFiles({prefix:'users/Mijuj20TzOWaHpAkDutAoVVFzRI2/propertyListing/images'});
		//await bucket.deleteFiles({prefix:'users/Mijuj20TzOWaHpAkDutAoVVFzRI2/propertyListing/images'});
		return Promise.all(arrayOfPromises);
	}




}