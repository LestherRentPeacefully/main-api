import * as admin from "firebase-admin";
const sql = require('mssql');
import {Database} from '../Database';
import {SharedService} from '../Shared/SharedService';
import {BigNumber} from 'bignumber.js';
import {environment} from '../../environments/environment';
import {Nodemailer} from "../Email/Nodemailer";

const sharedService = new SharedService();


export class ListingsService{



  public new(fields:any, uid:string):Promise<any> {
		return new Promise(async (resolve, reject)=>{
			const db = admin.firestore();
			const batch = db.batch();

			try{

				let userInfo = (await db.doc(`users/${uid}`).get()).data();

				const listingID = sharedService.generateId();
				

				let listingObj:any = {
					user:{
						firstName:userInfo.firstName,
						lastName:userInfo.lastName,
						email:userInfo.email,
						uid:userInfo.uid,
					},
					name:fields.name,
					type:fields.type,
					numOfBeds:fields.numOfBeds,
					numOfBaths:fields.numOfBaths,
					sqft:fields.sqft,
					rentAmount:fields.rentAmount,
					securityDepositAmount:fields.securityDepositAmount,
					details:fields.details,
					availableFrom:fields.availableFrom,
					photos:fields.photos,
					amenities:fields.amenities,
					id:listingID,	
					creationTS:Date.now(),
					applicants:0,
					// typeOfContract: 'traditional contract',
					recommendedUntil: 0,
					status : 'pending'
				}


					
				if (fields.role=='real estate agent') {

					if(!userInfo.status.realEstateAgentIDVerified){
						reject('Please, verify your ID'); return;
					}

					if(
						userInfo.membership.pricingPlan !== 'enterprise' &&
						userInfo.realEstateAgent.currentlyListed >= sharedService.pricings[userInfo.membership.pricingPlan].realEstateAgent.maxListed
						// (userInfo.membership.pricingPlan=='free' && userInfo.realEstateAgent.currentlyListed>=2) || 
						// (userInfo.membership.pricingPlan=='basic' && userInfo.realEstateAgent.currentlyListed>=7) || 
						// (userInfo.membership.pricingPlan=='plus' && userInfo.realEstateAgent.currentlyListed>=15) || 
						// (userInfo.membership.pricingPlan=='professional' && userInfo.realEstateAgent.currentlyListed>=20)
					){
						reject('Too many properties already listed. Upgrade to a higher plan'); return;
					}

					fields.location.geopoint = new admin.firestore.GeoPoint(fields.location.lat, fields.location.lng);
					delete fields.location.lat, delete fields.location.lng;

					if(fields.includesLandlord){
						let landlordSnapshot = await db.collection(`users`).where('email','==',fields.landlordEmail.toLowerCase()).get();
	
						if(landlordSnapshot.empty){
							reject('Email not found'); return;
						}
	
						let landlord = landlordSnapshot.docs[0].data();

						if(!landlord.status.IDVerified) {
							reject('Request the landlord to verify his/her Identity'); return;
						}

						if(landlord.uid === uid){
							reject("You can't be the landlord"); return;
						}
	
						listingObj.landlord = {
							firstName: landlord.firstName,
							lastName: landlord.lastName,
							email: landlord.email,
							uid: landlord.uid,
							// role:'landlord',
						};

						let notifID = sharedService.generateId();

						batch.set(db.doc(`users/${landlord.uid}/notifications/${notifID}`), {
							title: `Property listing`,
							content: `${sharedService.capitalize(userInfo.firstName)} ${sharedService.capitalize(userInfo.lastName)} wants to put your property for rent`,
							hasButton: true,
							buttonText: 'Confirm',
							action: 'confirm property listing',
							listingID,
							id: notifID,
							realEstateAgentID: uid,
							creationTS: Date.now(),
						});
	
						batch.update(db.doc(`users/${landlord.uid}/settings/preferences`), {
							'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
						});
					
						listingObj.status = 'wating for landlord confirmation';
					}


					listingObj.user.role = 'real estate agent';
					listingObj.address = fields.address;
					listingObj.location = fields.location;
					listingObj.unit = fields.unit || '';
					listingObj.includesLandlord = fields.includesLandlord;
					

					if(fields.agreementId){
						let agreement = (await db.doc(`agreements/${fields.agreementId}`).get()).data();
						
						listingObj.agreementId = fields.agreementId;
						listingObj.includesLandlord = true;
						listingObj.landlord = {
							firstName: agreement.landlord.firstName,
							lastName: agreement.landlord.lastName,
							email: agreement.landlord.email,
							uid: agreement.landlord.uid,
							// role:'landlord',
						};

						batch.update(db.doc(`agreements/${fields.agreementId}`), {
							listingID
						});
					}
					


					let realEstateAgentObj = {
						'realEstateAgent.currentlyListed': userInfo.realEstateAgent.currentlyListed + 1
					}

					batch.update(db.doc(`users/${uid}`), realEstateAgentObj);
					
					// batch.update(db.doc(`public/${uid}`), realEstateAgentObj);


					
				} else {

					if(!userInfo.status.IDVerified){
						reject('Please, verify your ID'); return;
					}

					if(
						userInfo.membership.pricingPlan !== 'enterprise' &&
						userInfo.landlord.currentlyListed >= sharedService.pricings[userInfo.membership.pricingPlan].landlord.maxListed
						// (userInfo.membership.pricingPlan=='free' && userInfo.landlord.currentlyListed>=2) || 
						// (userInfo.membership.pricingPlan=='basic' && userInfo.landlord.currentlyListed>=7) || 
						// (userInfo.membership.pricingPlan=='plus' && userInfo.landlord.currentlyListed>=15) || 
						// (userInfo.membership.pricingPlan=='professional' && userInfo.landlord.currentlyListed>=20)
					){
						reject('Too many properties already listed. Upgrade to a higher plan'); return;
					}
					
					let myProperty = (await db.doc(`myProperties/${fields.myPropertyID}`).get()).data();

					if(!myProperty){
						reject("The property doesn't exists"); return;
					}
					if(myProperty.createdByRealEstateAgent) {
						reject("You can't list for this property");
					}
					if(myProperty.listingID){
						reject('The listing already exists'); return;
					}
					

					listingObj.user.role = 'landlord';
					listingObj.address = myProperty.address;
					listingObj.location = myProperty.location;
					listingObj.unit = myProperty.unit || '';
					listingObj.myPropertyID = myProperty.id;


					let landlordObj = {
						'landlord.currentlyListed': userInfo.landlord.currentlyListed + 1
					};
					
					batch.update(db.doc(`users/${uid}`), landlordObj);
					
					// batch.update(db.doc(`public/${uid}`), landlordObj);
					
					batch.update(db.doc(`myProperties/${myProperty.id}`), { listingID });

				}

				
				
				batch.set(db.doc(`listings/${listingID}`), listingObj);

				await batch.commit();
				resolve(listingID);


			}catch(err){
				reject('Unexpected error listing property');
			}

		});
	}


	public async isTenant(uid:string, myPropertyID:string){
		return (await admin.firestore().doc(`myProperties/${myPropertyID}/tenants/${uid}`).get()).exists;
	}


	public submitApplication(uid:string, listingID:string, rentAmount:number, securityDepositAmount:number, message:string){
		const db = admin.firestore();
		const batch = db.batch();
		
		return new Promise(async (resolve, reject)=>{
			try{

				let [listingDoc, userDoc, applicationDoc] =	await Promise.all([
					db.doc(`listings/${listingID}`).get(),
					db.doc(`users/${uid}`).get(),
					db.collection(`applications`).where('listing.id','==',listingID).where('applicant.uid','==',uid).limit(1).get()
				]);

				let listing = listingDoc.data();
				let userInfo = userDoc.data();
				let application = applicationDoc.empty? null : applicationDoc.docs[0].data();

				let isTenant:boolean;

				if(listing.myPropertyID){
					isTenant = await this.isTenant(uid, listing.myPropertyID);
				}else{ //PROPERTY IS NOT CREATED YET
					isTenant = false;
				}

				if(listing.user.role == 'real estate agent' && !listing.includesLandlord){
					reject("You can't apply to a view only property"); return;
				}
				if (listing.user.uid==uid) {
					reject("You can't apply to your own propery"); return;
				}
				if (listing.user.role === 'real estate agent' && listing.includesLandlord && listing.landlord.uid === uid) {
					reject("You're the landlord of this property"); return;
				}
				if(!userInfo.status.IDVerified){
					reject('Please, verify your ID'); return;
				}
				if(listing.status!='listed'){
					reject("You can't apply to this property"); return;
				} 
				if(isTenant){
					reject("You're already a Tenant of this property"); return;					
				}

				if(!application){
					if (listing.applicants>=200) {
						reject('There are too many applicants for this property'); return;
					}

					const id = db.collection('uniqueID').doc().id;
					
					batch.set(db.doc(`applications/${id}`), {
						listing:{
							name:listing.name,
							address:listing.address,
							creationTS:listing.creationTS,
							type:listing.type,
							id:listing.id,
							status:'listed'
						},
						applicant:{
							offer:{	rentAmount,	securityDepositAmount	},
							isHighlighted:0,
							message,
							uid,
						},
						id,
						creationTS:Date.now()
					});
					
					batch.update(db.doc(`listings/${listingID}`), {
						applicants:listing.applicants + 1
					});

					await batch.commit();
					resolve();
					return;
				} 
				
				if(application.applicant.status=='selected'){
					reject("You can't no longer modify the application"); return;
				}

				batch.update(db.doc(`applications/${application.id}`), {
					'applicant.offer':{
						rentAmount,	securityDepositAmount
					},
					'applicant.message':message
				});

				await batch.commit();
				resolve();
				
					

			}catch(err){
				reject('Unexpected error submitting application');
			}

		});
	}


	public deleteApplication(uid:string, listingID:string):Promise<any>{
		const db = admin.firestore();
		const batch = db.batch();
		return new Promise(async (resolve, reject)=>{
			try{
				let [listingDoc, applicationDoc] = await Promise.all([ 
					db.doc(`listings/${listingID}`).get(), 
					db.collection(`applications`)
						.where('listing.id','==',listingID)
						.where('applicant.uid','==',uid)
						.limit(1).get()
				]);

				let listing = listingDoc.data();
				let application = applicationDoc.empty? null : applicationDoc.docs[0].data();

				if (application) {
					batch.update(db.doc(`listings/${listingID}`),{
						applicants:(listing.applicants-1)>=0? (listing.applicants-1) : 0
					});
					batch.delete(db.doc(`applications/${application.id}`));

					await batch.commit();
					resolve();

				} else {
					reject('Application already deleted');
				}
			}catch(err){
				reject('Unexpected error deleting application');
			}
		});
	}



	public acceptApplication(uid:string, listingID:string, applicantID:string){
		const db = admin.firestore();
		const batch = db.batch();
		return new Promise(async (resolve, reject)=>{
			try {
				let [listingDoc, applicationDoc] = await Promise.all([
					db.doc(`listings/${listingID}`).get(), 
					db.collection(`applications`)
						.where('listing.id','==',listingID)
						.where('applicant.uid','==',applicantID)
						.limit(1).get()
				]);
				
				let listing = listingDoc.data();

				let application = applicationDoc.docs[0].data();
				
				if (listing.user.uid==uid && application.applicant.status!='selected') {

					batch.update(db.doc(`applications/${application.id}`), {
						'applicant.status':'selected'
					});


					await batch.commit();
					resolve();

				} else {
					reject('Application already accepted');
				}
			} catch (err) {
				reject('Unexpected error retracting application');
			}
		});
	}


	public retractApplication(uid:string, listingID:string, applicantID:string){
		return new Promise(async (resolve, reject)=>{
			const db = admin.firestore();
			const batch = db.batch();

			try {
				let [listingDoc, applicationDoc] = await Promise.all([
					db.doc(`listings/${listingID}`).get(), 
					db.collection(`applications`)
						.where('listing.id','==',listingID)
						.where('applicant.uid','==',applicantID)
						.limit(1).get()
				]);
				
				let listing = listingDoc.data();

				let application = applicationDoc.docs[0].data();
				
				if (listing.user.uid==uid && application.applicant.status=='selected') {

					batch.update(db.doc(`applications/${application.id}`), {
						'applicant.status':admin.firestore.FieldValue.delete()
					});


					await batch.commit();
					resolve();

				} else {
					reject('Application already retracted');
				}
			} catch (err) {
				reject('Unexpected error retracting application');
			}
		});
	}



	public moveIn(tenantID:string, listingID:string, listingOwnerID:string){
		return new Promise(async (resolve, reject)=>{
			const db = admin.firestore();
			const batch = db.batch();

			try {
				let [[listingDoc, tenantDoc, listingOwnerDoc], applicationDoc] = await Promise.all([
					db.getAll(db.doc(`listings/${listingID}`), db.doc(`users/${tenantID}`), db.doc(`users/${listingOwnerID}`)),
					db.collection(`applications`)
						.where('listing.id','==',listingID)
						.where('applicant.uid','==',tenantID)
						.limit(1).get()
				]);
				
				let listing = listingDoc.data();
				let tenantInfo = tenantDoc.data();
				let listingOwnerInfo = listingOwnerDoc.data();
				let application = applicationDoc.docs[0].data();

				if (application.applicant.status!='selected') {
					reject("You can't move to this property"); return;
				}
				if(listing.user.uid!= listingOwnerInfo.uid){
					reject('Invalid Listing owner'); return;
				}


				let tenantObj = {
					'tenant.totalRented':tenantInfo.tenant.totalRented + 1,
					'tenant.currentlyRented':tenantInfo.tenant.currentlyRented + 1,
				};

				if(listing.user.role == 'real estate agent'){

					let realEstateAgentObj = {
						'realEstateAgent.totalTenants':listingOwnerInfo.realEstateAgent.totalTenants + 1
					};

					const request = Database.pool.request();

          const SQLcommand = `
            UPDATE realEstateAgents SET 
            [totalTenants] = ${realEstateAgentObj['realEstateAgent.totalTenants']} 
            WHERE uid = '${listingOwnerID}'`;

          await request.query(SQLcommand);
					
					batch.update(db.doc(`users/${listingOwnerID}`),realEstateAgentObj);
					// batch.update(db.doc(`public/${listingOwnerID}`), realEstateAgentObj);

				}else{
					
					let landlordObj = {
						'landlord.totalTenants':listingOwnerInfo.landlord.totalTenants + 1,
						'landlord.currentTenants':listingOwnerInfo.landlord.currentTenants + 1,
					};
					
					batch.update(db.doc(`users/${listingOwnerID}`),landlordObj);
					// batch.update(db.doc(`public/${listingOwnerID}`), landlordObj);
					
				}
				
				batch.update(db.doc(`users/${tenantID}`), tenantObj);
				
				// batch.update(db.doc(`public/${tenantID}`), tenantObj);

				batch.delete(db.doc(`applications/${application.id}`));

				batch.update(db.doc(`listings/${listingID}`), {
					applicants: (listing.applicants-1)>=0? (listing.applicants-1) : 0
				});
				

				if(listing.myPropertyID){
					batch.update(db.doc(`myProperties/${listing.myPropertyID}`), {
						tenantsID: admin.firestore.FieldValue.arrayUnion(tenantID)
					});

				} else{ //PROPERTY IS NOT CREATED YET
					listing.myPropertyID = sharedService.generateId();

					let landlord = listing.landlord;

					batch.set(db.doc(`myProperties/${listing.myPropertyID}`), {
						user:{
							firstName:landlord.firstName,
							lastName:landlord.lastName,
							email:landlord.email,
							uid:landlord.uid,
							role:'landlord',
						},
						name:listing.name,
						address:listing.address,
						location:listing.location,
						unit:listing.unit,
						type:listing.type,
						// typeOfContract:listing.typeOfContract,
						photos:[listing.photos[0]],
						id:listing.myPropertyID,	
						creationTS:Date.now(),
						tenantsID:[tenantID],
						createdByRealEstateAgent: true,
						listingID: null
					});

					if(listing.agreementId) {
						batch.update(db.doc(`agreements/${listing.agreementId}`), {
							isPropertyRented: true
						});
					}

					batch.update(db.doc(`listings/${listingID}`), {
						myPropertyID: listing.myPropertyID
					});

				}
				

				batch.set(db.doc(`myProperties/${listing.myPropertyID}/tenants/${tenantID}`), {
					firstName:tenantInfo.firstName,
					lastName:tenantInfo.lastName,
					uid:tenantInfo.uid,
					email:tenantInfo.email,
					rentAmount:application.applicant.offer.rentAmount,
					securityDepositAmount:application.applicant.offer.securityDepositAmount,
					creationTS: Date.now(),
					status:'active',
					id:tenantID
				});

				await batch.commit();
				resolve(listing.myPropertyID);

			} catch (err) {
				reject(err.message || err);
				// reject('Unexpected error confirming move. Try again later');
			}
		});
	}



	public deleteListing(uid:string, listingID:string, oldFiles:string[]){
		const db = admin.firestore();
		const batch = db.batch();
		return new Promise(async (resolve, reject)=>{
			try {
				
				let [listing, userInfo] = (await db.getAll( db.doc(`listings/${listingID}`),	db.doc(`users/${uid}`))).map(doc=>doc.data());
				

				let request = Database.pool.request().input('id', sql.NVarChar(50), listingID);
				let SQLcommand = 
				 `IF (SELECT COUNT(*) FROM listings WHERE id = @id) = 1
						BEGIN
							DELETE FROM listings WHERE id = @id;
						END`;

				if (listing.user.uid!=uid) {
					reject("You're not the owner of the listing"); return;
				}

				if(listing.status === 'rented') {
					reject("You can't delete this listing"); return;	
				}

				batch.delete(db.doc(`listings/${listingID}`));

				if (listing.user.role=='real estate agent') {
					let realEstateAgentObj = {
						'realEstateAgent.currentlyListed': userInfo.realEstateAgent.currentlyListed - 1 >=0? userInfo.realEstateAgent.currentlyListed - 1 : 0
					}

					batch.update(db.doc(`users/${uid}`), realEstateAgentObj);
					// batch.update(db.doc(`public/${uid}`), realEstateAgentObj);

					if(listing.includesLandlord){
						let applicationsDocs = await db.collection('applications').where('listing.id','==',listingID).get();
		
						for(let applicationDoc of applicationsDocs.docs){
							batch.delete(db.doc(`applications/${applicationDoc.id}`));
						}

						if(listing.agreementId) {
							batch.update(db.doc(`agreements/${listing.agreementId}`), {
								listingID: null
							});
						}
					}


				} else {
					let applicationsDocs = await db.collection('applications').where('listing.id','==',listingID).get();
	
					for(let applicationDoc of applicationsDocs.docs){
						batch.delete(db.doc(`applications/${applicationDoc.id}`));
					}
					let landlordObj = {
						'landlord.currentlyListed': userInfo.landlord.currentlyListed - 1 >=0? userInfo.landlord.currentlyListed - 1 : 0
					}
	
					batch.update(db.doc(`users/${uid}`), landlordObj);
	
					// batch.update(db.doc(`public/${uid}`), landlordObj);

					batch.update(db.doc(`myProperties/${listing.myPropertyID}`), { listingID:null });
				}


				await request.query(SQLcommand);
				await batch.commit();
				if(oldFiles && oldFiles.length>0) sharedService.deleteFiles(oldFiles).catch(e=>'');
				
				resolve();

			} catch (err) {
				reject('Unexpected error deletting property');
			}
		});
	}




	public async getTotalListingsOnApp(){
		let total = (await Database.pool.request().query('SELECT COUNT(*) AS total FROM listings')).recordset[0].total;
		return total;
	}




	public async getTotalListings(filter:any){
		if(filter.amenities){
			for(let amenity of filter.amenities.split(',')){
				filter[amenity=='view'? '_view' : amenity] = 1;
			}
		}
		delete filter.amenities;
		
		let conditionString = 'distance < @radius AND status = @status';
		let columsString = 'id, creationTS, status';

		let request = Database.pool.request()
										.input('lat', sql.Float, filter.lat)
										.input('lng', sql.Float, filter.lng)
										.input('radius', sql.Float, filter.radius)
										.input('status', sql.NVarChar(50), 'listed');

		for(let key of Object.keys(filter)){
			if(filter[key]){
				if (key=='minPrice') {
					request = request.input('minPrice', filter.minPrice);
					conditionString += ' AND rentAmount >= @minPrice';
					if(!columsString.includes('rentAmount')) columsString += ', rentAmount';
					
				} else if(key=='maxPrice'){
					request = request.input('maxPrice', filter.maxPrice);
					conditionString += ' AND rentAmount <= @maxPrice';
					if(!columsString.includes('rentAmount')) columsString += ', rentAmount';
					
				} else if(key!='radius' && key!='lat' && key!='lng'){
					request = request.input(key, filter[key]);
					conditionString += ` AND ${key} = @${key}`;
					columsString += `, ${key}`;
				}
			}
		}

		let SQLcommand = 
			`SELECT COUNT(*) as total FROM
			(
				SELECT ${columsString}, 
				( 6371 * acos( cos(radians(@lat))*cos(radians(lat))*cos(radians(lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(lat)) ) ) 
				AS distance FROM listings
			) AS tb
			WHERE ${conditionString}`;

		let total = (await request.query(SQLcommand)).recordset[0].total;
		return total;
	}



	public async getListings(filter:any, offset:number){
		if(filter.amenities){
			for(let amenity of filter.amenities.split(',')){
				filter[amenity=='view'? '_view' : amenity] = 1;
			}
			delete filter.amenities;
		}

		const db = admin.firestore();

		let arrayOfPromises:FirebaseFirestore.DocumentReference[] = [];
		let conditionString = 'distance < @radius AND status = @status';
		let columsString = 'id, creationTS, status, recommendedUntil';

		let request = Database.pool.request()
										.input('lat', sql.Float, filter.lat)
										.input('lng', sql.Float, filter.lng)
										.input('radius', sql.Float, filter.radius)
										.input('status', sql.NVarChar(50), 'listed');

		for(let key of Object.keys(filter)){
			if(filter[key]){
				if (key=='minPrice') {
					request = request.input('minPrice', filter.minPrice);
					conditionString += ' AND rentAmount >= @minPrice';
					if(!columsString.includes('rentAmount')) columsString += ', rentAmount';
					
				} else if(key=='maxPrice'){
					request = request.input('maxPrice', filter.maxPrice);
					conditionString += ' AND rentAmount <= @maxPrice';
					if(!columsString.includes('rentAmount')) columsString += ', rentAmount';
					
				} else if(key!='radius' && key!='lat' && key!='lng'){
					request = request.input(key, filter[key]);
					conditionString += ` AND ${key} = @${key}`;
					columsString += `, ${key}`;
				}
			}
		}

		let SQLcommand = 
			`SELECT id, creationTS, recommendedUntil FROM
			(
				SELECT ${columsString}, 
				( 6371 * acos( cos(radians(@lat))*cos(radians(lat))*cos(radians(lng)-radians(@lng)) + sin(radians(@lat))*sin(radians(lat)) ) ) 
				AS distance FROM listings
			) AS tb
			WHERE ${conditionString} ORDER BY recommendedUntil DESC, creationTS DESC`; // OFFSET ${offset} ROWS FETCH NEXT 9 ROWS ONLY
		
		let listings:any[] = (await request.query(SQLcommand)).recordset;
									 			
		for(let listing of listings){
			arrayOfPromises.push(db.doc(`listings/${listing.id}`));
		}

		let listingsSnapshot = arrayOfPromises.length> 0? await db.getAll(...arrayOfPromises) : [];
		
		return listingsSnapshot.map(snapshot=>{
			let data = snapshot.data();
			delete data.amenities, delete data.details, delete data.location, delete data.creationTS;
			return data;
		});
	}


	
	public async getTotalAndListings(filter:any,offset:number){
		let [listings, total] = await Promise.all([this.getListings(filter, offset), this.getTotalListings(filter)]);
		return {listings, total};
	}



	

	public editListing(uid:string, fields:any, listingID:string, oldFiles:string[]){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				
				let listing = (await db.doc(`listings/${listingID}`).get()).data();

				if (listing.user.uid != uid) {
					reject("You're not the owner of the listing"); return;
				}
				if(listing.status != 'pending' && listing.status != 'rejected' && listing.status !='wating for landlord confirmation'){
					reject("You can't edit this listing"); return;
				}

				if(listing.user.role=='real estate agent') {
					fields.location.geopoint = new admin.firestore.GeoPoint(fields.location.lat, fields.location.lng);
					delete fields.location.lat, delete fields.location.lng;
				}

				batch.update(db.doc(`listings/${listingID}`), {
					name:fields.name,
					type:fields.type,
					numOfBeds:fields.numOfBeds,
					numOfBaths:fields.numOfBaths,
					sqft:fields.sqft,
					rentAmount:fields.rentAmount,
					securityDepositAmount:fields.securityDepositAmount,
					details:fields.details,
					availableFrom:fields.availableFrom,
					photos:fields.photos,
					amenities:fields.amenities,
					location:listing.user.role=='real estate agent'? fields.location: listing.location,
					status:'pending',
				});

				await batch.commit();

				if(oldFiles && oldFiles.length>0) sharedService.deleteFiles(oldFiles).catch(e=>'');
				resolve();

			} catch (err) {
				console.log(err.message || err);
				reject('Unexpected error editing listing');
			}
		});
	}


	public markListingAsRented(uid:string, listingID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let [listingDoc, applicationsSnapshot, userDoc] = await Promise.all([
					db.doc(`listings/${listingID}`).get(),
					db.collection('applications').where('listing.id','==',listingID).get(),
					db.doc(`users/${uid}`).get()
				]);

				let listing = listingDoc.data();
				let userInfo = userDoc.data();
				
				const tenantsExist = !(await db.collection(`myProperties/${listing.myPropertyID}/tenants`).limit(1).get()).empty;

				if (listing.user.uid != uid) {
					reject("You're not the owner of the listing"); return;
				}
				if(listing.status != 'listed'){
					reject("You can't edit this listing"); return;
				}

				let request = Database.pool.request().input('id', sql.NVarChar(50), listingID);
				let SQLcommand = `UPDATE listings SET [status]='rented' WHERE id = @id`;

				batch.update(db.doc(`listings/${listingID}`), {	status: 'rented' });


				if (listing.user.role=='real estate agent') {

					if(listing.includesLandlord && !tenantsExist){ //!listing.myPropertyID
						reject("There's no tenants in this property"); return;
					}

					let realEstateAgentObj;

					if(listing.includesLandlord){
						realEstateAgentObj = {
							'realEstateAgent.currentlyListed': userInfo.realEstateAgent.currentlyListed - 1 >=0? userInfo.realEstateAgent.currentlyListed - 1 : 0,
							'realEstateAgent.totalRented':userInfo.realEstateAgent.totalRented + 1,
						}
						
						// batch.update(db.doc(`myProperties/${listing.myPropertyID}`), {
						// 	listingID: null
						// });
	
						for(let applicationSnapshot of applicationsSnapshot.docs){
							batch.update(db.doc(`applications/${applicationSnapshot.id}`), {
								'listing.status': 'rented'
							});
						}

						if(listing.agreementId) {
							batch.update(db.doc(`agreements/${listing.agreementId}`), {
								listingID: null
							});
						}

						const request2 = Database.pool.request();

						const SQLcommand = `
							UPDATE realEstateAgents SET 
							[totalRented] = ${realEstateAgentObj['realEstateAgent.totalRented']} 
							WHERE uid = '${uid}'`;

						await request2.query(SQLcommand);


					}else{
						realEstateAgentObj = {
							'realEstateAgent.currentlyListed': userInfo.realEstateAgent.currentlyListed - 1 >=0? userInfo.realEstateAgent.currentlyListed - 1 : 0
						}
					}


					batch.update(db.doc(`users/${uid}`), realEstateAgentObj);
					// batch.update(db.doc(`public/${uid}`), realEstateAgentObj);



				}else{

					if(!tenantsExist){
						reject("There's no tenants in this property"); return;
					}


					let landlordObj = {
						'landlord.currentlyListed': userInfo.landlord.currentlyListed - 1 >=0? userInfo.landlord.currentlyListed - 1 : 0,
						'landlord.totalRented':userInfo.landlord.totalRented + 1,
					};
	
					batch.update(db.doc(`users/${uid}`), landlordObj);
	
					// batch.update(db.doc(`public/${uid}`), landlordObj);
	
					batch.update(db.doc(`myProperties/${listing.myPropertyID}`), { listingID:null });

					for(let applicationSnapshot of applicationsSnapshot.docs){
						batch.update(db.doc(`applications/${applicationSnapshot.id}`), {
							'listing.status': 'rented'
						});
					}

				}


				await request.query(SQLcommand);
				await batch.commit();
				resolve();

			} catch (err) {
				reject('Unexpected error marking property as rented');
			}
		});
	}



	public async getCurrencyPriceForFeesAndCharges(uid:string, currency:string){
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForFeesAndCharges/${uid}`).set({currency, price, name});
    return {currency, price};
	}
	


	public highlightListing(uid:string, currency:string, listingID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = db.collection('uniqueID').doc().id;

				let [balanceSnaphot, [lastPriceForFeesAndChargesSnapshot, listingSnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`lastPriceForFeesAndCharges/${uid}`), db.doc(`listings/${listingID}`))
				]);
				
				const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();
        
				let balance = balanceSnaphot.docs[0].data();
				
				if(balanceSnaphot.empty){
					reject('Insufficient funds'); return;
				}

        let lastPriceForFeesAndCharges = lastPriceForFeesAndChargesSnapshot.data();
				let listing = listingSnapshot.data();

				let amountToPay = new BigNumber(10).div(lastPriceForFeesAndCharges.price.USD).decimalPlaces(8).toString(10); //10 USD

				
				if (listing.user.uid != uid) {
					reject("Invalid user"); return;
				}
				
				if(balanceSnaphot.empty || !(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))){
					reject('Insufficient funds'); return;
				}

				
				const recommendedUntil = sharedService.addMonths(Date.now(), 1).getTime();

				let request = Database.pool.request()
					.input('recommendedUntil', sql.BigInt, recommendedUntil)
					.input('id', sql.NVarChar(50), listingID);

				let SQLcommand = `UPDATE listings SET [recommendedUntil] = @recommendedUntil WHERE id = @id`;


				batch.update(db.doc(`balances/${balance.id}`), {
					available: new BigNumber(balance.available).minus(amountToPay).toString(10)
				});

				batch.update(db.doc(`totalUsersBalances/${currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
				});

				batch.update(db.doc(`listings/${listingID}`), {	recommendedUntil });

				batch.set(db.doc(`transactions/${id}`), {
					currency,
					type: 'fees and charges',
					processed: true,
					status: 'confirmed',
					amount: amountToPay,
					details: `Fee charged for highliting listing`,
					uid,
					id,
					creationTS: Date.now()
				});

				await request.query(SQLcommand);
				await batch.commit();
				resolve();

				
			} catch (err) {
				reject(err.message || err);
				// reject('Unexpected error. Try again later');
			}
		});
	}


	public highlightApplication(uid:string, currency:string, listingID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = db.collection('uniqueID').doc().id;

				let [balanceSnaphot, applicationDoc, [lastPriceForFeesAndChargesSnapshot]] = await Promise.all([
					db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
					db.collection(`applications`).where('listing.id','==',listingID).where('applicant.uid','==',uid).limit(1).get(),
          db.getAll(db.doc(`lastPriceForFeesAndCharges/${uid}`))
				]);

				const totalUsersBalance = (await db.doc(`totalUsersBalances/${currency}`).get()).data();
				
				let balance = balanceSnaphot.docs[0].data();

				if(balanceSnaphot.empty){
					reject('Insufficient funds'); return;
				}

        let lastPriceForFeesAndCharges = lastPriceForFeesAndChargesSnapshot.data();
				let application = applicationDoc.docs[0].data();

				let amountToPay = new BigNumber(5).div(lastPriceForFeesAndCharges.price.USD).decimalPlaces(8).toString(10); // 5 USD

				if(balanceSnaphot.empty || !(new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay))){
					reject('Insufficient funds'); return;
				}

				if(application.applicant.isHighlighted){
					reject('Application already highlighted'); return;
				}


				batch.update(db.doc(`balances/${balance.id}`), {
					available: new BigNumber(balance.available).minus(amountToPay).toString(10)
				});

				batch.update(db.doc(`totalUsersBalances/${currency}`), {
					amount: new BigNumber(totalUsersBalance.amount).minus(amountToPay).toString()
				});

				batch.update(db.doc(`applications/${application.id}`), { 'applicant.isHighlighted': 1	});

				batch.set(db.doc(`transactions/${id}`), {
					currency,
					type: 'fees and charges',
					processed: true,
					status: 'confirmed',
					amount: amountToPay,
					details: `Fee charged for highliting application`,
					uid,
					id,
					creationTS: Date.now()
				});

				await batch.commit();
				resolve();



			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}


	contactAdvertiser(listingID:string, message:string, email:string, name:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = db.collection('uniqueID').doc().id;
				let listing = (await db.doc(`listings/${listingID}`).get()).data();

				if(!listing){
					reject("listing doesn't exist"); return;
				}
				if(listing.status != 'listed'){
					reject("You can't contact this advertiser"); return;
				}


				batch.set(db.doc(`users/${listing.user.uid}/notifications/${id}`), {
					title: `You have a message about one of your properties listed`,
					content: `${name} emailed you. Check your email box!`,
					id,
					creationTS: Date.now(),
				});

				batch.update(db.doc(`users/${listing.user.uid}/settings/preferences`), {
					'counters.unreads.notifications':admin.firestore.FieldValue.increment(1)
				});


				let msg = {
					from: `${environment.appName} <${environment.noreplyEmail}>`,
					to: listing.user.email,
					subject: `${environment.appName}: You have a message about your property listed`,
					html:`<p>Listing title: ${listing.name}</p>
								<p>Listing address: ${listing.address}</p>
								<p>From: ${name}</p>
								<p>Message: ${message}</p>
								<p>To contact him. Reply to: ${email}</p>`
				};
				
				

				const nodemailer = new Nodemailer(msg);

				await nodemailer.sendMail();
				await batch.commit();
				resolve();
				

			} catch (err) {
				reject('Unexpected error. Try again later');
			}
		});
	}



	startChat(uid:string, content:string, listingID:string, applicantID:string){
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const chatID = sharedService.generateId();
				const messageID = sharedService.generateId();
				let [listing, applicantInfo] = (await db.getAll(db.doc(`listings/${listingID}`), db.doc(`users/${applicantID}`))).map(doc=>doc.data());
				
				let applicationInfo = (await db.collection(`applications`)
					.where('listing.id','==',listingID)
					.where('applicant.uid','==',applicantID)
					.limit(1).get()).docs[0].data();

				batch.update(db.doc(`applications/${applicationInfo.id}`), { 'applicant.chatID': chatID });

				batch.set(db.doc(`chats/${chatID}`), {
					chatName: `(${sharedService.capitalize(applicantInfo.firstName)} ${applicantInfo.lastName[0].toUpperCase()}) ${listing.name}`,
					id: chatID,
					lastMessage: {
						content,
						uid,
						creationTS: Date.now()
					},
					uids: [listing.user.uid, applicantInfo.uid]
				});

				batch.set(db.doc(`chats/${chatID}/messages/${messageID}`), {
					content,
					uid,
					creationTS: Date.now(),
					id: messageID
				});

				await batch.commit();
				resolve({chatID, messageID});


			} catch (err) {
				reject('Unexpected error. Try again later');
			}

		});
	}

	

	

}