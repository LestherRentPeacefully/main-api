import {environment} from '../../environments/environment';
import * as admin from "firebase-admin";
import {Nodemailer} from "../Email/Nodemailer";
import {SharedService} from '../Shared/SharedService';


const sharedService = new SharedService();


export class SupportService{


  public sendMailToSupport(name:string,email:string,subject:string,message:string){
		const db = admin.firestore();
		let ID = sharedService.generateId();
		let msg = {from:`${name} <${email}>`,
					 to:environment.supportEmail,
					 subject:`Support ID: ${ID} - Subject: ${subject}`,
					 text:message
		};
		return new Promise(async (resolve,reject)=>{
			try{
				const nodemailer = new Nodemailer(msg);
				await nodemailer.sendMail();
				resolve();
			}catch(err){
				reject('Unexpected error contacting support. Try again later');
		}
		});
	}

}