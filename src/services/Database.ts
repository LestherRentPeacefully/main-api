import * as admin from "firebase-admin";
const sql = require('mssql');
import {environment} from "../environments/environment";

export class Database {
	private config;
	public static pool;

	constructor(){
		this.config = { 
		  credential: admin.credential.cert(environment.firebaseConfig.credential),
			databaseURL: environment.firebaseConfig.databaseURL,
			storageBucket:environment.firebaseConfig.storageBucket
		}
	}

	public async init() {
		admin.initializeApp(this.config);
		Database.pool = await sql.connect(environment.mssqlConfig);
		console.log('Connected to the SQL server');
	}

}