import * as admin from "firebase-admin";


export class ChatService{

  getChats(uid:string){
    return new Promise(async (resolve, reject)=>{
      try {
        let chats = [];
        let usersRefs = [];
        let uids:string[] = [];
        
        const db = admin.firestore();
        const chatSnapshots = await db.collection('chats').where('uids', 'array-contains', uid).get();

        if(!chatSnapshots.empty){
          for(let chatSnapshot of chatSnapshots.docs){
            chats.push(chatSnapshot.data());
            let userid = (<string[]>chatSnapshot.data().uids).find(_uid=>_uid!=uid);

            if(!uids.includes(userid)){
              uids.push(userid);
              usersRefs.push(db.doc(`users/${userid}`));
            }
          }
  
          const usersSnapshot = await db.getAll(...usersRefs);
  
          chats.map(chat=>{
            let userSnapshot = usersSnapshot.find(item=>item.data().uid == (<string[]>chat.uids).find(_uid=>_uid!=uid));
            let user = userSnapshot.data();
            chat.user = {
              firstName: user.firstName,
              lastName: user.lastName,
              uid:user.uid,
              photo:user.photo
            };
            return chat;
          });

        }

        resolve(chats);

      } catch (err) {
        reject('Unexpected error getting chats. Try again later');
      }
    });
  }

  sendMessage(uid:string, chatID:string, content:string){
    return new Promise(async (resolve, reject)=>{
      try {
        const db = admin.firestore();
        const batch = db.batch();
        const messageID = db.collection('uniqueID').doc().id;
        const chat = (await db.doc(`chats/${chatID}`).get()).data();

        if ((<string[]>chat.uids).includes(uid)){
          batch.update(db.doc(`chats/${chatID}`), {
            lastMessage: {
              content,
              uid,
              creationTS: Date.now()
            },
          });

          batch.set(db.doc(`chats/${chatID}/messages/${messageID}`), {
            content,
            uid,
            creationTS: Date.now(),
            id: messageID
          });

          await batch.commit();
          resolve(messageID);

        } else {
          reject('Invalid user');
        }
      } catch (err) {
        reject('Unexpected error sending message. Try again later');
      }
    });
  }



}