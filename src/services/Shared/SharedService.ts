import {BigNumber} from 'bignumber.js';
import * as admin from "firebase-admin";
import {environment} from '../../environments/environment';
import fetch from 'node-fetch';


export class SharedService{

  currencies = Object.keys(environment.blockchain);
  
  async verifyIdToken(req, res, next){
    const token = req.get('authorization');

    if (token) {

      try {
        res.locals.uid = (await admin.auth().verifyIdToken(token)).uid;
        next();
      } catch (err) {
        res.send({success:false, data:null, error:{message:'Error verifying ID, try again later'}})
      }
  
    } else {
      res.send({success:false, data:null, error:{message:'No token provided'}});
    }
  }


  formatCryptoAmount(currency:string, amount:string | number | BigNumber){
    return new BigNumber(amount).decimalPlaces(environment.blockchain[currency].decimals).toString(10);
  }

  capitalize(value:string){
		return value.charAt(0).toUpperCase() + value.slice(1);
  }
  
  deleteFiles(files:string[]){
		const bucket = admin.storage().bucket();
		let arrayOfPromises = [];
		for(let file of files){
			arrayOfPromises.push(bucket.file(file).delete());
		}
		// let [res] = await bucket.getFiles({prefix:'users/Mijuj20TzOWaHpAkDutAoVVFzRI2/propertyListing/images'});
		//await bucket.deleteFiles({prefix:'users/Mijuj20TzOWaHpAkDutAoVVFzRI2/propertyListing/images'});
		return Promise.all(arrayOfPromises);
  }
  

  addMonths(oldDate:number | Date, count:number) {
    const date = new Date(oldDate);
    const day = date.getDate();
    date.setMonth(date.getMonth() + count, 1);
    const month = date.getMonth();
    date.setDate(day);
    if (date.getMonth() !== month) date.setDate(0);
    return date;
  }

  generateId(){
    return admin.firestore().collection('uniqueID').doc().id;
  }


  forwardRequest(path: string, body: any) {
    return new Promise(async(resolve, reject) => {

     try {
       let res = await fetch(`${environment.listenerAPIURL}/${path}`, {
         method: 'POST',
         body: JSON.stringify(body),
         headers: { 'Content-Type': 'application/json' }
       });
       let response = await res.json();

       resolve(response);
       
     } catch (err) {
       console.log(err.message || err);
       reject('Unexpected error. Try again later');
     }
      
       
   });
 }

  public pricings = {
    free: {
      amount: 0,
      tenant: {},
      realEstateAgent: {
        maxListed: 2,
      },
      landlord: {
        maxListed: 2,
      },
    },
    basic: {
      amount: 4.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 7,
      },
      landlord: {
        maxListed: 7,
      },
    },
    plus: {
      amount: 9.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 15,
      },
      landlord: {
        maxListed: 15,
      },
    },
    professional: {
      amount: 14.95,
      tenant: {},
      realEstateAgent: {
        maxListed: 20,
      },
      landlord: {
        maxListed: 20,
      },
    },
    enterprise: {
      amount: 19.95,
    }
  }



}