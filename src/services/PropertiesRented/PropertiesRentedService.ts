import * as admin from "firebase-admin";
import {BigNumber} from 'bignumber.js';
import {environment} from '../../environments/environment';
const hellosign = require('hellosign-sdk')({ key: environment.hellosign.privateKey });

export class PropertiesRentedService{

  public endLease(uid:string, userInPropertyID:string, propertyRentedID:string){
		const db = admin.firestore();
		const batch = db.batch();

		return new Promise(async(resolve, reject)=>{
			try{
				
				let [propertyRentedRentedSnapshot] = await Promise.all([
					db.doc(`propertiesRented/${propertyRentedID}`).get(), 
				]);
					
				let propertyRented = propertyRentedRentedSnapshot.data();
				
				if (propertyRented.tenant.uid==uid || propertyRented.landlord.uid==uid) {
					if (propertyRented.status=='rented') {
						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							status:'endingLease',
							requestedBy:uid
						});
						await batch.commit();
						resolve();
						

					} else if(propertyRented.status=='endingLease' && propertyRented.requestedBy!=uid){

						let [userInfoSnapshot, userInPropertySnapshot] = await Promise.all([
							db.doc(`users/${uid}`).get(), 
							db.doc(`users/${userInPropertyID}`).get(), 
						]);
							
						let userInfo = userInfoSnapshot.data();
						let userInProperty = userInPropertySnapshot.data();

						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							status:'ended',
							culminationTS: new Date().getTime()
						});

						if (userInfo.uid==propertyRented.tenant.uid && userInProperty.uid==propertyRented.landlord.uid) {

							let tenantObj = {
								'tenant.totalLandlords':userInfo.tenant.totalLandlords+1,
								'tenant.currentlyRented':userInfo.tenant.currentlyRented-1>=0? userInfo.tenant.currentlyRented-1:0,
								'tenant.totalRented':userInfo.tenant.totalRented+1,
							}
							let landlordObj = {
								'landlord.totalTenants':userInProperty.landlord.totalTenants+1,
								'landlord.currentlyRented':userInProperty.landlord.currentlyRented-1>=0? userInProperty.landlord.currentlyRented-1:0,
								'landlord.totalRented':userInProperty.landlord.totalRented+1,
							}

							batch.update(db.doc(`users/${propertyRented.tenant.uid}`), tenantObj);

							// batch.update(db.doc(`public/${propertyRented.tenant.uid}`), tenantObj);

							batch.update(db.doc(`users/${propertyRented.landlord.uid}`), landlordObj);

							// batch.update(db.doc(`public/${propertyRented.landlord.uid}`), landlordObj);

							await batch.commit();
							resolve();
							
						} else if(userInfo.uid==propertyRented.landlord.uid && userInProperty.uid==propertyRented.tenant.uid){

							let tenantObj = {
								'tenant.totalLandlords':userInProperty.tenant.totalLandlords+1,
								'tenant.currentlyRented':userInProperty.tenant.currentlyRented-1>=0? userInProperty.tenant.currentlyRented-1:0,
								'tenant.totalRented':userInProperty.tenant.totalRented+1,
							}
							let landlordObj = {
								'landlord.totalTenants':userInfo.landlord.totalTenants+1,
								'landlord.currentlyRented':userInfo.landlord.currentlyRented-1>=0? userInfo.landlord.currentlyRented-1:0,
								'landlord.totalRented':userInfo.landlord.totalRented+1,
							}

							batch.update(db.doc(`users/${propertyRented.tenant.uid}`), tenantObj);

							// batch.update(db.doc(`public/${propertyRented.tenant.uid}`), tenantObj);

							batch.update(db.doc(`users/${propertyRented.landlord.uid}`), landlordObj);

							// batch.update(db.doc(`public/${propertyRented.landlord.uid}`), landlordObj);

							await batch.commit();
							resolve();
							
						} else{
							reject("You're not an user of this property");
						}
						

					} else if(propertyRented.status=='ended'){
						reject('Lease already ended');
					}else{
						reject('Uknown action');
					}

				} else {
					reject("You're not an user of this property");
				}
				
			}catch(err){
				reject('Unexpected error ending lease');
			}
		});
  }
  


  public cancelCulmination(uid:string, propertyRentedID:string){
		const db = admin.firestore();
		const batch = db.batch();

		return new Promise(async(resolve, reject)=>{
			try{
				let [propertyRentedRentedSnapshot] = await Promise.all([
					db.doc(`propertiesRented/${propertyRentedID}`).get(), 
				]);
				let propertyRented = propertyRentedRentedSnapshot.data();

				if (propertyRented.status=='endingLease' && propertyRented.requestedBy==uid) {
					batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
						status:'rented',
						requestedBy:admin.firestore.FieldValue.delete()
					});
					await batch.commit();
					resolve();
				} else {
					reject('Uknown action');
				}
			}catch(err){
				reject('Unexpected error canceling request');
			}
		});
  }


  public provideFeedBack(uid:string, userInPropertyID:string, propertyRentedID:string, feedback:any){ //Review after 28 days
		const db = admin.firestore();
		const batch = db.batch();

		return new Promise(async(resolve, reject)=>{
			try{

				let [propertyRentedRentedSnapshot, userInPropertySnapshot] = await Promise.all([
					db.doc(`propertiesRented/${propertyRentedID}`).get(), 
          db.doc(`users/${userInPropertyID}`).get(),
				]);

				let propertyRented = propertyRentedRentedSnapshot.data();
				let userInProperty = userInPropertySnapshot.data();
				let id = db.collection('uniqueID').doc().id;


        if (uid==propertyRented.tenant.uid && userInProperty.uid==propertyRented.landlord.uid && 
            uid!=userInProperty.uid && !propertyRented.tenantProvidedFeedback && feedback.score>0 && feedback.score<=5 && 
            feedback.avgTimeOfTicketResolution>0 && feedback.avgTimeOfTicketResolution<=5 && 
            propertyRented.status=='ended' && (propertyRented.culminationTS-propertyRented.creationTS)/(1000*3600*24)>=28 ) {

              
					let landlordObj = {
						'landlord.score':new BigNumber( ((userInProperty.landlord.score*userInProperty.landlord.reviews) + feedback.score)/(userInProperty.landlord.reviews+1) ).decimalPlaces(1).toNumber(),
						'landlord.reviews':userInProperty.landlord.reviews+1,
						'landlord.avgTimeOfTicketResolution':new BigNumber( ((userInProperty.landlord.avgTimeOfTicketResolution*userInProperty.landlord.reviews) + feedback.avgTimeOfTicketResolution)/(userInProperty.landlord.reviews+1)).decimalPlaces(1).toNumber(),
					};

					batch.update(db.doc(`users/${userInPropertyID}`), landlordObj);
					// batch.update(db.doc(`public/${userInPropertyID}`), landlordObj);
					batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {tenantProvidedFeedback:true});
					batch.set(db.doc(`users/${userInPropertyID}/reviews/${id}`), {
            role:'landlord',
            reason:'propertyRented',
            propertyRentedID:propertyRentedID,
						feedback:{
							score:feedback.score,
							avgTimeOfTicketResolution:feedback.avgTimeOfTicketResolution,
							review:feedback.review || ''
						},
						providedBy:{
							uid,
							role:'tenant'
						},
						id:id,
						creationTS:new Date().getTime()
					});

					await batch.commit();
					resolve();

        } else if (uid==propertyRented.landlord.uid && userInProperty.uid==propertyRented.tenant.uid && 
                  uid!=userInProperty.uid && !propertyRented.landlordProvidedFeedback && feedback.score>0 && 
									feedback.score<=5 && feedback.ontimePayments>0 && feedback.ontimePayments<=5 && 
									propertyRented.status=='ended' && (propertyRented.culminationTS-propertyRented.creationTS)/(1000*3600*24)>=28){

					let tenantObj = {
						'tenant.score':new BigNumber( ((userInProperty.tenant.score*userInProperty.tenant.reviews) + feedback.score)/(userInProperty.tenant.reviews+1)).decimalPlaces(1).toNumber(),
						'tenant.reviews':userInProperty.tenant.reviews+1,
						'tenant.ontimePayments':new BigNumber(((userInProperty.tenant.ontimePayments*userInProperty.tenant.reviews) + feedback.ontimePayments)/(userInProperty.tenant.reviews+1)).decimalPlaces(1).toNumber(),
					}
					
					batch.update(db.doc(`users/${userInPropertyID}`), tenantObj);
					// batch.update(db.doc(`public/${userInPropertyID}`), tenantObj);
					batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {landlordProvidedFeedback:true});
					batch.set(db.doc(`users/${userInPropertyID}/reviews/${id}`), {
						role:'tenant',
						feedback:{
							score:feedback.score,
							ontimePayments:feedback.ontimePayments,
							review:feedback.review || ''
						},
						providedBy:{
							uid,
							role:'landlord'
						},
						id:id,
						creationTS:new Date().getTime()
					});

					await batch.commit();
					resolve();

				} else if((propertyRented.culminationTS-propertyRented.creationTS)/(1000*3600*24)<28) {
					reject("You can't rate an user for a property whose lease lasted less than 28 days");
				}else{
					reject("You can't rate this user");
				}

			}catch(err){
				reject('Unexpected error providing feedback');
			}

		});
	}
	


	public depositWithCrypto(uid:string, propertyRentedID:string, currency:string, amount:number){ //DELETE
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();

				let [propertyRentedSnapshot, currencyInfoSnapshot] = await db.getAll(db.doc(`propertiesRented/${propertyRentedID}`), db.doc(`currencies/${currency}`));
				let [propertyRented, currencyInfo] = [propertyRentedSnapshot.data(), currencyInfoSnapshot.data()];

				let [landlordBalanceSnapshot, tenantBalanceSnapshot] = await Promise.all([
          db.collection(`balances`).where('uid','==',propertyRented.landlord.uid).where('currency','==',currency).limit(1).get(),
          db.collection(`balances`).where('uid','==',propertyRented.tenant.uid).where('currency','==',currency).limit(1).get()
				]);
				

				if (!landlordBalanceSnapshot.empty) {

					let [landlordBalance, tenantBalance] = [landlordBalanceSnapshot.docs[0].data(), tenantBalanceSnapshot.docs[0].data()];

					const amountToPay = new BigNumber(amount).decimalPlaces(8).toString(10);

					if (!tenantBalanceSnapshot.empty && new BigNumber(tenantBalance.available).isGreaterThanOrEqualTo(amountToPay)) {

						batch.update(db.doc(`balances/${tenantBalance.id}`), {
							available: new BigNumber(tenantBalance.available).minus(amountToPay).toString(10)
						});
	
						batch.update(db.doc(`balances/${landlordBalance.id}`), {
							available: new BigNumber(landlordBalance.available).plus(amountToPay).toString(10)
						});

						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							'state.depositInfo':{
								creationTS: new Date().getTime(),
								currencyForPayment:{
									currency,
									amount:amountToPay,
									name:currencyInfo.name
								}
							}
						});	

						const id1 = db.collection('uniqueID').doc().id;
						const id2 = db.collection('uniqueID').doc().id;
						const id3 = db.collection('uniqueID').doc().id;

						batch.set(db.doc(`propertiesRented/${propertyRentedID}/history/${id1}`), {
							currency,
							action:'deposit',
							user: propertyRented.tenant,
							currencyForPayment:{
								currency,
								amount:amountToPay,
								name:currencyInfo.name
							},
							id:id1,
							creationTS: new Date().getTime(),
						});

						batch.set(db.doc(`transactions/${id2}`), {
							currency,
							type: 'payment sent',
							details: `Rent payment for property: ${propertyRented.property.address}`,
							propertyRentedID,
							processed: true,
							status: 'confirmed',
							amount: amountToPay,
							uid: propertyRented.tenant.uid,
							id: id2,
							creationTS: new Date().getTime()
						});

						batch.set(db.doc(`transactions/${id3}`), {
							currency,
							type: 'payment received',
							details: `Rent payment for property: ${propertyRented.property.address}`,
							propertyRentedID,
							processed: true,
							status: 'confirmed',
							amount: amountToPay,
							fee:0,
							uid: propertyRented.landlord.uid,
							id: id3,
							creationTS: new Date().getTime()
						});

						if (uid==propertyRented.tenant.uid) {
							await batch.commit();
							resolve();
							
						} else {
							reject('Invalid user');
						}


					}else{
						reject('Insufficient funds');
					}
					
				} else {
					reject("Landlord doesn't have a wallet for this currency. Ask him/her to create one");
				}
			} catch (err) {
				reject('Unexpected error sending payment');
			}
		});
	}


	ticket(uid:string, propertyRentedID:string, details:string){ //DELETE
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = db.collection('uniqueID').doc().id;
				let [propertyRentedSnapshot] = await db.getAll(db.doc(`propertiesRented/${propertyRentedID}`));
				let propertyRented = propertyRentedSnapshot.data();
				const open = propertyRented.state && propertyRented.state.ticketInfo && propertyRented.state.ticketInfo.open;
				
				if (propertyRented.tenant.uid==uid) {
					if (propertyRented.status=='rented') {
						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							'state.ticketInfo':{
								details:!open? details : null,
								open:!open,
								creationTS: new Date().getTime(),
							}
						});
	
						batch.set(db.doc(`propertiesRented/${propertyRentedID}/history/${id}`), {
							creationTS: new Date().getTime(),
							details:!open? details : null,
							action:'ticket',
							open:!open,
							user: propertyRented.tenant,
							id
						});
	
						await batch.commit();
						resolve();
						
					} else {
						reject("You can't modify the rent");
					}

				} else {
					reject('Invalid user');
				}
				
			} catch (err) {
				reject('Unexpected error');
			}
		});
	}


	modifyRent(uid:string, propertyRentedID:string, newRent:number){ //DELETE
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
				const batch = db.batch();
				const id = db.collection('uniqueID').doc().id;
				let [propertyRentedSnapshot] = await db.getAll(db.doc(`propertiesRented/${propertyRentedID}`));
				let propertyRented = propertyRentedSnapshot.data();

				if (propertyRented.landlord.uid==uid) {
					if (propertyRented.status=='rented') {
						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							'property.rentAmount':newRent,
							'state.rentModificationInfo':{
								creationTS: new Date().getTime(),
							}
						});
	
						batch.set(db.doc(`propertiesRented/${propertyRentedID}/history/${id}`), {
							creationTS: new Date().getTime(),
							action: 'rentModified',
							newRent,
							user: propertyRented.landlord,
							id
						});
	
						await batch.commit();
						resolve();
						
					} else {
						reject("You can't modify the rent");
					}
				} else {
					reject('Invalid user');
				}

			} catch (err) {
				reject('Unexpected error Modifying Rent');
			}
		});
	}


	public async getCurrencyPriceForEsign(uid:string, currency:string){ //DELETE
    const db = admin.firestore();
    let {price, name} = (await db.doc(`currencies/${currency}`).get()).data();
    await db.doc(`lastPriceForEsign/${uid}`).set({currency, price, name});
    return {currency, price};
  }


	payForEsignWithCrypto(uid:string, currency:string, propertyRentedID:string){ //DELETE
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();

				let [balanceSnaphot, [lastPriceForEsignSnapshot, propertyRentedSnapshot]] = await Promise.all([
          db.collection(`balances`).where('uid','==',uid).where('currency','==',currency).limit(1).get(),
          db.getAll(db.doc(`lastPriceForEsign/${uid}`), db.doc(`propertiesRented/${propertyRentedID}`))
				]);
				
				let balance = balanceSnaphot.docs[0].data();
				let lastPriceForEsign = lastPriceForEsignSnapshot.data();
				let propertyRented = propertyRentedSnapshot.data();

				let amountToPay = new BigNumber(10).div(lastPriceForEsign.price.USD).decimalPlaces(8).toString(10);

				if (!balanceSnaphot.empty && new BigNumber(balance.available).isGreaterThanOrEqualTo(amountToPay)) {
					const batch = db.batch();
					const id = db.collection('uniqueID').doc().id;

					batch.update(db.doc(`balances/${balance.id}`), {
						available: new BigNumber(balance.available).minus(amountToPay).toString(10)
					});			

					if (propertyRented.landlord.uid==uid && !propertyRented.landlordPaidForEsign) {

						batch.update(db.doc(`propertiesRented/${propertyRentedID}`), {
							landlordPaidForEsign:true
						});

						batch.set(db.doc(`transactions/${id}`), {
              currency: currency,
              type: 'eSign',
              processed: true,
              status: 'confirmed',
							amount: amountToPay,
							propertyRentedID,
              uid: uid,
              id: id,
              creationTS: new Date().getTime()
						});
						
						await batch.commit();
						resolve();


					}else{
						reject('eSign Already paid');
					}

				}else{
					reject('Insufficient funds');
				}
				
			} catch (err) {
				reject('Unexpected error sending payment');
			}
		});
	}


	async getSignatures(signatureRequestID:string){ //DELETE
		const signatures = (await hellosign.signatureRequest.get(signatureRequestID)).signature_request.signatures;
		for(let i in signatures){
			delete signatures[i].has_pin; delete signatures[i].signer_name; delete signatures[i].signer_role;
			delete signatures[i].order; delete signatures[i].last_viewed_at; delete signatures[i].last_reminded_at;
		}
		return signatures;
	}


	uploadDocument(uid:string, propertyRentedID:string, path:string){ //DELETE
		return new Promise(async (resolve, reject)=>{
			try {
				const db = admin.firestore();
	
				let propertyRented =	(await db.doc(`propertiesRented/${propertyRentedID}`).get()).data();
				if (!propertyRented.signatureRequestID) {
					
					const opts = {
						test_mode: environment.hellosign.test_mode,
						clientId: environment.hellosign.clientId,
						subject: 'eSigned document on RentPeacefully',
						// message: 'Please sign this NDA and then we can discuss more.',
						signers: [
							{
								email_address: propertyRented.landlord.email,
								name: `${this.capitalize(propertyRented.landlord.firstName)} ${this.capitalize(propertyRented.landlord.lastName)}`
							},
							{
								email_address: propertyRented.tenant.email,
								name: `${this.capitalize(propertyRented.tenant.firstName)} ${this.capitalize(propertyRented.tenant.lastName)}`
							}
						],
						files: [path]
					};

					if (propertyRented.landlord.uid==uid) {
						
						const signatureRequest = (await hellosign.signatureRequest.createEmbedded(opts)).signature_request;
						const signatureRequestID = signatureRequest.signature_request_id;
						const signatures = signatureRequest.signatures;
						for(let i in signatures){
							delete signatures[i].has_pin; delete signatures[i].signer_name; delete signatures[i].signer_role;
							delete signatures[i].order;	delete signatures[i].last_viewed_at; delete signatures[i].last_reminded_at;
						}
						const landlordSignatureID = signatures[0].signature_id;
						await db.doc(`propertiesRented/${propertyRentedID}`).update({	signatureRequestID });
						const signUrl = await this.getSignUrl(landlordSignatureID);
						resolve({signatureRequestID, signUrl, signatures});

					} else {
						reject('Invalid user');
					}

				} else {
					reject('Document already uploaded');
				}
				
			} catch (err) {
				reject('Unexpected error uploading documen');
			}
		});
	}


	async getSignUrl(signatureID:string){ //DELETE
		let signUrl = (await hellosign.embedded.getSignUrl(signatureID)).embedded.sign_url;
		return signUrl;
	}


	getDocUrl(signatureRequestID:string){ //DELETE
		return new Promise((resolve, reject)=>{
			hellosign.signatureRequest.download(signatureRequestID, { file_type: 'pdf', get_url:true }, (err, res) => {
				if (!err) resolve(res.file_url);
				else reject(err.message || err);
			});
		});
	}


	private capitalize(value:string){ //DELETE
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
  
  


}