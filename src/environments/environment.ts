
let production = false;



/* Environment Variable to be exported */

export const environment = {
    firebaseConfig: {   /* Firebase config */
        credential: require("../../serviceAccountKey.json"),
        databaseURL: 'https://blockchain-projects.firebaseio.com',
        storageBucket:'blockchain-projects.appspot.com'
    },
    sengridPrivateKey:'', //'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',
    smtpConfig:{    /* Nodemailer config */
        host:'mail.rentpeacefully.com',
        port:587,
        secure:false,
        auth:{
            user:'noreply@rentpeacefully.com',
            pass:'Rent4$'
        }
    },
    mssqlConfig:{  /* SQL server config */
        user: 'LestherCaballeroDB',
        password: 'RentPLesther#1234',
        server: '173.248.133.193', // You can use 'localhost\\instance' to connect to named instance
        port:1533,
        database: 'RentPeacefully',
        options: { encrypt: true } // Use this if you're on Windows Azure
    },
    hellosign:{
        privateKey: 'ac5d1b05a12cd79b6039501c3314fdeeb24b2916621b99c317f642d15e8845ad',
        clientId: 'c675f77b711a96f7aaf15a2deb333d33',
        test_mode: production? 0 : 1
    },
    listenerAPIURL: 'http://164.68.106.226:8080',
    noreplyEmail:'noreply@rentpeacefully.com',
    supportEmail:'support@rentpeacefully.com',
    appName:'RentPeacefully',
    mainAppURL: 'https://rentpeacefully.com',
    etherscanURL: production? 'api.etherscan.io':'api-ropsten.etherscan.io',
    notifierContractAddress:'0x561A407e8894c746d881eBf25a6033d24fd694aF',
    contractVersion:'v1.0',
    propertyContractABI: require('../../ABIs/propertyContractABI.json'),
    altcoinNodesAuth: {
        user: 'user',
        password: 'password'
    },
    blockchain:{
        ETH:{
            nodeURL: 'http://164.68.106.226:8545',
            decimals: 18,
            minConfirm: 12,
            securePlatformWallet:{
                address:"0x529B1cCDB8a02571Ad22c0e534D18Dd7c66d1E01",
                privateKey:'9ba2a9dd26361c2db2025f58c6eee59810823969d857182a876c52f8cafbadca'
            },
        },
        LTC:{
            nodeURL:'http://164.68.106.226:9332', //19332 testnet 19443 regtest
            decimals:8,
            minConfirm: 4,
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //main
                messagePrefix: '\x19Litecoin Signed Message:\n',
                bip32: {
                  public: 0x019da462,
                  private: 0x019d9cfe
                },
                pubKeyHash: 0x30,
                scriptHash: 0x32,
                wif: 0xb0
            }
        },
        BTC:{
            nodeURL:'http://164.68.106.226:8332', //18332 testnet 18443 regtest 
            decimals:8,
            minConfirm: 2,
            securePlatformWallet:{
                address:'',
                privateKey:'',
            },
            network:{ //main
                messagePrefix: '\x18Bitcoin Signed Message:\n',
                bech32: 'bc',
                bip32: {
                    public: 0x0488b21e,
                    private: 0x0488ade4
                },
                pubKeyHash: 0x00,
                scriptHash: 0x05,
                wif: 0x80
            },
            
        },
        DASH:{
            nodeURL:'http://164.68.106.226:9998', //19998 testnet 18332 regtest
            decimals:8,
            minConfirm: 6,
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //mainnet
                messagePrefix: '\x19Dash Signed Message:\n',
                bip32: {
                  public: 0x0488b21e,
                  private: 0x0488ade4
                },
                pubKeyHash: 0x4c,
                scriptHash: 0x10,
                wif: 0xcc
            }
        },
        ZEC:{
            nodeURL:'http://164.68.106.226:8232', //18232 testnet 18232 regtest
            decimals:8,
            minConfirm: 8,
            securePlatformWallet:{
                address:'',
                privateKey:''
            },
            network:{ //mainnet
                messagePrefix: '\x19Zcash Signed Message:\n',
                bip32: {
                  public: 0x0488b21e, 
                  private: 0x0488ade4
                },
                pubKeyHash: 0x1cb8,
                scriptHash: 0x1cbd,
                wif: 0x80
            }
        },
        DOGE:{
            nodeURL:'http://164.68.106.226:22555', //18332 testnet 18443 regtest 
            decimals:8,
            minConfirm: 6,
            securePlatformWallet:{
                address:'',
                privateKey:'',
            },
            network:{ //mainnet
                messagePrefix: '\x18Dogecoin Signed Message:\n',
                bip32: {
                    public: 0x02facafd,
                    private: 0x02fac398
                },
                pubKeyHash: 0x1e,
                scriptHash: 0x16,
                wif: 0x9e
            }
            
        },
        // BCH:{
        //     nodeURL:'http://164.68.106.226:8332', //18332 testnet 18332 regtest 
        //     decimals:8,
        //     securePlatformWallet:{
        //         address:'',
        //         privateKey:'',
        //     },
        //     network:{ //mainnet
        //         messagePrefix: '\x18Bitcoin Signed Message:\n',
        //         bech32: 'bc',
        //         bip32: {
        //             public: 0x0488b21e,
        //             private: 0x0488ade4
        //         },
        //         pubKeyHash: 0x00,
        //         scriptHash: 0x05,
        //         wif: 0x80
        //     },
         
        // }
    },
};