const bodyParser = require('body-parser');
const cors = require('cors');
import * as express from 'express';

/*Database initialization */
import {Database} from "./services/Database";
let database = new Database();
database.init();
/* Routes */
import {EthereumRoutes} from './routes/EthereumRoutes';
import {AccountRoutes} from './routes/AccountRoutes';
import {RentalAgreementRoutes} from './routes/RentalAgreementRoutes';
import {SettingsRoutes} from './routes/SettingsRoutes';
import {SupportRoutes} from './routes/SupportRoutes';
import {myPropertiesRoutes} from './routes/myPropertiesRoutes';
import {ListingsRoutes} from './routes/ListingsRoutes';
import {PropertiesRentedRoutes} from './routes/PropertiesRentedRoutes';
import {AdminRoutes} from './routes/AdminRoutes';
import {ProfileRoutes} from './routes/ProfileRoutes';
import {WalletRoutes} from './routes/WalletRoutes';
import {PricingRoutes} from './routes/PricingRoutes';
import {DirectoriesRoutes} from './routes/DirectoriesRoutes';
import {myServiceProvidersJobsRoutes} from './routes/MyServiceProvidersJobsRoutes';
import {ChatRoutes} from './routes/ChatRoutes';
import {NotificationsRoutes} from './routes/NotificationsRoutes';
import {AgreementsRoutes} from './routes/AgreementsRoutes';


const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

let whiteList:any[] = [];
let corsOptions:any = {};

app.use(cors(corsOptions));

const PORT = process.env.PORT || 8080;

app.get('/', (req, res)=>{
	res.send('Hello World');
})

app.use('/ethereum', EthereumRoutes);

app.use('/account', AccountRoutes);

app.use('/rental-agreement', RentalAgreementRoutes);

app.use('/settings', SettingsRoutes);

app.use('/support', SupportRoutes);

app.use('/my-properties', myPropertiesRoutes);

app.use('/listings', ListingsRoutes);

app.use('/properties-rented', PropertiesRentedRoutes);

app.use('/admin', AdminRoutes);

app.use('/profile', ProfileRoutes);

app.use('/wallet', WalletRoutes);

app.use('/pricing', PricingRoutes);

app.use('/directory', DirectoriesRoutes);

app.use('/my-service-providers-jobs', myServiceProvidersJobsRoutes);

app.use('/chats', ChatRoutes);

app.use('/notifications', NotificationsRoutes);

app.use('/agreements', AgreementsRoutes);





app.listen(PORT, ()=>{
	console.log(`Node Server initialized on http://localhost:${PORT}`);
});



// const config = {
// 	user: 'LestherCaballeroDB',
// 	password: 'RentPLesther#123',
// 	server: '173.248.133.193', // You can use 'localhost\\instance' to connect to named instance
// 	port:1533,
// 	database: 'RentPeacefully',
// 	options: {
// 		encrypt: true // Use this if you're on Windows Azure
// 	}
// }
// const sql = require('mssql');

// // SELECT id, ( 6371 * acos( cos( radians(40.70655) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-74.01013569999998) ) + sin( radians(40.70655) ) * sin( radians( lat ) ) ) ) AS distance FROM listings HAVING distance < 25 ORDER BY distance LIMIT 0 , 20;

// (async  () => {
// 	try {
// 			let pool = await sql.connect(config)
// 			console.log('Connected')
// 			// let result1 = await sql.query`SELECT * FROM listings GO`;
// 			let id = 'AeNTKyPZDvBooqfCqnqZ';
// 			const offset = '0';
// 			let _sql = `IF (SELECT COUNT(*) FROM listings WHERE id = @id) = 1
// 										BEGIN
// 											DELETE FROM listings WHERE id = @id;
// 										END
									
// 									INSERT INTO listings
// 									(
// 									[id], [lat], [lng], [type], [numOfBeds], [numOfBaths], [sqft], 
// 									[rentAmount], [securityDepositAmount], [availableFrom],[creationTS],
// 									[uid],[role], [status], [sponsored]
// 									)
// 									VALUES
// 									(
// 									@id, 40.70655, -74.01013569999998, 'Apartment', 'Studio',1,700,
// 									2595,2595,1540180800000,1540202548199,'Mijuj20TzOWaHpAkDutAoVVFzRI2','landlord','listed', 0
// 									)`;
// 			let result1 = await pool.request()
// 					.input('id', id)
// 					.query(_sql)

// 			// let result1 = await pool.request()
// 			// // 	.input('input_parameter', sql.Int, value)
// 			// 	.query(`INSERT INTO listings
// 			// 		( 
// 			// 			[id], [lat], [lng], [type], [numOfBeds], [numOfBaths], [sqft], 
// 			// 			[rentAmount], [securityDepositAmount], [availableFrom],[creationTS],
// 			// 			[uid], [status], [sponsored]
// 			// 		)
// 			// 		VALUES
// 			// 		(
// 			// 			'fake', 40.70655, -74.01013569, 'Apartment', 'Studio',1,700,
// 			// 			2595,2595,1540180800000,1540202548199,'Mijuj20TzOWaHpAkDutAoVVFzRI2','listed', 0
// 			// 		)`)
// 			console.log(result1.recordset)
// 			pool.close()

// 	} catch (err) {
// 		console.log('Error 1:',err.message || err, err.code);
// 	}
// })()

// sql.on('error', err => {
// 	console.log('Error 2:',err.message || err);
// })



